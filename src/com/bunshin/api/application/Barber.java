package com.bunshin.api.application;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.linking.DeclarativeLinkingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import com.bunshin.api.MyApplicationEventListener;
import com.bunshin.api.ResourceFilterBindingFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@ApplicationPath("barber")
public class Barber extends ResourceConfig {
public Barber(){
	packages("com.bunshin.api").
	register(RolesAllowedDynamicFeature.class).register(JacksonJsonProvider.class)
	.register(MultiPartFeature.class).register(DeclarativeLinkingFeature.class)
	.register(LoggingFilter.class).register(ResourceFilterBindingFeature.class)
	.register(MyApplicationEventListener.class)/*.register(CsrfProtectionFilter.class)*/
	.property(ServerProperties.METAINF_SERVICES_LOOKUP_DISABLE, true);
}
}