package com.bunshin.api;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

import com.bunshin.api.SecurityCheck;
import com.bunshin.api.SecurityContextFilter;

@Provider
public class ResourceFilterBindingFeature implements DynamicFeature {

	@Override
	public void configure(ResourceInfo resourceInfo, FeatureContext context) {
		if (resourceInfo.getResourceMethod().isAnnotationPresent(SecurityCheck.class)) {
			context.register(SecurityContextFilter.class);

		}
		
		
	}

}
