package com.bunshin.api;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public abstract class EndPoint {
	protected Channel channel = null;
	protected String endPointName;
	protected String exchangeType;
	protected String exchange;

	public EndPoint(Connection connection, String endPointName, String exchangeType,
			String exchange) throws IOException {
		this.endPointName = endPointName;
		this.exchange = exchange;
		this.exchangeType = exchangeType;

		// create a channel
		channel = connection.createChannel();

		// declare exchange
		channel.exchangeDeclare(exchange, exchangeType, true);
		
		
		System.out.println("channel "+channel.isOpen());

	}

	public Channel getChannel() {
		return channel;
	}

}
