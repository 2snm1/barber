package com.bunshin.api;

import java.util.Map;

public class Account implements java.security.Principal {

	private boolean active;
	private String accountId = null;
	private String sessionId = null;
	private String about=null;
	private String name = null;
	private Map<String, Object> accountType = null;
	private String email = null;
	private String phone = null;
	private String thumbPic = null;
	private String pic = null;

	public Account() {

	}

	public Account(boolean active, String accountId, String sessionId, String name, Map<String, Object> accountType,
			String email, String phone, String thumbPic, String pic,String about) {
		this.active = active;
		this.accountId = accountId;
		this.sessionId = sessionId;
		this.name = name;
		this.accountType = accountType;
		this.about=about;
		this.email = email;
		this.phone = phone;
		this.thumbPic = thumbPic;
		this.pic = pic;
	}

	public String getAbout() {
		return about;
	}

	public boolean isActive() {
		return active;
	}

	public String getAccountId() {
		return accountId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public Map<String, Object> getAccountType() {
		return accountType;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getThumbPic() {
		return thumbPic;
	}

	public String getPic() {
		return pic;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String toString() {
		return "Account [active=" + active + ", accountId=" + accountId + ", sessionId=" + sessionId + ", about="
				+ about + ", name=" + name + ", accountType=" + accountType + ", email=" + email + ", phone=" + phone
				+ ", thumbPic=" + thumbPic + ", pic=" + pic + "]";
	}
}
