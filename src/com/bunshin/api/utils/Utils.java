package com.bunshin.api.utils;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.poi.util.IOUtils;
import org.atmosphere.cpr.DefaultBroadcaster;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.Ostermiller.util.Base64;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.bunshin.api.Account;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.RabbitConnectionRecieve;
import com.bunshin.api.RabbitConnectionSend;
import com.bunshin.api.ScheduledEventsHandler;
import com.bunshin.api.messagequeues.Producer;
import com.bunshin.api.messagequeues.QueueConsumer;
import com.bunshin.api.services.user.representation.UserRepresentation;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.sematime.api.json.JSONArray;
import com.sematime.api.json.JSONObject;

public class Utils {

	public final String dbName = "business_site";
	private final Logger log = LoggerFactory.getLogger(Utils.class);
	private boolean exceptionThrown = false;
	private Scheduler scheduler = null;
	public final static ConcurrentHashMap<String, String> userAcc = new ConcurrentHashMap<String, String>();

	/**
	 * creates a JSON string status error message to be delivered to the
	 * developer for debugging
	 *
	 * @param status
	 *            The HTML error status
	 * @param message
	 *            the message to be appended to the response
	 * @return a String object that is sent to the developer
	 */
	public String createErrorResponse(Status status, String message) {

		String description = "";
		JSONObject json = null;
		try {
			switch (status.getStatusCode()) {
			case 400:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1";
				break;
			case 403:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.4";
				break;
			case 404:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.5";
				break;
			case 405:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.6";
				break;
			case 409:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.10";
				break;
			case 412:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.13";
				break;
			case 415:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.16";
				break;
			case 417:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.18";
				break;
			case 500:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.1";
				break;
			case 503:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.4";
				break;
			default:
				description = "Undocumented error. Please contact the bunshin team for help.";
				break;
			}

			/*
			 * message = "\"status:\"" + status.getStatusCode() + " - " +
			 * status.getReasonPhrase() + "\"," + "\"error message\":" + "\"" +
			 * escapeString(message) + "\"," + "\"description\":" + "\"" + new
			 * URL(description) + "\"";
			 */
			json = new JSONObject();
			json.accumulate("status", status.getStatusCode() + " - " + status.getReasonPhrase())
					.accumulate("error message", escapeString(message)).accumulate("description", new URL(description));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// return
		// Response.status(status).entity(message).cacheControl(cacheControl()).build();
		return json.toString();
	}

	/**
	 * used to escape JSON strings
	 *
	 * @param text
	 *            the String value to be escaped
	 * @return a String value with all the escape sequences removed
	 */
	public String escapeString(String text) {
		text = text.replace("\"", "\'");
		text = text.replace("\'", "\'");
		text = text.replace("\n", "\\n");
		text = text.replace("\r", "\r");
		text = text.replace("\t", "\t");
		text = text.replace("\\", "\\");

		return text;
	}

	public boolean businesslStatus(String businessId) {
		DataSource ds = MyDataSource.INSTANCE.getDataSource(dbName);
		Connection conn = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			if (conn != null) {
				pstmt = conn.prepareStatement("SELECT * FROM business_site.`business_info`  "
						+ "WHERE `business_id` = ? ");
				pstmt.setString(1, businessId);
				rs = pstmt.executeQuery();
				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						return rs.getBoolean("active");
					}

				} else {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	public Account getAccount(String sessionToken, String accountId) throws NamingException {
		String sessionId = null;
		String name = null;
		String email = null;
		String phone = null;
		String thumbPic = null;
		String pic = null;
		Map<String, Object> accountType = new HashMap<String, Object>();
		DataSource ds = MyDataSource.INSTANCE.getDataSource(dbName);
		String about = null;
		Connection conn = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			if (conn != null) {
				boolean active = false;

				pstmt = conn
						.prepareStatement("SELECT user.about_me,`user`.`user_id`,`user`.`name`,`user`.user_type,`user`.email,"
								+ "`user`.phone,`user`.active,`user`.thumb_pic,`user`.pic,`user_type`.name AS type_name,`user`.last_login "
								+ " FROM `business_site`.`user` "
								+ " INNER JOIN `business_site`.`user_type` ON `user_type`.user_type_id=`user`.user_type "
								+ " INNER JOIN `business_site`.`access_tokens` ON `access_tokens`.`user_id`=`user`.`user_id` "
								+ " WHERE `user`.`user_id`=? AND `access_tokens`.token=? ");
				pstmt.setString(2, sessionToken);
				pstmt.setString(1, accountId);
				rs = pstmt.executeQuery();
				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						active = rs.getBoolean("active");
						sessionId = rs.getString("last_login");
						email = rs.getString("email");
						phone = rs.getString("phone");
						thumbPic = rs.getString("thumb_pic");
						pic = rs.getString("pic");
						accountType.put("type", rs.getInt("user_type"));
						accountType.put("name", rs.getString("type_name"));
						about = rs.getString("about_me");
						return new Account(active, accountId, sessionId, name, accountType, email, phone, thumbPic,
								pic, about);

					}
				} else {
					return null;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}

	public boolean deleteToken(String tokenId, String userId) {
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (tokenId != null) {
					stmt = conn
							.prepareStatement(" DELETE FROM `business_site`.`access_tokens` WHERE access_tokens_id=?");
					stmt.setString(1, tokenId);
				} else {
					stmt = conn.prepareStatement(" DELETE FROM `business_site`.`access_tokens` WHERE user_id=?");
					stmt.setString(1, userId);
				}

				if (stmt.executeUpdate() == 1) {

					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return false;

		} finally {
			try {

				if (stmt != null)

					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				log.error(e.getMessage());
				return false;
			}
		}

	}

	public CacheControl cacheControl() {
		CacheControl cc = new CacheControl();
		cc.setMaxAge(259200);

		return cc;

	}

	public boolean isValidNumber(String number) {
		Pattern pattern = Pattern.compile("[+]?\\d{9,12}");
		Matcher match = pattern.matcher(number);
		if (match.matches() || (number.startsWith("07") && number.length() == 10 && isAllDigits(number))
				|| (number.startsWith("7") && number.length() == 9 && isAllDigits(number))
				|| (number.startsWith("254") && number.length() == 12 && isAllDigits(number))
				|| (number.startsWith("+") && number.length() >= 10 && isAllDigits(number.substring(1)))) {
			return true;
		}

		return false;
	}

	/**
	 * Checks whether {@code item} is all digits
	 * 
	 * @param item
	 * @return
	 */
	public boolean isAllDigits(String item) {
		Pattern pattern = Pattern.compile("\\d");
		Matcher match = pattern.matcher(item);
		if (match.matches()) {
			return true;
		}

		return false;
	}

	public String sanitizeNumber(String phoneNumber) {
		// replace 'o' for ogre with '0' (zero)
		phoneNumber = phoneNumber.trim();

		phoneNumber = phoneNumber.replace('o', '0');
		phoneNumber = phoneNumber.replace('O', '0');

		if (phoneNumber.startsWith("0"))
			phoneNumber = phoneNumber.substring(1);

		if (phoneNumber.startsWith("254"))
			phoneNumber = "+" + phoneNumber;

		// If number does not begin with a plus, then prefix number with country
		// code
		if (!phoneNumber.startsWith("+")) {
			phoneNumber = "+254" + phoneNumber;
		}

		return phoneNumber;
	}

	public boolean isValidEmail(String email) {
		if (email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Gets a hash of {@code password} using the SHA-1 encryption algorithm.
	 *
	 * @param password
	 *            the password to hash
	 */
	public String getPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		int iterations = 1000;
		char[] chars = password.toCharArray();
		byte[] salt = getSalt(20).getBytes();

		PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
		byte[] hash = skf.generateSecret(spec).getEncoded();
		// return iterations + ":" + toHex(salt) + ":" + toHex(hash);
		return toHex(hash);
	}

	/**
	 *
	 * @param array
	 * @return String Hex value of the array
	 * @throws NoSuchAlgorithmException
	 */

	private String toHex(byte[] array) throws NoSuchAlgorithmException {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}

	/**
	 *
	 * @return a SecureRandom Salt value
	 * @throws NoSuchAlgorithmException
	 */
	public static String getSalt(int length) throws NoSuchAlgorithmException {
		SecureRandom sr;
		try {
			sr = SecureRandom.getInstance("SHA1PRNG", "SUN");

			byte[] salt = new byte[length];
			sr.nextBytes(salt);

			return Base64.encodeToString(salt);
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String setToken(String userId) {
		DataSource ds = null;
		PreparedStatement stmt = null;

		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			Long now = System.currentTimeMillis();
			String token = getPasswordHash(String.valueOf(now) + userId);

			ds = MyDataSource.INSTANCE.getDataSource(dbName);

			conn = ds.getConnection();
			if (conn != null) {
				// TODO remove validity period not needed, will add the time
				// during filtering
				conn.setAutoCommit(false);
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`access_tokens` VALUES (?,?,?,?)");
				stmt.setString(1, String.valueOf(now));
				// token valid for one year
				stmt.setString(2, String.valueOf(now + (long) (1000 * 60 * 60 * 24 * 7 * 4 * 3)));
				stmt.setString(3, token);
				stmt.setString(4, userId);

				pstmt = conn.prepareStatement("UPDATE `business_site`.user SET last_login=? WHERE user_id=?");
				pstmt.setString(1, String.valueOf(now));
				pstmt.setString(2, userId);

				if (stmt.executeUpdate() >= 1 && pstmt.executeUpdate() >= 1) {
					conn.commit();
					return token;
				} else {
					conn.rollback();
					return null;
				}

			} else {
				return null;
			}
		} catch (Exception e) {
			exceptionThrown = true;
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			log.error(e.getMessage());
			return null;

		} finally {
			try {

				if (exceptionThrown) {
					conn.rollback();
				}
				conn.setAutoCommit(true);

				if (stmt != null)

					stmt.close();
				stmt = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;
				if (conn != null)
					conn.close();
			} catch (SQLException e) {

				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
				log.error(e.getMessage());
				return null;
			}
		}

	}

	public HashMap<String, String> checkUserByEmail(String email) {
		DataSource ds = MyDataSource.INSTANCE.getDataSource(dbName);
		System.out.print(ds);
		Connection conn = null;
		HashMap<String, String> user = new HashMap<String, String>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			System.out.println(conn + " other " + ds.toString() + "\n empty " + user.isEmpty());
			if (conn != null) {
				pstmt = conn.prepareStatement("SELECT * " + " FROM `business_site`.`user` " + " WHERE user.email=? ");
				pstmt.setString(1, email);
				rs = pstmt.executeQuery();
				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						user.put("user_id", rs.getString("user_id"));
					}

					return user;
				} else {
					return user;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return user;
	}

	public HashMap<String, String> checkUserByPhone(String phoneNumber) {
		DataSource ds = MyDataSource.INSTANCE.getDataSource(dbName);
		Connection conn = null;
		HashMap<String, String> user = new HashMap<String, String>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			if (conn != null) {
				pstmt = conn.prepareStatement("SELECT * " + " FROM `business_site`.`user` " + " WHERE user.phone=? ");
				pstmt.setString(1, phoneNumber);
				rs = pstmt.executeQuery();
				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						user.put("user_id", rs.getString("user_id"));
					}

					return user;
				} else {
					return user;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return user;
	}

	public ArrayList<String> getAllEmailAdress() {
		ArrayList<String> emails = new ArrayList<String>();
		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(dbName);

			conn = ds.getConnection();

			if (conn != null) {

				stmt = conn.createStatement();
				rs = stmt.executeQuery("SELECT email FROM `business_site`.users ");

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						emails.add(rs.getString("email"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (stmt != null)

					stmt.close();
				stmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return emails;
	}

	public void queueMessage(String endPointName, String exchangeType, String exchange, AMQP.BasicProperties props,
			HashMap<String, Object> body) {

		 Producer prod = null;

		try {
			// redacted for production
			 prod = new
			 Producer(RabbitConnectionSend.INSTANCE.getConnection(),
			 endPointName, exchangeType, exchange, props);
			 prod.publishMessage(body);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void consumeMessage(String endPointName, String exchangeType, String exchange, BasicProperties props,
			boolean autoAck) {
		 QueueConsumer consumer = null;
		try {
			// // redacted for production use
			 consumer = new QueueConsumer(
			 RabbitConnectionRecieve.INSTANCE.getConnection(),
			 endPointName, exchangeType, exchange,autoAck);
			 Thread consumerThread = new Thread(consumer);
			 consumerThread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isValidPasword(String password) {
		System.out.println(password);
		if (password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=_\\*{}\":;.,<>?/-`~])(?=\\S+$).{8,}$")) {
			return true;
		} else {
			return false;
		}
	}

	public UserRepresentation getUserRepresentation(String accountId, String phone, String email) {
		String name = null;
		String thumbPic = null;
		String about = null;
		String pic = null;
		String error = null;
		Map<String, Object> accountType = new HashMap<String, Object>();
		DataSource ds = MyDataSource.INSTANCE.getDataSource(dbName);
		Connection conn = null;
		String sql = "SELECT user.about_me,`user`.`user_id`,`user`.`name`,`user`.user_type,`user`.email,"
				+ "`user`.phone,`user`.active,`user`.thumb_pic,`user`.pic,`user_type`.name AS type_name "
				+ " FROM `business_site`.`user` "
				+ " LEFT JOIN `business_site`.`user_type` ON `user_type`.user_type_id=`user`.user_type "
				+ " LEFT JOIN `business_site`.`access_tokens` ON `access_tokens`.`user_id`=`user`.`user_id` ";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			if (conn != null) {
				boolean active = false;
				if (accountId == null) {
					if (phone == null) {
						pstmt = conn.prepareStatement(sql + " WHERE `user`.`email`=?  ");
						pstmt.setString(1, email);
					} else {
						pstmt = conn.prepareStatement(sql + " WHERE `user`.`phone`=?  ");
						pstmt.setString(1, phone);
					}
				} else {
					pstmt = conn.prepareStatement(sql + " WHERE `user`.`user_id`=?  ");
					pstmt.setString(1, accountId);
				}

				rs = pstmt.executeQuery();
				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						active = rs.getBoolean("active");
						email = rs.getString("email");
						phone = rs.getString("phone");
						thumbPic = rs.getString("thumb_pic");
						pic = rs.getString("pic");
						accountType.put("type", rs.getInt("user_type"));
						accountType.put("name", rs.getString("type_name"));
						about = rs.getString("about_me");
						accountId = rs.getString("user_id");
						name = rs.getString("name");

					}
					return new UserRepresentation(active, accountId, name, accountType, email, phone, thumbPic, pic,
							about, error);
				} else {
					return null;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}

	/**
	 * Sends an email.
	 *
	 * @param subject
	 * @param message
	 * @param recipientEmail
	 * @return
	 */

	public boolean sendResetPasswordEmail(String passwordHash, String site, String recipientEmail, String ccEmail) {
		String summary = "<html><body>" + "Dear user, <br>"
				+ "We have received a request to change your Account password. If it "
				+ "was not you that sent us the request, ignore this email and delete it as soon as possible."
				+ "<br><br>" + "Otherwise, click on the link below to create a new password.<br><br>" + "<a href=\""
				+ site + "password/reset?source=" + passwordHash + "\" target=\"_blank\" rel=\"nofollow\">" + site
				+ "resources/user/details/change/email/?token=" + passwordHash + "</a>"
				+ "<strong>Please note that the link is only valid for 3 days</strong>" + "<br><br>"
				+ "<br /><br />Kind Regards<br /> " + site + "Administrators";

		String message = getEmailTemplate();

		message = message.replace("{summary}", summary);
		message = message.replace("{subject}", "Password Reset");

		return sendEmailNotification("Password Reset", message, recipientEmail, ccEmail);
	}

	public String getEmailTemplate() {
		String emailTemplate = "";
		BufferedReader reader = null;
		try {
			String path = "/com/bunshin/api/utils/email_template.html";

			reader = new BufferedReader(new FileReader(new File(getClass().getResource(path).toURI())));

			String temp = null;
			while ((temp = reader.readLine()) != null) {
				emailTemplate += temp;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}

		return emailTemplate;
	}

	/**
	 * Sends beautiful HTML-themed email notifications using the default email
	 * template.
	 *
	 * @param subject
	 * @param emailMessage
	 * @param recipientEmail
	 *            Returns boolean value whether the email was sent
	 */
	public boolean sendEmailNotification(String subject, String emailMessage, String recipientEmail, String ccEmail) {
		try {
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "mail.sematime.com");// TODO change this
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props, new DefaultAuthenticator("accounts@sematime.com",
					"#@?PZGPcHJ}Z"));

			Message message = new MimeMessage(session);
			message.setRecipient(RecipientType.TO, new InternetAddress(recipientEmail));
			if (ccEmail != null)
				message.setRecipient(RecipientType.CC, new InternetAddress(ccEmail));
			message.setFrom(new InternetAddress("info@bunshin.com", "Bunshin"));
			message.setSubject(subject);

			MimeBodyPart summaryBodyPart = new MimeBodyPart();
			summaryBodyPart.setText(emailMessage);
			summaryBodyPart.setContent(emailMessage, "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(summaryBodyPart);

			message.setContent(multipart);

			Transport.send(message);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public ArrayList<String> getAllNumbers() {
		ArrayList<String> numbers = new ArrayList<String>();
		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(dbName);

			conn = ds.getConnection();

			if (conn != null) {

				stmt = conn.createStatement();
				rs = stmt.executeQuery("SELECT phone FROM `business_site`.user ");
				while (rs.next()) {
					numbers.add(rs.getString("phone"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (stmt != null)

					stmt.close();
				stmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return numbers;
	}

	public Date scheduleJob(String expression, String jobName, Date endsDate, String userId) {
		try {
			if (scheduler == null)
				scheduler = new StdSchedulerFactory().getScheduler("MyScheduler");

			CronTrigger trigger = newTrigger()
					.withIdentity(jobName, userId)
					.withSchedule(
							CronScheduleBuilder.cronSchedule(expression).withMisfireHandlingInstructionFireAndProceed())
					.endAt(endsDate).build();

			JobDetail jobDetail = newJob(ScheduledEventsHandler.class).withIdentity(jobName, userId).build();

			Date scheduleDate = scheduler.scheduleJob(jobDetail, trigger);
			System.out.println("SCHEDULLING");
			return scheduleDate;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public Date reScheduleJob(String expression, String jobName, Date endsDate, String userId) {
		try {
			if (scheduler == null)
				scheduler = new StdSchedulerFactory().getScheduler("MyScheduler");

			CronTrigger trigger = newTrigger().withIdentity(jobName, userId)
					.withSchedule(CronScheduleBuilder.cronSchedule(expression)).endAt(endsDate).build();

			Date scheduleDate = scheduler.rescheduleJob(new TriggerKey(jobName, userId), trigger);
			System.out.println("RESCHEDULLING");
			return scheduleDate;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public boolean unScheduleJob(String jobName, String userId) {
		try {
			if (scheduler == null)
				scheduler = new StdSchedulerFactory().getScheduler("MyScheduler");

			System.out.println("UNSCHEDULLING");
			return scheduler.unscheduleJob(new TriggerKey(jobName, userId));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public HashMap<String, Object> suspendedAuthentication(String sessionKey) {
		HashMap<String, Object> res = new HashMap<String, Object>();
		Account account = null;
		if (null == sessionKey || !sessionKey.startsWith("Basic")) {
			res.put("false", "{" + createErrorResponse(Status.NOT_FOUND, "Authentication credentials are required"));

			return res;
		}

		sessionKey = sessionKey.replaceFirst("[B|b]asic", "");
		String[] values = Base64.decodeToString(sessionKey).split(":");
		if (values.length < 2 || values.length > 2) {
			res.put("false", "{"
					+ createErrorResponse(Status.BAD_REQUEST, "INvalid Syntax for username and sessionKey"));

			return res;
		}
		String username = values[0];
		String token = values[1];
		if ((null == username) || (token == null)) {
			res.put("false", "{" + createErrorResponse(Status.BAD_REQUEST, "Missing user name or session Key"));

			return res;
		}

		try {
			account = getAccount(token, username);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res.put("false",
					"{"
							+ createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops it seems something went wrong, please try again later"));
		}
		res.put("true", account);
		return res;
	}

	public boolean setAppointmentsDone(String appointmentsId) {
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		try {

			ds = MyDataSource.INSTANCE.getDataSource(dbName);

			conn = ds.getConnection();
			if (conn != null) {
				conn.setAutoCommit(false);
				stmt = conn
						.prepareStatement("UPDATE `business_site`.`appointments` SET done=true WHERE appointments_id=? ");

				stmt.setString(1, appointmentsId);

				if (stmt.executeUpdate() >= 1) {
					conn.commit();

					return true;
				} else {
					conn.rollback();
					return false;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			exceptionThrown = true;
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			log.error(e.getMessage());

		} finally {
			try {

				if (exceptionThrown) {
					conn.rollback();
				}
				conn.setAutoCommit(true);

				if (stmt != null)

					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {

				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
				log.error(e.getMessage());
			}
		}

		return false;

	}

	public boolean setNotification(String id, String userId, String message, String apiCall, String response, int type,
			DefaultBroadcaster df) {
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		try {

			ds = MyDataSource.INSTANCE.getDataSource(dbName);

			conn = ds.getConnection();
			if (conn != null) {
				conn.setAutoCommit(false);
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`notification` VALUES (?,?,?,?,?)");

				stmt.setString(1, id);
				stmt.setString(2, userId);
				stmt.setString(3, message);
				stmt.setString(4, apiCall);
				stmt.setInt(5, type);

				if (stmt.executeUpdate() >= 1) {
					conn.commit();

					df.broadcast(response);

					return true;
				} else {
					conn.rollback();
					return false;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			exceptionThrown = true;
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			log.error(e.getMessage());

		} finally {
			try {

				if (exceptionThrown) {
					conn.rollback();
				}
				conn.setAutoCommit(true);

				if (stmt != null)

					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {

				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
				log.error(e.getMessage());
			}
		}

		return false;

	}

	public boolean sendEmail(String subject, String emailMessage, String recipientEmail, String bccEmail,
			String ccEmail, String from, boolean attachment, JSONArray attachments, String userEmail) {
		Folder sent = null;
		Store store = null;
		File tempFile = null;
		try {
			String host = "mail.sematime.com";
			String password = "#@?PZGPcHJ}Z";
			String fromName = "Barber";
			if (host == null || password == null) {
				return false;
			}
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", "587");

			// deals with bounced messages
			props.put("mail.smtp.from", userEmail);

			Session session = Session.getInstance(props, new DefaultAuthenticator("accounts@sematime.com", password));

			store = getStore(host, userEmail, password);
			store.connect();
			Folder[] folders = store.getDefaultFolder().list("*");
			for (Folder fold : folders) {
				if (fold.getName().equalsIgnoreCase("sent")) {
					sent = fold;
				}
			}
			if (sent == null) {
				sent = (Folder) store.getFolder("Sent");
				if (!sent.exists()) {
					sent.create(Folder.HOLDS_MESSAGES);
				}
			}

			sent.open(Folder.READ_WRITE);
			System.out.println("folder: " + sent);
			Message message = new MimeMessage(session);
			if (recipientEmail != null && !recipientEmail.isEmpty()) {
				message.setRecipients(RecipientType.CC, InternetAddress.parse(recipientEmail, false));
			}
			if (ccEmail != null && !ccEmail.isEmpty()) {
				message.setRecipients(RecipientType.CC, InternetAddress.parse(ccEmail, false));

			}
			if (bccEmail != null && !bccEmail.isEmpty()) {
				message.setRecipients(RecipientType.CC, InternetAddress.parse(bccEmail, false));

			}

			message.setFrom(new InternetAddress(from, fromName));
			if (subject != null && !subject.isEmpty()) {
				message.setSubject(subject);
			}

			MimeBodyPart summaryBodyPart = new MimeBodyPart();
			summaryBodyPart.setText(emailMessage);
			summaryBodyPart.setContent(emailMessage, "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(summaryBodyPart);

			AmazonS3 s3Client = new AmazonS3Client(new ProfileCredentialsProvider());
			if (attachments != null && attachments.length() != 0) {
				for (int index = 0; index < attachments.length(); index++) {

					String fileName = attachments.getJSONObject(index).getString("file_name");
					String albumId = attachments.getJSONObject(index).getString("album_id");
					String key = fileName.replace("http://s3.amazonaws.com/barber-document/", "");
					// String key=fileName.replace("s3.amazonaws.com/", "");
					S3Object object = s3Client.getObject(new GetObjectRequest("barber-document", key));
					InputStream objectData = object.getObjectContent();
					// Process the objectData stream.

					tempFile = File.createTempFile("barber-email" + System.currentTimeMillis() + "-", ".tmp");
					// tempFile.createNewFile();
					tempFile.deleteOnExit();
					try (OutputStream outpuStream = new FileOutputStream(tempFile)) {
						IOUtils.copy(objectData, outpuStream);
					}

					// TODO: SAVE FILE HERE

					// if you want media type for validation, it's
					// field.getMediaType()

					System.out.println("file: " + tempFile + "\n exists: " + tempFile.exists());
					if (attachment && tempFile.exists()) {
						System.out.println("attahcment");
						summaryBodyPart = new MimeBodyPart();
						javax.activation.DataSource source = new FileDataSource(tempFile);
						summaryBodyPart.setDataHandler(new DataHandler(source));
						summaryBodyPart.setFileName(key.replace(albumId + "/", ""));
						multipart.addBodyPart(summaryBodyPart);

					}
					if (objectData != null) {
						objectData.close();
					}
				}
			}
			message.setContent(multipart);

			Transport.send(message);
			message.setFlag(Flag.SEEN, true);
			sent.appendMessages(new Message[] { message });
			return true;
			// append to sent folder

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				tempFile = null;
				if (sent != null) {
					sent.close(false);
					sent = null;
				}
				if (store != null) {
					store.close();
					store = null;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return false;
	}

	private Store getStore(String host, String userEmail, String password) throws javax.mail.NoSuchProviderException {
		Properties props = new Properties();
		System.out.println("password: " + password);
		// server setting
		props.put("mail.imap.host", host);
		props.put("mail.imap.port", "993");

		props.put("mail.imap.partialfetch", "false");

		// SSL setting
		props.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.imap.socketFactory.fallback", "false");
		props.setProperty("mail.imap.socketFactory.port", String.valueOf("993"));

		Session session = Session.getInstance(props, new DefaultAuthenticator(userEmail, password));
		return session.getStore("imap");

	}
}
