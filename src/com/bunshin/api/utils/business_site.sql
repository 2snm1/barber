SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `business_site` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `business_site` ;

-- -----------------------------------------------------
-- Table `business_site`.`user_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`user_type` (
  `user_type_id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`user_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`user` (
  `user_id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `user_type` VARCHAR(45) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `hash` VARCHAR(255) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `last_login` VARCHAR(45) NOT NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `thumb_pic` VARCHAR(1000) NULL,
  `pic` VARCHAR(1000) NULL,
  `about_me` VARCHAR(255) NULL,
  `email_active` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`),
  INDEX `fk_user_user_type1_idx` (`user_type` ASC),
  CONSTRAINT `fk_user_user_type1`
    FOREIGN KEY (`user_type`)
    REFERENCES `business_site`.`user_type` (`user_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`business`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`business` (
  `business_id` VARCHAR(45) NOT NULL,
  `location` VARCHAR(255) NULL,
  `name` VARCHAR(255) NOT NULL,
  `avi` VARCHAR(1000) NULL,
  `creator_id` VARCHAR(45) NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`business_id`),
  INDEX `fk_business_info_user_idx` (`creator_id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  CONSTRAINT `fk_business_info_user`
    FOREIGN KEY (`creator_id`)
    REFERENCES `business_site`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`album_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`album_type` (
  `album_type_id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`album_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`services` (
  `services_id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`services_id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`album`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`album` (
  `album_id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(255) NULL,
  `active` TINYINT(1) NULL DEFAULT 1,
  `creator_id` VARCHAR(45) NOT NULL,
  `album_type` VARCHAR(45) NOT NULL,
  `service_id` VARCHAR(45) NOT NULL,
  `avi` VARCHAR(1000) NULL,
  PRIMARY KEY (`album_id`),
  INDEX `fk_album_user1_idx` (`creator_id` ASC),
  INDEX `fk_album_album_type1_idx` (`album_type` ASC),
  INDEX `fk_album_services1_idx` (`service_id` ASC),
  CONSTRAINT `fk_album_user1`
    FOREIGN KEY (`creator_id`)
    REFERENCES `business_site`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_album_album_type1`
    FOREIGN KEY (`album_type`)
    REFERENCES `business_site`.`album_type` (`album_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_album_services1`
    FOREIGN KEY (`service_id`)
    REFERENCES `business_site`.`services` (`services_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`image` (
  `image_id` VARCHAR(45) NOT NULL,
  `album_id` VARCHAR(45) NOT NULL,
  `description` VARCHAR(100) NULL,
  `name` VARCHAR(255) NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `creator_id` VARCHAR(45) NOT NULL,
  `link` VARCHAR(1000) NOT NULL,
  `thumb_link` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`image_id`),
  INDEX `fk_images_album1_idx` (`album_id` ASC),
  INDEX `fk_image_user1_idx` (`creator_id` ASC),
  CONSTRAINT `fk_images_album1`
    FOREIGN KEY (`album_id`)
    REFERENCES `business_site`.`album` (`album_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_image_user1`
    FOREIGN KEY (`creator_id`)
    REFERENCES `business_site`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`business_services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`business_services` (
  `business_id` VARCHAR(45) NOT NULL,
  `services_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`business_id`, `services_id`),
  INDEX `fk_business_services_services1_idx` (`services_id` ASC),
  CONSTRAINT `fk_business_services_services1`
    FOREIGN KEY (`services_id`)
    REFERENCES `business_site`.`services` (`services_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_business_services_business_info1`
    FOREIGN KEY (`business_id`)
    REFERENCES `business_site`.`business` (`business_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`access_tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`access_tokens` (
  `access_tokens_id` VARCHAR(45) NOT NULL,
  `validity_period` VARCHAR(45) NOT NULL,
  `token` VARCHAR(1000) NOT NULL,
  `user_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`access_tokens_id`),
  INDEX `fk_access_tokens_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_access_tokens_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `business_site`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`reset_passwords`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`reset_passwords` (
  `reset_id` VARCHAR(45) NOT NULL,
  `hash` VARCHAR(255) NOT NULL,
  `validity_period` VARCHAR(45) NOT NULL,
  `done` TINYINT(1) NOT NULL DEFAULT 0,
  `user_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`reset_id`),
  INDEX `fk_reset_passwords_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_reset_passwords_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `business_site`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`repeat_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`repeat_type` (
  `repeat_type_id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`repeat_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`appointments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`appointments` (
  `appointments_id` VARCHAR(45) NOT NULL,
  `business_id` VARCHAR(45) NOT NULL,
  `service_id` VARCHAR(45) NOT NULL,
  `user_id` VARCHAR(45) NOT NULL,
  `time` VARCHAR(45) NOT NULL,
  `repeat_frequency` INT NOT NULL DEFAULT 0,
  `description` VARCHAR(255) NULL,
  `appointment_time` VARCHAR(45) NOT NULL,
  `done` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`appointments_id`),
  INDEX `fk_appointments_business_info1_idx` (`business_id` ASC),
  INDEX `fk_appointments_services1_idx` (`service_id` ASC),
  INDEX `fk_appointments_user1_idx` (`user_id` ASC),
  INDEX `fk_appointments_repeat_type1_idx` (`repeat_frequency` ASC),
  CONSTRAINT `fk_appointments_business_info1`
    FOREIGN KEY (`business_id`)
    REFERENCES `business_site`.`business` (`business_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_appointments_services1`
    FOREIGN KEY (`service_id`)
    REFERENCES `business_site`.`services` (`services_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_appointments_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `business_site`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_appointments_repeat_type1`
    FOREIGN KEY (`repeat_frequency`)
    REFERENCES `business_site`.`repeat_type` (`repeat_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`notification_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`notification_type` (
  `notification_type_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`notification_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `business_site`.`notification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `business_site`.`notification` (
  `notification_id` VARCHAR(45) NOT NULL,
  `user_id` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `api_call` VARCHAR(255) NOT NULL,
  `type` INT NOT NULL,
  PRIMARY KEY (`notification_id`, `user_id`),
  INDEX `fk_notification_user1_idx` (`user_id` ASC),
  INDEX `fk_notification_notification_type1_idx` (`type` ASC),
  CONSTRAINT `fk_notification_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `business_site`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_notification_notification_type1`
    FOREIGN KEY (`type`)
    REFERENCES `business_site`.`notification_type` (`notification_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

GRANT INSERT,DELETE,INDEX,UPDATE,SELECT ON `business_site`.* TO 'customer'@'%'IDENTIFIED BY 'dbCu$t0m3r';
GRANT CREATE ON *.* TO 'customer'@'%' IDENTIFIED BY 'dbCu$t0m3r';
REVOKE DROP,ALTER ON `business_site`.* FROM 'customer'@'%';

-- for quartz scheduling

CREATE TABLE IF NOT EXISTS  QRTZ_JOB_DETAILS(
SCHED_NAME VARCHAR(120) NOT NULL,
JOB_NAME VARCHAR(200) NOT NULL,
JOB_GROUP VARCHAR(200) NOT NULL,
DESCRIPTION VARCHAR(250) NULL,
JOB_CLASS_NAME VARCHAR(250) NOT NULL,
IS_DURABLE VARCHAR(1) NOT NULL,
IS_NONCONCURRENT VARCHAR(1) NOT NULL,
IS_UPDATE_DATA VARCHAR(1) NOT NULL,
REQUESTS_RECOVERY VARCHAR(1) NOT NULL,
JOB_DATA BLOB NULL,
PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
JOB_NAME VARCHAR(200) NOT NULL,
JOB_GROUP VARCHAR(200) NOT NULL,
DESCRIPTION VARCHAR(250) NULL,
NEXT_FIRE_TIME BIGINT(13) NULL,
PREV_FIRE_TIME BIGINT(13) NULL,
PRIORITY INTEGER NULL,
TRIGGER_STATE VARCHAR(16) NOT NULL,
TRIGGER_TYPE VARCHAR(8) NOT NULL,
START_TIME BIGINT(13) NOT NULL,
END_TIME BIGINT(13) NULL,
CALENDAR_NAME VARCHAR(200) NULL,
MISFIRE_INSTR SMALLINT(2) NULL,
JOB_DATA BLOB NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
REFERENCES QRTZ_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_SIMPLE_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
REPEAT_COUNT BIGINT(7) NOT NULL,
REPEAT_INTERVAL BIGINT(12) NOT NULL,
TIMES_TRIGGERED BIGINT(10) NOT NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_CRON_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
CRON_EXPRESSION VARCHAR(120) NOT NULL,
TIME_ZONE_ID VARCHAR(80),
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_SIMPROP_TRIGGERS
  (          
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    STR_PROP_1 VARCHAR(512) NULL,
    STR_PROP_2 VARCHAR(512) NULL,
    STR_PROP_3 VARCHAR(512) NULL,
    INT_PROP_1 INT NULL,
    INT_PROP_2 INT NULL,
    LONG_PROP_1 BIGINT NULL,
    LONG_PROP_2 BIGINT NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR(1) NULL,
    BOOL_PROP_2 VARCHAR(1) NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_BLOB_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
BLOB_DATA BLOB NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
INDEX (SCHED_NAME,TRIGGER_NAME, TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_CALENDARS (
SCHED_NAME VARCHAR(120) NOT NULL,
CALENDAR_NAME VARCHAR(200) NOT NULL,
CALENDAR BLOB NOT NULL,
PRIMARY KEY (SCHED_NAME,CALENDAR_NAME))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_PAUSED_TRIGGER_GRPS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_FIRED_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
ENTRY_ID VARCHAR(95) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
INSTANCE_NAME VARCHAR(200) NOT NULL,
FIRED_TIME BIGINT(13) NOT NULL,
SCHED_TIME BIGINT(13) NOT NULL,
PRIORITY INTEGER NOT NULL,
STATE VARCHAR(16) NOT NULL,
JOB_NAME VARCHAR(200) NULL,
JOB_GROUP VARCHAR(200) NULL,
IS_NONCONCURRENT VARCHAR(1) NULL,
REQUESTS_RECOVERY VARCHAR(1) NULL,
PRIMARY KEY (SCHED_NAME,ENTRY_ID))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_SCHEDULER_STATE (
SCHED_NAME VARCHAR(120) NOT NULL,
INSTANCE_NAME VARCHAR(200) NOT NULL,
LAST_CHECKIN_TIME BIGINT(13) NOT NULL,
CHECKIN_INTERVAL BIGINT(13) NOT NULL,
PRIMARY KEY (SCHED_NAME,INSTANCE_NAME))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS  QRTZ_LOCKS (
SCHED_NAME VARCHAR(120) NOT NULL,
LOCK_NAME VARCHAR(40) NOT NULL,
PRIMARY KEY (SCHED_NAME,LOCK_NAME))
ENGINE=InnoDB;

DELIMITER $$

DROP PROCEDURE IF EXISTS `business_site`.`CreateIndex` $$
CREATE PROCEDURE `business_site`.`CreateIndex`
(
    given_database VARCHAR(64),
    given_table    VARCHAR(64),
    given_index    VARCHAR(100),
    given_columns  VARCHAR(100)
)
BEGIN

    DECLARE IndexIsThere INTEGER;

    SELECT COUNT(1) INTO IndexIsThere
    FROM INFORMATION_SCHEMA.STATISTICS
    WHERE table_schema = given_database
    AND   table_name   = given_table
    AND   index_name   = given_index;

    IF IndexIsThere = 0 THEN
        SET @sqlstmt = CONCAT('CREATE INDEX ',given_index,' ON ',
        given_database,'.',given_table,' (',given_columns,')');
        PREPARE st FROM @sqlstmt;
        EXECUTE st;
        DEALLOCATE PREPARE st;
    ELSE
        SELECT CONCAT('Index ',given_index,' already exists on Table ',
        given_database,'.',given_table) CreateindexErrorMessage;   
    END IF;

END $$

DELIMITER ;

call createIndex('business_site','QRTZ_JOB_DETAILS','IDX_QRTZ_J_REQ_RECOVERY','SCHED_NAME,REQUESTS_RECOVERY');
call createIndex('business_site','QRTZ_JOB_DETAILS','IDX_QRTZ_J_GRP','SCHED_NAME,JOB_GROUP');



call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_J','SCHED_NAME,JOB_NAME,JOB_GROUP');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_JG','SCHED_NAME,JOB_GROUP');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_C','SCHED_NAME,CALENDAR_NAME');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_G','SCHED_NAME,TRIGGER_GROUP');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_STATE','SCHED_NAME,TRIGGER_STATE');
call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_N_STATE','SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_STATE');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_N_G_STATE','SCHED_NAME,TRIGGER_GROUP,TRIGGER_STATE');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_NEXT_FIRE_TIME','SCHED_NAME,NEXT_FIRE_TIME');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_NFT_ST','SCHED_NAME,TRIGGER_STATE,NEXT_FIRE_TIME');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_NFT_MISFIRE','SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_NFT_ST_MISFIRE','SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_STATE');

call createIndex('business_site','QRTZ_TRIGGERS','IDX_QRTZ_T_NFT_ST_MISFIRE_GRP','SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_GROUP,TRIGGER_STATE');


call createIndex('business_site','QRTZ_FIRED_TRIGGERS','IDX_QRTZ_FT_TRIG_INST_NAME','SCHED_NAME,INSTANCE_NAME');

call createIndex('business_site','QRTZ_FIRED_TRIGGERS','IDX_QRTZ_FT_INST_JOB_REQ_RCVRY','SCHED_NAME,INSTANCE_NAME,REQUESTS_RECOVERY');

call createIndex('business_site','QRTZ_FIRED_TRIGGERS','IDX_QRTZ_FT_J_G','SCHED_NAME,JOB_NAME,JOB_GROUP');

call createIndex('business_site','QRTZ_FIRED_TRIGGERS','IDX_QRTZ_FT_JG','SCHED_NAME,JOB_GROUP');

call createIndex('business_site','QRTZ_FIRED_TRIGGERS','IDX_QRTZ_FT_T_G','SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP');

call createIndex('business_site','QRTZ_FIRED_TRIGGERS','IDX_QRTZ_FT_TG','SCHED_NAME,TRIGGER_GROUP');

INSERT IGNORE INTO `user_type`(`user_type_id`,`name`) VALUES (1,'Administrator'),(2,'User');
INSERT IGNORE INTO `album_type`(`album_type_id`,`name`) VALUES (1,'Business'),(2,'Personal');
INSERT IGNORE INTO `services`(`services_id`,`name`,`description`) VALUES (1,'Personal','For Personal Stuff');
INSERT IGNORE INTO `notification_type`(`notification_type_id`,`name`) VALUES (1,'Appointment'),(2,'Group'),(3,'Email'),(4,'Comment'),(5,'Message');
INSERT IGNORE  INTO `repeat_type`(`repeat_type_id`,`name`) VALUES (0,'NONE'),(1,'DAILY'),(2,'WEEKLY'),(3,'MONTHLY'),(4,'YEARLY');


SET SQL_SAFE_UPDATES = 0;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
COMMIT;
