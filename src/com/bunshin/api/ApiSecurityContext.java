package com.bunshin.api;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import com.bunshin.api.Account;

public class ApiSecurityContext implements SecurityContext {

	private Account account;

	public ApiSecurityContext(Account acc) {
		this.account = acc;

	}

	@Override
	public String getAuthenticationScheme() {

		return SecurityContext.BASIC_AUTH;
	}

	@Override
	public Principal getUserPrincipal() {

		return (Principal) account;

	}

	@Override
	public boolean isSecure() {
		return (null != account.getAccountId()) ? true : false;

	}

	@Override
	public boolean isUserInRole(String dev) {
		if (!account.getAccountType().get("name").equals(dev)) {
			return false;
		} else {
			return true;
		}

	}

}
