package com.bunshin.api.messagequeues;

import java.io.IOException;
import java.io.Serializable;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Connection;
import com.bunshin.api.EndPoint;

public class Producer extends EndPoint {
	AMQP.BasicProperties props=null;
	public Producer(Connection connection, String endPointName, String exchangeType,
			String exchange, AMQP.BasicProperties props) throws IOException {
		super( connection,endPointName, exchangeType, exchange);
		this.props=props;
	}

	public void publishMessage(Serializable object) {
		try {

			//			channel.basicPublish(exchange, endPointName, true, true, null,
//					SerializationUtils.serialize(object));
			channel.basicPublish(exchange, endPointName, props,
					SerializationUtils.serialize(object));
			System.out.println("in publish queue, message sent "+object.toString());
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
