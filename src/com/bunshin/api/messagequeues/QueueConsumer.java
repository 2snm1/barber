package com.bunshin.api.messagequeues;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.SerializationUtils;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.script.ScriptService;

import com.bunshin.api.EndPoint;
import com.bunshin.api.NodeOperation;
import com.bunshin.api.utils.Utils;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

public class QueueConsumer extends EndPoint implements Runnable, Consumer {
	private volatile ShutdownSignalException _shutdown;
	private volatile ConsumerCancelledException _cancelled;

	private volatile String _consumer_Tag;
	private Utils utils = new Utils();
	private boolean autoAck;

	public QueueConsumer(Connection connection, String endPointName, String exchangeType, String exchange,
			boolean autoAck) throws IOException {
		super(connection, endPointName, exchangeType, exchange);
		this.autoAck = autoAck;
	}

	@Override
	public void run() {
		try {
			// declare a queue for the channel
			String queueName = channel.queueDeclare(exchange, true, false, false, null).getQueue();
			channel.queueBind(queueName, exchange, endPointName);
			channel.basicConsume(queueName, autoAck, this);
			System.out.println("in queue consumer");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void handleCancel(String consumerTag) throws IOException {
		_cancelled = new ConsumerCancelledException();

	}

	@Override
	public void handleCancelOk(String consumerTag) {
		// TODO Auto-generated method stub

	}

	// when consumer is registered
	@Override
	public void handleConsumeOk(String consumerTag) {
		this._consumer_Tag = consumerTag;
		System.out.println("registered" + _consumer_Tag);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties props, byte[] body)
			throws IOException {
		String routingKey = envelope.getRoutingKey();
		// String contentType = props.getContentType();
		long deliveryTag = envelope.getDeliveryTag();
		boolean status = false;
		Map<String, Object> details = (HashMap<String, Object>) SerializationUtils.deserialize(body);
		// do processing here
		System.out.println("in handle delivery" + routingKey);
		if (routingKey.equalsIgnoreCase("DELETE")) {

			System.out.println("delete");
			status = deleteIndex((String) details.get("index_name"), (String) details.get("type_name"),
					(String) details.get("id"));
		}
		if (routingKey.equalsIgnoreCase("INPUT")) {
			System.out.println("input");
			status = inputIndex((String) details.get("index_name"), (String) details.get("type_name"),
					(String) details.get("id"), (String) details.get("source"));
		}
		if (routingKey.equalsIgnoreCase("UPDATE")) {

			System.out.println("update");
			status = updateIndex((String) details.get("index_name"), (String) details.get("type_name"),
					(String) details.get("id"), (String) details.get("source"));
		}
		if (routingKey.equalsIgnoreCase("PARTIAL")) {

			System.out.println("partial");
			status = updatePartialIndex((String) details.get("index_name"), (String) details.get("type_name"),
					(String) details.get("id"), (HashMap<String, String>) details.get("partial"));
		}
		if (routingKey.equalsIgnoreCase("DELETE PARTIAL")) {

			System.out.println("del partial");
			status = deletePartialIndex((String) details.get("index_name"), (String) details.get("type_name"),
					(String) details.get("id"), (HashMap<String, String>) details.get("partial"));
		}
		if (routingKey.equalsIgnoreCase("ADD PARTIAL")) {
			System.out.println("add partial");
			status = updateAddPartialIndex((String) details.get("index_name"), (String) details.get("type_name"),
					(String) details.get("id"), (HashMap<String, String>) details.get("partial"));
		}
		/*
		 * if (routingKey.equalsIgnoreCase("EMAIL")) {
		 * System.out.println("add partial");
		 * utils.getNotificationMessage((String) details.get("host"), (String)
		 * details.get("email"), (String) details.get("password"), (String)
		 * details.get("folder_name"), (DefaultBroadcaster)
		 * details.get("broadcaster"), (String) details.get("user_id"), (String)
		 * details.get("school_id")); }
		 */
		System.out.println("message satus: " + status);
		if (status) {
			// if true
			channel.basicAck(deliveryTag, false);
		} else {
			// if false
			channel.basicNack(deliveryTag, false, true);
		}

	}

	public String getConsumerTag() {
		return _consumer_Tag;
	}

	@Override
	public void handleRecoverOk(String consumerTag) {

	}

	@Override
	public void handleShutdownSignal(String consumerTag, ShutdownSignalException sig) {
		_shutdown = sig;
		// TODO add ways of getting errors, logs

	}

	private boolean deleteIndex(String indexName, String typeName, String id) {

		if (isIndexExist(indexName)) {
			System.out.println("INDEX EXISTS>>>DELETE INDEX");
			return NodeOperation.INSTANCE.getClient().prepareDelete(indexName, typeName, id).execute().actionGet()
					.isFound();
		} else {
			return false;
		}

	}

	private boolean inputIndex(String indexName, String typeName, String id, String source) {
		if (isTypeExist(indexName, typeName)) {
			System.out.println("INDEX EXISTS>>>INPUTTING");
			return NodeOperation.INSTANCE.getClient().prepareIndex(indexName, typeName, id).setSource(source).execute()
					.actionGet().isCreated();
		} else {
			return false;
		}
	}

	private boolean updateIndex(String indexName, String typeName, String id, String source) {
		if (isIndexExist(indexName)) {
			System.out.println("INDEX EXISTS>>>UPDATING");
			return NodeOperation.INSTANCE.getClient().prepareUpdate(indexName, typeName, id).setDoc(source).execute()
					.actionGet().isCreated();
		} else {
			return false;
		}
	}

	private boolean updatePartialIndex(String indexName, String typeName, String id, HashMap<String, String> partials) {
		if (isIndexExist(indexName)) {
			System.out.println("INDEX EXISTS>>>UPDATING PARTIAL");

			String script = "";
			for (String name : partials.keySet()) {
				script += "ctx._source." + name + "=\"" + partials.get(name) + "\";";
			}

			return NodeOperation.INSTANCE.getClient().prepareUpdate(indexName, typeName, id)
					.setScript(script, ScriptService.ScriptType.INLINE).get().isCreated();

		} else {
			return false;
		}
	}

	private boolean updateAddPartialIndex(String indexName, String typeName, String id, HashMap<String, String> partials) {

		if (isIndexExist(indexName)) {

			System.out.println("INDEX EXISTS>>>UPDATEW ADD PARTIAL");
			String script = "";
			UpdateRequest request = new UpdateRequest(indexName, typeName, id);
			int index = 0;
			for (String name : partials.keySet()) {
				script += "ctx._source." + name + " += new_services_" + index + ";";
				request.addScriptParam("new_services_" + index, partials.get(name));
				index++;
			}

			try {
				return NodeOperation.INSTANCE.getClient()
						.update(request.script(script, ScriptService.ScriptType.INLINE)).get().isCreated();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}

	private boolean deletePartialIndex(String indexName, String typeName, String id, HashMap<String, String> partials) {

		if (isIndexExist(indexName)) {

			System.out.println("INDEX EXISTS>>>DELETE PARTIAL");
			String script = "";
			UpdateRequest request = new UpdateRequest(indexName, typeName, id);
			int index = 0;
			for (String name : partials.keySet()) {
				script += "ctx._source.services.remove(old_services_" + index + ");";
				request.addScriptParam("old_services_" + index, partials.get(name));
				index++;
			}
			try {
				return NodeOperation.INSTANCE.getClient()
						.update(request.script(script, ScriptService.ScriptType.INLINE)).get().isCreated();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}

	private boolean isTypeExist(String indexName, String typeName) {
		System.out.println("IS TYPE EXIST?");
		return NodeOperation.INSTANCE.getClient().admin().indices().prepareTypesExists(indexName).setTypes(typeName)
				.execute().actionGet().isExists();
	}

	private boolean isIndexExist(String indexName) {
		System.out.println("IS INDEX EXIST?");
		return NodeOperation.INSTANCE.getClient().admin().indices().prepareExists(indexName).execute().actionGet()
				.isExists();
	}
}
