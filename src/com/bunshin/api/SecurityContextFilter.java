package com.bunshin.api;

import java.io.IOException;
import java.util.Optional;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.Ostermiller.util.Base64;
import com.bunshin.api.Account;
import com.bunshin.api.utils.Utils;
import com.bunshin.api.ApiSecurityContext;

@Priority(Priorities.AUTHENTICATION)
public class SecurityContextFilter implements ContainerRequestFilter {

	private Utils utils = new Utils();

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		Account account = null;
		try {

			String sessionKey = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
			if (null == sessionKey || !sessionKey.startsWith("Basic")) {
				requestContext
						.abortWith(Response
								.status(Status.NOT_FOUND)
								.entity(
										 utils.createErrorResponse(Status.NOT_FOUND,
												"Authentication credentials are required") ).build());
			}

			sessionKey = sessionKey.replaceFirst("[B|b]asic", "");
			String[] values = Base64.decodeToString(sessionKey).split(":");
			if (values.length < 2 || values.length > 2) {
				requestContext.abortWith(Response
						.status(Status.BAD_REQUEST)
						.entity(utils.createErrorResponse(Status.BAD_REQUEST,
										"INvalid Syntax for username and sessionKey") ).build());
			}
			String userId = values[0];// the userId
			String token = values[1];
			Optional userIdNull = Optional.ofNullable(userId);
			Optional tokenNull = Optional.ofNullable(token);
			if (!tokenNull.isPresent() || !userIdNull.isPresent()) {
				requestContext.abortWith(Response
						.status(Status.BAD_REQUEST)
						.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Missing user id or session Key")
								).build());
			}

			account = utils.getAccount(token, userId);

			Optional<Account> nullAcc = Optional.ofNullable(account);
			if (!nullAcc.isPresent()) {
				requestContext.abortWith(Response
						.status(Status.NOT_FOUND)
						.entity( utils.createErrorResponse(Status.NOT_FOUND,
										"The Session Key specified is not valid. Please log in again") ).build());
			}
		/*	if (account.isActive() == false) {
				account = null;
				requestContext
						.abortWith(Response
								.status(Status.NOT_FOUND)
								.entity("{"
										+ utils.createErrorResponse(Status.NOT_FOUND,
												"Your account has been deactivated. Please contact the Sematime team for more information")
										).build());
			}*/
			if (Long.parseLong(account.getSessionId()) + (long) (1000 * 60 * 60 * 24 * 7 * 4 * 3) > System
					.currentTimeMillis()) {
				utils.deleteToken(account.getSessionId(),null);
				requestContext.abortWith(Response
						.status(Status.NOT_FOUND)
						.entity( utils.createErrorResponse(Status.NOT_FOUND,
										"Your token has expired. Please log in again") ).build());
			}

		} catch (Exception e) {

			e.printStackTrace();
			requestContext.abortWith(Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops there seems something went wrong ") ).build());
		}

		requestContext.setSecurityContext(new ApiSecurityContext(account));

	}

}
