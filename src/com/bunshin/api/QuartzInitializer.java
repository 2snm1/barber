package com.bunshin.api;

import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class QuartzInitializer implements ServletContextListener {
	private Scheduler scheduler = null;
	private ComboPooledDataSource cp = null;

	public void contextDestroyed(ServletContextEvent arg0) {
		System.out
				.println("Context destroyed - now shutting down scheduler and elasticsearch instance.");
		try{ 
		Enumeration<Driver> drivers = DriverManager.getDrivers();     

	        Driver driver = null;

	        // clear drivers
	        while(drivers.hasMoreElements()) {
	                driver = drivers.nextElement();
	                DriverManager.deregisterDriver(driver);
	        }

//	         MySQL driver leaves around a thread. This static method cleans it up.
//	            AbandonedConnectionCleanupThread.shutdown();

			NodeOperation.INSTANCE.shutdown();
			MyDataSource.INSTANCE.close();
		if (scheduler != null) {
			
				scheduler.shutdown();

			
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void contextInitialized(ServletContextEvent arg0) {
		System.out
				.println("Context initialized - now starting scheduler. and Elastic search instance");

		try {
			Properties properties = new Properties();
			properties.load(getClass().getResourceAsStream(
					"/com/bunshin/api/quartz.properties"));

			NodeOperation.INSTANCE.getClient();
			RabbitConnectionSend.INSTANCE
			.getConnection();
			RabbitConnectionRecieve.INSTANCE
			.getConnection();

/*			DBConnectionManager dbmgr = DBConnectionManager.getInstance();
			dbmgr.addConnectionProvider("", new ConnectionProvider() {

				@Override
				public void shutdown() throws SQLException {
					cp.close();

				}

				@Override
				public void initialize() throws SQLException {

					cp = new ComboPooledDataSource();
					cp.setJdbcUrl("jdbc:mysql://localhost:3306/business_site");
					cp.setPassword("");
//					cpds.setPassword("r00tAW$1");
					cp.setUser("root");

				}

				@Override
				public Connection getConnection() throws SQLException {
					return cp.getConnection();
				}
			});
*/
			scheduler = new StdSchedulerFactory(properties).getScheduler();
			System.out.println("Scheduler name:\t"
					+ scheduler.getSchedulerName());

			scheduler.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
