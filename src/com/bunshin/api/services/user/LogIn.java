package com.bunshin.api.services.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.mindrot.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.services.user.representation.UserRepresentation;
import com.bunshin.api.utils.Utils;
import com.google.gson.Gson;
import com.sematime.api.json.JSONObject;

@Path("resources/user/logIn/")
@Consumes({ MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON })
@Produces(MediaType.APPLICATION_JSON)
public class LogIn {
	private Account acc = null;
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(LogIn.class);
	private Gson gson = new Gson();
private String error=null;
	@Consumes(MediaType.APPLICATION_JSON)
	@SecurityCheck
	@GET
	@Path("check")
	public UserRepresentation check(@Context SecurityContext sc) {
		acc = (Account) sc.getUserPrincipal();
		System.out.println("check");
		return new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc.getAccountType(),
				acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc.getAbout(), error);
	}

	@PermitAll
	@POST
	public Response logIn(@FormParam("json") final String json) {
		String email = "";
		String phoneNumber = "";
		String password = null;
		String pic = null;
		String thumbPic = null;
		String name = null;
		Map<String, Object> accountType = null;

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("email")) {
				email = object.getString("email").trim();
				if (email != null) {
					if (!utils.isValidEmail(email)) {
						Response res = Response
								.status(Status.BAD_REQUEST)
								.entity("{\"error\":"
										+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid email address ")
										+ "}").cacheControl(utils.cacheControl()).build();

						return res;
					}
				}
			}
			if (object.keySet().contains("phone_number")) {
				phoneNumber = utils.sanitizeNumber(object.getString("phone_number").trim());

				if (phoneNumber != null) {
					if (!utils.isValidNumber(phoneNumber)) {
						Response res = Response
								.status(Status.BAD_REQUEST)
								.entity("{\"error\":"
										+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid phone number ") + "}")
								.cacheControl(utils.cacheControl()).build();

						return res;
					}
				}
			}

			password = object.getString("password").trim();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			Response res = Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();

			return res;

		}
		if (email == null && phoneNumber == null || email.isEmpty() && phoneNumber.isEmpty()) {
			log.error(Status.BAD_REQUEST + "Provide either an email address or phone number for login ");
			Response res = Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST,
									"Provide either an email address or phone number for login ") + "}")
					.cacheControl(utils.cacheControl()).build();

			return res;
		}
		if (password == null || password.isEmpty()) {
			log.error(Status.BAD_REQUEST + "Provide a password ");
			Response res = Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST,
									"Password cannot be empty/null . Provide a password ") + "}")
					.cacheControl(utils.cacheControl()).build();

			return res;
		}
		Connection conn = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		DataSource ds = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {

				String hash = null;
				String loggedDate = null;
				String accountId = null;
				boolean emailActive;
				String about = null;

				if (!email.isEmpty() && email != null) {
					pstmt = conn
							.prepareStatement("SELECT user.about_me,user.user_type,user_type.name AS type_name,user.email,user.phone,user.name,user.thumb_pic,user.pic,user.hash,user.last_login,user.user_id,user.active "
									+ " FROM `business_site`.user "
									+ " INNER JOIN `business_site`.user_type ON user_type.user_type_id=user.user_type "
									+ " WHERE email =? ");

					pstmt.setString(1, email);
					rs = pstmt.executeQuery();
				} else {
					pstmt = conn
							.prepareStatement("SELECT user.about_me,user.user_type,user_type.name AS type_name,user.email,user.phone,user.name,user.thumb_pic,user.pic,user.hash,user.last_login,user.user_id,user.active "
									+ " FROM `business_site`.user "
									+ " INNER JOIN `business_site`.user_type ON user_type.user_type_id=user.user_type "
									+ " WHERE phone =? ");

					pstmt.setString(1, phoneNumber);
					System.out.println(pstmt.toString());
					rs = pstmt.executeQuery();
				}

				if (rs.isBeforeFirst()) {
					if (rs.next()) {
						System.out.println("rs\n" + rs);
						hash = rs.getString("hash");
						loggedDate = rs.getString("last_login");
						accountId = rs.getString("user_id");
						emailActive = rs.getBoolean("active");
						email = rs.getString("email");
						pic = rs.getString("pic");
						thumbPic = rs.getString("thumb_pic");
						phoneNumber = rs.getString("phone");
						name = rs.getString("name");
						accountType = new HashMap<>();
						accountType.put("id", rs.getInt("user_type"));
						accountType.put("name", rs.getString("type_name"));
						about = rs.getString("about_me");
						if (accountId == null) {
							Response res = Response
									.status(Status.INTERNAL_SERVER_ERROR)
									.entity("{\"error\":"
											+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
													"Server error: please try to log in again") + "}")
									.cacheControl(utils.cacheControl()).build();

							return res;
						}

						if (BCrypt.checkpw(password, hash) == true) {
							long time = System.currentTimeMillis();
							if (time > Long.parseLong(loggedDate.trim())) {
								String token = utils.setToken(accountId);

								Response res = Response
										.ok(new UserRepresentation(emailActive, accountId, name, accountType, email,
												phoneNumber, thumbPic, pic, about, error))
										.header("TOKEN", token).header("USER_ID", accountId)

										.cacheControl(utils.cacheControl()).build();
								return res;
							} else {
								Response res = Response
										.status(Status.PRECONDITION_FAILED)
										.entity("{\"user\":{ \"logged in\" : \"false\" ,\"reason\" : {\"timeout\" : "
												+ (time - Long.parseLong(loggedDate)) + "}} ,\"error\" :  "
												+ utils.createErrorResponse(Status.PRECONDITION_FAILED, "timed out")
												+ "}").cacheControl(utils.cacheControl()).build();

								return res;

							}

						} else {
							Response res = Response
									.status(Status.NOT_FOUND)
									.entity("{\"user\":{ \"logged in\" : \"false\" },\"error\" : { "
											+ utils.createErrorResponse(Status.NOT_FOUND,
													"no user with given credentials exists - incorrect password and/or email")
											+ "}").cacheControl(utils.cacheControl()).build();

							return res;
						}

					} else {
						Response res = Response
								.status(Status.NOT_FOUND)
								.entity("{\"user\":{ \"logged in\" : \"false\" ,\"error\" :  "
										+ utils.createErrorResponse(Status.NOT_FOUND,
												"no user with given credentials exists - incorrect password and/or email")
										+ "}}").cacheControl(utils.cacheControl()).build();

						return res;
					}
				} else {
					Response res = Response
							.status(Status.NOT_FOUND)
							.entity("{\"user\":{ \"logged in\" : \"false\"} ,\"error\" :  "
									+ utils.createErrorResponse(Status.NOT_FOUND,
											"no user with given credentials exists - incorrect password and/or email")
									+ "}").cacheControl(utils.cacheControl()).build();

					return res;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			Response res = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Server error: please try to log in again") + "}")
					.cacheControl(utils.cacheControl()).build();

			return res;
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		Response res = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"error\":"
						+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: please try to log in again") + "}").cacheControl(utils.cacheControl())
				.build();

		return res;

	}

}
