package com.bunshin.api.services.user;

import java.net.URI;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import org.atmosphere.config.service.AtmosphereService;

import com.bunshin.api.Account;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.utils.Utils;

@Path("resources/user/")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces( MediaType.APPLICATION_JSON )
public class LogOut {

	private Account acc = null;
	private Utils utils = new Utils();
	@Context
	UriInfo info;
	private URI path = null;

	// @SecurityCheck
	@DELETE
	@Path("logOut")
	public Response logOut(@Context SecurityContext sc) {

		acc = (Account) sc.getUserPrincipal();
		path = info.getBaseUri();
		javax.ws.rs.core.Link self = javax.ws.rs.core.Link.fromUri(path + "resources/user/logOut/${entity.map.id}")
				.rel("self").title("log out").type("DELETE").build();
		javax.ws.rs.core.Link logIn = javax.ws.rs.core.Link.fromUri(path + "resources/user/logOut").rel("logIn")
				.title("log in").type("POST").build();
		DataSource ds = null;
		Connection conn = null;

		PreparedStatement updateStmt = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);

			conn = ds.getConnection();

			if (conn != null) {
				String sql = "DELETE FROM `business_site`.`access_tokens` WHERE `access_tokens_id`=? AND user_id=? ";
				updateStmt = conn.prepareStatement(sql);
				updateStmt.setString(1, acc.getSessionId());
				updateStmt.setString(2, acc.getAccountId());

				int rowAffected = updateStmt.executeUpdate();

				if (rowAffected > 0) {

					return Response
							.ok("{\"user\" : \"" + acc.getAccountId() + "\",\time\" : \"" + System.currentTimeMillis()
									+ "\",\"log out\" : \"true\"}").cacheControl(utils.cacheControl())
							.links(self, logIn).build();
				} else {
					return Response
							.status(Status.NOT_FOUND)
							.entity("{\"user\" : \"" + acc.getAccountId() + "\",\time\" : \""
									+ System.currentTimeMillis() + "\",\"log out\" : \"false\",\"error\" :"
									+ utils.createErrorResponse(Status.NOT_FOUND, "user does not exist ") + "}")
							.cacheControl(utils.cacheControl()).links(self, logIn).build();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (updateStmt != null)

					updateStmt.close();
				updateStmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"user\" : \"" + acc.getAccountId() + "\",\time\" : \"" + System.currentTimeMillis()
						+ "\",\"log out\" : \"false\",\"error\" :{"
						+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR, "Server error: ") + "}}")
				.cacheControl(utils.cacheControl()).links(self, logIn).build();

	}

}
