package com.bunshin.api.services.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.mindrot.BCrypt;

import com.bunshin.api.services.user.representation.UserRepresentation;
import com.bunshin.api.utils.Utils;
import com.bunshin.api.MyDataSource;
import com.sematime.api.json.JSONObject;

@Path("/resources/user/forgotPassword")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class ForgotPassword {

	private Utils utils = new Utils();

	@POST
	public Response forgotPassword(@FormParam("json") final String json, @HeaderParam("SITE") final String site) {
		String email = null;
		String phoneNumber = null;
		UserRepresentation user = null;
		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("email")) {

				email = object.getString("email").trim();
				user = utils.getUserRepresentation(null, null, email);
				if (user == null) {
					throw new Exception("No user is associated with the given email address");
				}

			} else {

				phoneNumber = utils.sanitizeNumber(object.getString("phone_number").trim());
				user = utils.getUserRepresentation(null, utils.sanitizeNumber(phoneNumber), null);
				if (user == null) {
					throw new Exception("No user is associated with the given phone number");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

			Response.status(Status.BAD_REQUEST)
					.entity(user)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}

		String passwordHash = null;

		if (email != null && !email.isEmpty()) {
			passwordHash = getPasswordHashEmail(email);
			if (passwordHash == null) {
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity("{\"error\":"
								+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
										"Server error: There is no user associated with that email account") + "}")
						.cacheControl(utils.cacheControl()).build();

			}
			if (utils.sendResetPasswordEmail(passwordHash, site, email, null) == true) {
				String time = String.valueOf(System.currentTimeMillis());

				DataSource ds = null;
				PreparedStatement insertStmt = null;
				Connection conn = null;
				try {
					ds = (DataSource) MyDataSource.INSTANCE.getDataSource(utils.dbName);

					conn = ds.getConnection();

					if (conn != null) {

						insertStmt = conn
								.prepareStatement("INSERT INTO `business_site`.reset_passwords VALUES (?, ?, ?, ?,?)");
						insertStmt.setString(1, time);

						insertStmt.setString(5, user.getAccountId());
						insertStmt.setString(2, passwordHash);
						insertStmt.setString(3, String.valueOf(System.currentTimeMillis() + ((long) (1000 * 60 * 60))));
						insertStmt.setBoolean(4, false);

						if (insertStmt.executeUpdate() == 1) {
							utils.deleteToken(null,user.getAccountId());
							return Response.ok(user).cacheControl(utils.cacheControl()).build();

						}

					}
				} catch (Exception e) {
					e.printStackTrace();
					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(user)
							.entity("{\"error\":"
									+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
											"Server error: There is no user associated with that email account") + "}")
							.cacheControl(utils.cacheControl()).build();
				} finally {
					try {

						if (insertStmt != null)

							insertStmt.close();

						if (conn != null)

							conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return Response
								.status(Status.INTERNAL_SERVER_ERROR)
								.entity(user)
								.entity("{\"error\":"
										+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
												"Server error: " + e.getLocalizedMessage()) + "}")
								.cacheControl(utils.cacheControl()).build();
					}
				}

			} else {
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(user)
						.entity("{\"error\":"
								+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR, "Server error: ") + "}")
						.cacheControl(utils.cacheControl()).build();

			}
		} else {
			// TODO generate code to be used
			passwordHash = getPasswordHashPhone(phoneNumber);
			if (passwordHash == null) {
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(user)
						.entity("{\"error\":"
								+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
										"Server error: There is no user associated with that phone number") + "}")
						.cacheControl(utils.cacheControl()).build();

			}

			if (passwordHash != null) {

				try {
					String hashed = utils.getPasswordHash(passwordHash);
					System.out.println("hash: " + hashed);
					// add to db reset password then it'll be checked from there
					if (setHash(hashed, user.getAccountId())) {
						// TODO check whether this is needed
						/*
						 * if (!utils.sendMessage("This is your temporary key: "
						 * + hashed, phoneNumber)) { throw new
						 * Exception("could not send message"); } else {
						 * 
						 * return Response.ok(
						 * "{\"user\": \"unknown\",\"status\" : \"true\"}")
						 * .cacheControl(utils.cacheControl()).build(); }
						 */} else {
						throw new Exception("could not set hash");
					}

				} catch (Exception e) {
					e.printStackTrace();
					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity("{\"user\": \"unknown\",\"error\":"
									+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
											"Oops it seems something went wrong please try again.") + "}")
							.cacheControl(utils.cacheControl()).build();
				}
			}
		}
		// TODO create method to send message through phone and add it below

		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"user\": \"unkown\",\"error\":"
						+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR, "Server error: ") + "}")
				.cacheControl(utils.cacheControl()).build();
	}

	private String getPasswordHashEmail(String emailAddress) {
		DataSource ds = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);

			conn = ds.getConnection();

			if (conn != null) {

				String sql = "SELECT * FROM `business_site`.user WHERE email =?";
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, emailAddress);

				rs = stmt.executeQuery();
				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						return rs.getString("hash");
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				rs = null;
				if (stmt != null)
					stmt.close();
				stmt = null;
				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	private String getPasswordHashPhone(String phone) {

		DataSource ds = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);

			conn = ds.getConnection();

			if (conn != null) {

				String sql = "SELECT * FROM `business_site`.user " + " WHERE user.phone =?";
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, phone);

				rs = stmt.executeQuery();
				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						return rs.getString("hash");
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				rs = null;
				if (stmt != null)
					stmt.close();
				stmt = null;
				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	private boolean setHash(String password, String accountId) {
		DataSource ds = null;
		Connection conn = null;
		boolean excetionThrown = false;
		PreparedStatement updateStmt = null;
		PreparedStatement setDone = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);

			conn = ds.getConnection();

			if (conn != null) {
				conn.setAutoCommit(false);
				setDone = conn.prepareStatement("UPDATE `business_site`.reset_passwords SET done=? WHERE user_id=?");
				setDone.setBoolean(1, true);
				setDone.setString(2, accountId);

				updateStmt = conn.prepareStatement("INSERT INTO `business_site`.reset_passwords VALUES (?, ?, ?, ?,?)");
				updateStmt.setString(1, String.valueOf(System.currentTimeMillis()));

				updateStmt.setString(5, accountId);
				updateStmt.setString(2, BCrypt.hashpw(password, BCrypt.gensalt(13)));
				updateStmt
						.setString(3, String.valueOf(System.currentTimeMillis() + ((long) (1000 * 60 * 60 * 24 * 3))));
				updateStmt.setBoolean(4, false);

				if (setDone.executeUpdate() >= 0 && updateStmt.executeUpdate() > 0) {
					conn.commit();
					return true;
				} else {
					conn.rollback();
					return false;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			excetionThrown = true;
		} finally {
			try {
				if (excetionThrown) {
					conn.rollback();
				}
				conn.setAutoCommit(true);
				if (setDone != null)

					setDone.close();
				setDone = null;
				if (updateStmt != null)

					updateStmt.close();
				updateStmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

		return false;
	}

}
