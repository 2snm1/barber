package com.bunshin.api.services.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.services.business.service.representation.ServiceRepersentation;
import com.bunshin.api.services.user.representation.UserTypeRepresentation;
import com.bunshin.api.services.user.representation.UserTypesRepresentation;
import com.bunshin.api.utils.Utils;
import com.sematime.api.json.JSONObject;

@Path("resources/user/type")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class UserType {
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(UserType.class);
	private String error = null;

	@DenyAll
	@SecurityCheck
	@POST
	public Response createUserType(@Context SecurityContext sc, @FormParam("json") final String json) {
		String name = null;

		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		if (name == null) {
			log.error(Status.BAD_REQUEST + "No name given for the user type ");
			return Response
					.status(Status.BAD_REQUEST)
					.entity(utils
							.createErrorResponse(
									Status.BAD_REQUEST,
									"User type name missing .The user type should be accompanied with a name and group id, a description is also advised but not necessary "))
					.cacheControl(utils.cacheControl()).build();
		}

		String now = String.valueOf(System.currentTimeMillis());
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`user_type` VALUES (?,?)");
				stmt.setString(1, now);
				stmt.setString(2, name);

				if (stmt.executeUpdate() == 1) {
					return Response.ok(new UserTypeRepresentation(now, name, error)).cacheControl(utils.cacheControl())
							.build();
				} else {

					error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to register, please try again.");
					return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserTypeRepresentation())
							.cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to create user type, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserTypeRepresentation())
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to create user type, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserTypeRepresentation())
						.cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to create user type, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserTypeRepresentation())
				.cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getUserTypes(@Context SecurityContext sc) {
		// returns a list of all user types

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		List<UserTypeRepresentation> list = new ArrayList<UserTypeRepresentation>();
		int modelLimit = 0;
		UserTypesRepresentation response = new UserTypesRepresentation();

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				// TODO edit get count
				stmt = conn
						.prepareStatement("SELECT user_type.user_type_id,user_type.name,COUNT(cnt.user_type_id) AS nos "
								+ " FROM `business_site`.`user_type` "
								+ " LEFT JOIN (SELECT user_type_id FROM `business_site`.`user_type` ) cnt "
								+ " ON user_type.user_type_id IN (SELECT user_type_id FROM `business_site`.`user_type` ) "
								+ " GROUP BY user_type.user_type_id ");
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						modelLimit = rs.getInt("nos");
						list.add(new UserTypeRepresentation(rs.getString("user_type_id"), rs.getString("name"), error));
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any user type categories ");
					return Response.status(Status.NOT_FOUND).entity(list).cacheControl(utils.cacheControl()).build();
				}
				response = new UserTypesRepresentation(list, 0, modelLimit, modelLimit);
				return Response.ok(response).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get user type, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to get user type, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)

				.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get user type, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
				.build();
	}

	@SecurityCheck
	@DELETE
	@Path("{typeId}")
	@DenyAll
	public Response deleteUserType(@Context SecurityContext sc, @PathParam("typeId") final String typeId) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("DELETE FROM `business_site`.`user_type` WHERE `user_type_id` = ?");
				stmt.setString(1, typeId);

				if (stmt.executeUpdate() == 1) {

					return Response.ok(new UserTypeRepresentation()).cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any user type with that id");
					return Response.status(Status.NOT_FOUND).entity(new UserTypeRepresentation(typeId, null, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to delete user type , please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new UserTypeRepresentation(typeId, null, error)).cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to delete user type , please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserTypeRepresentation(typeId, null, error)).cacheControl(utils.cacheControl())
						.build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete user type , please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserTypeRepresentation(typeId, null, error))
				.cacheControl(utils.cacheControl()).build();

	}

	@SecurityCheck
	@GET
	@Path("{typeId}")
	public Response getUserType(@Context SecurityContext sc, @PathParam("typeId") final String typeId,
			@PathParam("userId") final String userId) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("SELECT * FROM `business_site`.`user_type` WHERE  `user_type_id`=?");
				stmt.setString(1, typeId);
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						return Response
								.ok(new UserTypeRepresentation(rs.getString("user_type_id"), rs.getString("name"),
										error)).cacheControl(utils.cacheControl()).build();

					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any user type categories with such an id");
					return Response.status(Status.NOT_FOUND).entity(new UserTypeRepresentation(typeId, null, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get user type, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new UserTypeRepresentation(typeId, null, error)).cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to get user type, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserTypeRepresentation(typeId, null, error)).cacheControl(utils.cacheControl())
						.build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get user type, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserTypeRepresentation(typeId, null, error))
				.cacheControl(utils.cacheControl()).build();
	}

	@PermitAll
	@SecurityCheck
	@DenyAll
	@PUT
	@Path("{typeId}")
	public Response editUserType(@Context SecurityContext sc, @PathParam("typeId") final String typeId,
			@FormParam("json") String json) {
		String name = null;

		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("UPDATE `business_site`.`user_type` SET name=?  WHERE  user_type_id=?");
				stmt.setString(1, name);
				stmt.setString(2, typeId);

				if (stmt.executeUpdate() == 1) {
					return Response.ok(new UserTypeRepresentation(typeId, name, error))
							.cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any user type categories ");
					return Response.status(Status.NOT_FOUND).entity(new UserTypeRepresentation(typeId, name, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to delete user type group, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new UserTypeRepresentation(typeId, name, error)).cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to delete user type group, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserTypeRepresentation(typeId, name, error)).cacheControl(utils.cacheControl())
						.build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete user type group, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserTypeRepresentation(typeId, name, error))
				.cacheControl(utils.cacheControl()).build();

	}

}
