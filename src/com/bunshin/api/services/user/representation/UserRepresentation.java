package com.bunshin.api.services.user.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.Binding;
import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLink.Style;
import org.glassfish.jersey.linking.InjectLinks;

import com.bunshin.api.services.user.LogOut;
import com.bunshin.api.services.user.Register;
import com.bunshin.api.services.user.User;
import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "user")
@InjectLinks({
		@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/user/${instance.accountId}", rel = "get", title = "Get Specific User"),
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/user/forgotPassword/?json=", rel = "forgot password", title = "Forgot Password"),
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/user/${instance.accountId}/?json=", rel = "create", title = "Add User"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/details/change/unknown/password/?json=", rel = "edit password (unknown)", title = "Edit password from unknown user"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/details/change/${instance.accountId}/password/?json=", rel = "edit password", title = "Edit User Password"),
		@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/user/details/change/email/?token=", rel = "get userId from token", title = "Get UserId From token in email"),
		@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/user/details/change/phone/${instance.phone}", rel = "get userId from phone", title = "Get UserId From phone number"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/details/change/${instance.accountId}/email/?json=", rel = "edit email", title = "Edit User Email"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/${instance.accountId}/?json=", rel = "edit", title = "Edit User"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/user/${instance.accountId}", rel = "del", title = "Delete User") })
public class UserRepresentation {
	@XmlTransient
	// does not show on the JaXB represetation
	private boolean active;
	@XmlElement
	// accessible from the JaXB representation
	private String accountId = null;
	@XmlElement
	// accessible from the JaXB representation
	private String error = null;
	@XmlTransient
	private String name = null;
	@XmlTransient
	private Map<String, Object> accountType = null;
	@XmlTransient
	private String email = null;
	@XmlElement
	private String phone = null;
	@XmlTransient
	private String thumbPic = null;
	@XmlTransient
	private String pic = null;
	@XmlTransient
	private String about = null;

	@InjectLink(resource = Register.class, style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLink(resource = User.class, style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self2;

	@InjectLinks({
			@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/user/${instance.accountId}", rel = "get", title = "Get Specific User"),
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/user/forgotPassword/?json=", rel = "forgot password", title = "Forgot Password"),
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/user/${instance.accountId}/?json=", rel = "create", title = "Add User"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/details/change/unknown/password/?json=", rel = "edit password (unknown)", title = "Edit password from unknown user"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/details/change/${instance.accountId}/password/?json=", rel = "edit password", title = "Edit User Password"),
			@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/user/details/change/email/?token=", rel = "get userId from token", title = "Get UserId From token in email"),
			@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/user/details/change/phone/${instance.phone}", rel = "get userId from phone", title = "Get UserId From phone number"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/details/change/${instance.accountId}/email/?json=", rel = "edit email", title = "Edit User Email"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/${instance.accountId}/?json=", rel = "edit", title = "Edit User"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/user/${instance.accountId}", rel = "del", title = "Delete User") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public UserRepresentation() {

	}

	public UserRepresentation(boolean active, String accountId, String name, Map<String, Object> accountType,
			String email, String phone, String thumbPic, String pic, String about, String error) {
		this.active = active;
		this.accountId = accountId;
		this.name = name;
		this.accountType = accountType;
		this.email = email;
		this.phone = phone;
		this.thumbPic = thumbPic;
		this.error = error;
		this.pic = pic;
		this.about = about;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if(error!=null){
		 errorMap = new HashMap<>();
		JSONObject errorObject = new JSONObject(error);
		errorMap.put("status", errorObject.get("status"));
		errorMap.put("error message", errorObject.get("error message"));
		errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}

	@Override
	public String toString() {
		return "UserRepresentation [active=" + active + ", accountId=" + accountId + ", error=" + error + ", name="
				+ name + ", accountType=" + accountType + ", email=" + email + ", phone=" + phone + ", thumbPic="
				+ thumbPic + ", pic=" + pic + ", about=" + about + ", self=" + self + ", self2=" + self2 + ", links="
				+ links + "]";
	}

	public boolean isActive() {
		return active;
	}

	public String getAccountId() {
		return accountId;
	}

	public String getAbout() {
		return about;
	}

	public String getName() {
		return name;
	}

	public Map<String, Object> getAccountType() {
		return accountType;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getThumbPic() {
		return thumbPic;
	}

	public String getPic() {
		return pic;
	}

}
