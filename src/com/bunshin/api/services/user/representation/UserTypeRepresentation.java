package com.bunshin.api.services.user.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.user.UserType;
import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "user type")
@InjectLinks({
		@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/user/type/${instance.typeId}", rel = "get", title = "Get Specific User Type"),
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/user/type/${instance.typeId}/?json=", rel = "create", title = "Add User Type"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/type/${instance.typeId}/?json=", rel = "edit", title = "Edit User Type"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/user/type/${instance.typeId}", rel = "del", title = "Delete User Type") })
public class UserTypeRepresentation {
	@XmlElement
	// accessible from the JaXB representation
	private String typeId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String name = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String error = null;

	@InjectLink(resource = UserType.class, style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/user/type/${instance.typeId}", rel = "get", title = "Get Specific User Type"),
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/user/type/${instance.typeId}/?json=", rel = "create", title = "Add User Type"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/user/type/${instance.typeId}/?json=", rel = "edit", title = "Edit User Type"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/user/type/${instance.typeId}", rel = "del", title = "Delete User Type") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public UserTypeRepresentation() {

	}

	public UserTypeRepresentation(String typeId, String name, String error) {
		this.typeId = typeId;
		this.name = name;
		this.error = error;
	}

	public String getTypeId() {
		return typeId;
	}

	public String getName() {
		return name;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if(error!=null){
		 errorMap = new HashMap<>();
		JSONObject errorObject = new JSONObject(error);
		errorMap.put("status", errorObject.get("status"));
		errorMap.put("error message", errorObject.get("error message"));
		errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}

	@Override
	public String toString() {
		return "UserTypeRepresentation [typeId=" + typeId + ", name=" + name + ", error=" + error + ", self=" + self
				+ ", links=" + links + "]";
	}

}
