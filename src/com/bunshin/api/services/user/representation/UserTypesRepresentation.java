package com.bunshin.api.services.user.representation;

import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "user types")
@InjectLinks({
		@InjectLink(value = "/resources/user/type/?json=%7B%22offset%22:%22${instance.offset + instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
		@InjectLink(value = "/resources/user/type/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
public class UserTypesRepresentation {

	@XmlElement
	private List<UserTypeRepresentation> userTypes;
	@XmlTransient
	private int offset = 0;
	@XmlTransient
	private int limit = 0;
	private int modelLimit = 0;

	@InjectLink(type = "query", style = Style.ABSOLUTE, value = "/resources/user/type/?json={offset:${instance.offset},limit:${instance.limit}}", rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(value = "/resources/user/type/?json=%7B%22offset%22:%22${instance.offset + instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
			@InjectLink(value = "/resources/user/type/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public UserTypesRepresentation() {

	}

	public UserTypesRepresentation(List<UserTypeRepresentation> userTypes, int offset, int limit, int modelLimit) {
		this.userTypes = userTypes;
		this.offset = offset;
		this.limit = limit;
		this.modelLimit = modelLimit;
	}

	public List<UserTypeRepresentation> getUserTypes() {
		return userTypes;
	}

	public int getOffset() {
		return offset;
	}

	public int getLimit() {
		return limit;
	}

	public int getModelLimit() {
		return modelLimit;
	}

	@Override
	public String toString() {
		return "UserTypesRepresentation [userTypes=" + userTypes + ", offset=" + offset + ", limit=" + limit
				+ ", modelLimit=" + modelLimit + "]";
	}

}
