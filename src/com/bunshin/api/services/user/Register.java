package com.bunshin.api.services.user;

import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.atmosphere.config.service.AtmosphereService;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.mindrot.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.services.user.representation.UserRepresentation;
import com.bunshin.api.utils.Utils;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.NodeOperation;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.sematime.api.json.JSONObject;
import com.bunshin.api.utils.ScriptRunner;

@Path("/resources/user/register")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@AtmosphereService(dispatch = true, path = "/barber/resources/", servlet = "org.glassfish.jersey.servlet.ServletContainer")
public class Register {
	private static final int ADMIN = 0;
	private static final int USER = 1;
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(Register.class);
	private String error = null;

	@Path("user")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response registerUser(@FormParam("json") final String json) {
		String email = null;
		String phone = null;
		String password = null;
		String name = null;
		try {
			JSONObject object = new JSONObject(json);
			email = object.getString("email").trim();

			if (!email.isEmpty() && email != null) {
				if (!utils.isValidEmail(email)) {
					log.error(Status.BAD_REQUEST + "Invalid Email address ");
					throw new Exception("Invalid email Address");
				}
			}
			if (!utils.checkUserByEmail(email).isEmpty()) {
				log.error(Status.BAD_REQUEST + "Existing Email address ");
				throw new Exception(
						"The email address you've provided already existsw in the system, please select a new one");

			}

			phone = object.getString("phone_number").trim();

			if (!phone.isEmpty() && phone != null) {
				if (!utils.isValidNumber(phone)) {
					log.error(Status.BAD_REQUEST + "Invalid phone number ");
					throw new Exception(
							"The phone number you've provided already existsw in the system, please select a new one");
				}
			}

			if (!utils.checkUserByPhone(phone).isEmpty()) {

				log.error(Status.BAD_REQUEST + "Existing phone number ");
				throw new Exception("Existing phone number");
			}

			password = object.getString("password");
			name = object.getString("name");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		String time = String.valueOf(System.currentTimeMillis());
		HashMap<String, Object> user = new HashMap<String, Object>();
		user.put("phone", phone);
		user.put("password", password);
		user.put("email", email);
		user.put("name", name);
		user.put("time", time);

		Map<String, Object> response = insertUserData(user, USER);
		if ((Boolean) response.get("status")) {
			return Response.ok((UserRepresentation) response.get("response")).build();
		} else {
			// TODO check on adding an error message
			return Response.status((Status) response.get("code")).entity((UserRepresentation) response.get("response"))
					.entity(response.get("error")).build();
		}

	}

	@Path("admin")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response registerAdmin(@FormParam("json") final String json) {
		String email = null;
		String phone = null;
		String password = null;
		String name = null;
		try {
			JSONObject object = new JSONObject(json);
			email = object.getString("email").trim();

			if (!email.isEmpty() && email != null) {
				if (!utils.isValidEmail(email)) {
					log.error(Status.BAD_REQUEST + "Invalid Email address ");
					throw new Exception("Invalid email Address");
				}
			}

			phone = object.getString("phone_number").trim();

			if (!phone.isEmpty() && phone != null) {
				if (!utils.isValidNumber(phone)) {
					log.error(Status.BAD_REQUEST + "Invalid phone number ");
					throw new Exception(
							"The phone number you've provided already existsw in the system, please select a new one");
				}
			}

			name = object.getString("name");
			password = object.getString("password");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		String time = String.valueOf(System.currentTimeMillis());

		ScriptRunner runner = null;

		Connection conn = null;
		try {
			conn = MyDataSource.INSTANCE.getDataSource("main").getConnection();
			runner = new ScriptRunner(conn, false, true);
			runner.runScript(new InputStreamReader(getClass().getResourceAsStream(
					"/com/bunshin/api/utils/business_site.sql")));
			HashMap<String, String> user = utils.checkUserByPhone(phone);
			if (user != null && !user.isEmpty()) {
				return Response
						.status(Status.CONFLICT)
						.entity("{\"error\":"
								+ utils.createErrorResponse(Status.CONFLICT,
										"DUPLICATE ENTRY: There seems to be a user with the given credentials") + "}")
						.cacheControl(utils.cacheControl()).build();
			}
			if (!utils.checkUserByEmail(email).isEmpty()) {
				return Response
						.status(Status.CONFLICT)
						.entity("{\"error\":"
								+ utils.createErrorResponse(Status.CONFLICT,
										"DUPLICATE ENTRY: There seems to be a user with the given credentials") + "}")
						.cacheControl(utils.cacheControl()).build();
			}
			if (!createIndices("business_site")) {
				
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity("{\"error\":"
								+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
										"Oops it seems something went wrong please try again. ") + "}")
						.cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(Status.INTERNAL_SERVER_ERROR + "it seems something went wrong ");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops it seems something went wrong please try again. ") + "}")
					.cacheControl(utils.cacheControl()).build();
		} finally {

			try {
				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity("{\"error\":"
								+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
										"Oops it seems something went wrong please try again. ") + "}")
						.cacheControl(utils.cacheControl()).build();

			}
		}

		HashMap<String, Object> user = new HashMap<String, Object>();
		user.put("phone", phone);
		user.put("password", password);
		user.put("email", email);
		user.put("name", name);
		user.put("time", time);
		Map<String, Object> response = insertUserData(user, USER);
		if ((Boolean) response.get("status")) {
			return Response.ok((UserRepresentation) response.get("response")).build();
		} else {
			// TODO check on adding an error message
			return Response.status((Status) response.get("code")).entity((UserRepresentation) response.get("response"))
					.entity(response.get("error")).build();
		}

	}

	private HashMap<String, Object> insertUserData(HashMap<String, Object> userDetails, int type) {
		boolean exceptionThrown = false;
		DataSource ds = null;
		Connection conn = null;
		PreparedStatement userStatement = null;
		String phone = utils.sanitizeNumber(userDetails.get("phone").toString());
		String password = userDetails.get("password").toString();
		String email = userDetails.get("email").toString();
		String name = userDetails.get("name").toString();
		String userId = userDetails.get("time").toString();
		String hash = BCrypt.hashpw(password, BCrypt.gensalt(13));
		Map<String, Object> accountType = new HashMap<>();
		ds = com.bunshin.api.MyDataSource.INSTANCE.getDataSource(utils.dbName);
		try {
			conn = ds.getConnection();
			if (conn != null) {
				conn.setAutoCommit(false);
				switch (type) {
				case USER:
					accountType.put("id", USER);
					accountType.put("name", "user");

					break;
				case ADMIN:
					accountType.put("id", ADMIN);
					accountType.put("name", "admin");

					break;

				default:

					break;

				}

				userStatement = conn
						.prepareStatement("INSERT INTO business_site.user (user_id,name,user_type,email,hash,phone,last_login,active,email_active) "
								+ " VALUES ( ?, ?, ?, ?, ?,?,?,?,?) ");
				userStatement.setString(1, userId);
				userStatement.setString(2, name);
				userStatement.setInt(3, type);
				userStatement.setString(4, email);
				userStatement.setString(5, hash);
				userStatement.setString(6, phone);
				userStatement.setString(7, userId);
				userStatement.setBoolean(8, true);
				userStatement.setBoolean(9, false);

				if (userStatement.executeUpdate() >= 1) {
					HashMap<String, Object> response = new HashMap<String, Object>();
					response.put("status", true);
					response.put("response", new UserRepresentation(false, userId, name, accountType, email, phone,
							null, null, null, error));
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.currentTimeMillis())).type("custom");
					HashMap<String, Object> user = new HashMap<String, Object>();
					user.put("index_name", "business_site");
					user.put("type_name", "users");
					user.put("source", "{" + "\"user_id\":\"" + userId + "\"," + "\"user_type\":\"" + type + "\","
							+ "\"email\":\"" + email + "\"," + "\"phone\":\"" + phone + "\"," + "\"name\":\"" + name
							+ "\"," + "\"active\":\"" + true + "\"," + "\"about_me\":\"" + null + "\","
							+ "\"email_active\":\"" + false + "\"," + "\"photo_link\":\"" + null + "\","
							+ "\"thumb_link\":\"" + null + "\"}");

					utils.consumeMessage("INPUT", "direct", "search", props.build(), true);
					utils.queueMessage("INPUT", "direct", "search", props.build(), user);
					conn.commit();
					return response;

				} else {
					HashMap<String, Object> response = new HashMap<String, Object>();
					error = utils.createErrorResponse(Status.CONFLICT,
							"DUPLICATE ENTRY: There seems to be a user with the given credentials");
					response.put("status", false);
					response.put("response", new UserRepresentation(false, null, null, null, null, null, null, null,
							null, error));
					response.put("error", error);
					response.put("code", Status.CONFLICT);
					return response;

				}

			}
		} catch (SQLIntegrityConstraintViolationException me) {

			exceptionThrown = true;
			me.printStackTrace();
			log.error(Status.CONFLICT + " Server error");
			HashMap<String, Object> response = new HashMap<String, Object>();
			response.put("status", false);
			error = utils.createErrorResponse(Status.CONFLICT,
					"DUPLICATE ENTRY: There seems to be a user with the given credentials");
			response.put("response", new UserRepresentation(false, null, null, null, null, null, null, null, null,
					error));
			response.put("error", error);
			response.put("code", Status.CONFLICT);
			return response;

		} catch (Exception e) {
			exceptionThrown = true;
			e.printStackTrace();
			log.error(Status.INTERNAL_SERVER_ERROR + " Server error");
			HashMap<String, Object> response = new HashMap<String, Object>();
			response.put("status", false);
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to register, please try again.");
			response.put("response", new UserRepresentation(false, null, null, null, null, null, null, null, null,
					error));
			response.put("error", error);
			response.put("code", Status.INTERNAL_SERVER_ERROR);
			return response;
		} finally {
			try {

				if (exceptionThrown) {

					conn.rollback();
				}

				conn.setAutoCommit(true);
				if (userStatement != null) {
					userStatement.close();
					userStatement = null;
				}
			} catch (SQLException e) {

				e.printStackTrace();
				log.error(e.getMessage());
				HashMap<String, Object> response = new HashMap<String, Object>();
				response.put("status", false);
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to register you, please try again.");
				response.put("response", new UserRepresentation(false, null, null, null, null, null, null, null, null,
						error));
				response.put("error", error);
				return response;

			}
		}
		HashMap<String, Object> response = new HashMap<String, Object>();
		response.put("status", false);
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to register you, please try again.");
		response.put("response", new UserRepresentation(false, null, null, null, null, null, null, null, null, error));
		response.put("error", error);
		return response;

	}

	private boolean createIndices(String indiceName) {
		boolean response = true;
		HashMap<String, String> indxData = new HashMap<String, String>();
		indxData.put(
				"business",
				"{\"business\": {\"_index\":{\"enabled\":\"true\"},"
						+ "\"_id\":{\"path\":\"business_id\"},"
						+ "\"dynamic\":\"strict\","
						+ "\"properties\":{"
						+ "\"business_id\":{\"type\":\"string\",\"store\":\"true\",\"index\":\"not_analyzed\"},"
						+ "\"status\":{\"type\":\"boolean\",\"store\":\"true\",\"index\":\"not_analyzed\"},"
						+ "\"services\":{\"type\":\"string\",\"store\":\"true\"},"
						+ "\"name\":{\"type\":\"string\",\"store\":\"true\",\"index_analyzer\":\"autocomplete\",\"search_analyzer\":\"standard\"},"
						+ "\"location\":{\"type\":\"string\",\"store\":\"true\",\"index_analyzer\":\"autocomplete\",\"search_analyzer\":\"standard\"},"
						+ "\"user_id\":{\"type\":\"string\",\"store\":\"true\",\"index\":\"not_analyzed\"}}}}");

		indxData.put(
				"users",
				"{\"users\":{\"_type\":{\"store\":\"true\"},"
						+ "\"_index\":{\"enabled\":\"true\"},"
						+ "\"_id\":{\"path\":\"user_id\"},"
						+ "\"dynamic\":\"strict\","
						+ "\"properties\":{"
						+ "\"user_id\":{\"type\":\"string\",\"store\":\"true\",\"index\":\"not_analyzed\"},"
						+ "\"user_type\":{\"type\":\"integer\",\"store\":\"true\",\"index\":\"not_analyzed\"},"
						+ "\"email\":{\"type\":\"string\",\"store\":\"true\",\"index_analyzer\":\"email_custom\",\"search_analyzer\":\"standard\"},"
						+ "\"phone\":{\"type\":\"string\",\"fields\":{\"raw\":{\"type\":\"string\",\"store\":\"true\",\"index_analyzer\":\"autocomplete\",\"search_analyzer\":\"standard\"}}},"
						+ "\"name\":{\"type\":\"string\",\"store\":\"true\",\"index_analyzer\":\"autocomplete\",\"search_analyzer\":\"standard\"},"
						+ "\"active\":{\"type\":\"boolean\",\"store\":\"true\",\"index\":\"not_analyzed\"},"
						+ "\"about_me\":{\"type\":\"string\",\"store\":\"true\"}, "
						+ "\"email_active\":{\"type\":\"boolean\",\"store\":\"true\",\"index\":\"not_analyzed\"},"
						+ "\"photo_link\":{\"type\":\"string\",\"store\":\"true\",\"index\":\"not_analyzed\"},"
						+ "\"thumb_link\":{\"type\":\"string\",\"store\":\"true\",\"index\":\"not_analyzed\"}}}}");
		indxData.put(
				"services",
				"{\"services\": {\"_type\":{\"store\":\"true\"},"
						+ "\"_index\":{\"enabled\":\"true\"},"
						+ "\"_id\":{\"path\":\"services_id\"},"
						+ "\"dynamic\":\"strict\","
						+ "\"properties\":{ \"services_id\":{\"type\":\"string\",\"store\":\"true\",\"index\":\"not_analyzed\"},"
						+ "\"name\":{\"type\":\"string\",\"store\":\"true\",\"index_analyzer\":\"autocomplete\",\"search_analyzer\":\"standard\"},"
						+ "\"description\":{\"type\":\"string\",\"store\":\"true\"} " + " }  } }");

		for (String key : indxData.keySet()) {

			response = createIndices(indiceName, key, indxData.get(key));
			System.out.println(key + " created: " + response);
			if (!response) {
				System.out.println("index not created");
				break;
			}
		}
		if (!response) {

			deleteIndex(indiceName);
		}

		return response;
	}

	private boolean createIndices(String indexName, String typeName, String mapping) {
		try {
			System.out.println(mapping + "\n type : " + typeName);
			if (!NodeOperation.INSTANCE.getClient().admin().indices().prepareExists(indexName).execute().actionGet()
					.isExists()) {
				System.out.println("CREATE INDEX: " + indexName);
				System.out.println("index does not exist, create it and : " + indexName);
				log.trace("CREATE INDEX: " + indexName);
				String settings = "{\"aliases\": { " + "\"content\": {}, \"business\": {}}," + "\"settings\":{"
						+ "\"index\":{" + "\"analysis\":{\"filter\":"
						+ "{\"autocomplete_filter\":{ \"type\" :\"edge_ngram\"," + "\"min_gram\":\"2\","
						+ "\"max_gram\":20" + " }" + "  },\"analyzer\" : {" + "\"autocomplete\" : { "
						+ "\"type\" :\"custom\"," + "\"tokenizer\":\"keyword\","
						+ "\"filter\":[\"lowercase\", \"autocomplete_filter\" ] } " + "," + "\"email_custom\" : { "
						+ "\"type\" :\"custom\"," + "\"tokenizer\":\"uax_url_email\","
						+ "\"filter\":[\"lowercase\", \"autocomplete_filter\" ] } " + "}" + " } " + "}}}";

				CreateIndexRequestBuilder irb = NodeOperation.INSTANCE.getClient().admin().indices()
						.prepareCreate(indexName).setSource(settings)/*
																	 * setSettings(
																	 * ImmutableSettings
																	 * .
																	 * settingsBuilder
																	 * ().
																	 * loadFromSource
																	 * (
																	 * settings)
																	 * )
																	 */;
				System.out.println(settings + "\n" + irb.toString());
				if (!irb.execute().actionGet().isAcknowledged()) {
					System.out.println("ERROR: did not create index");
					return false;
				}

			}

			if (!isTypeExist(indexName, typeName)) {
				System.out.println("type does not exist create it: " + typeName);
				PutMappingResponse res = NodeOperation.INSTANCE.getClient().admin().indices()
						.preparePutMapping(indexName).setType(typeName).setSource(mapping).setIndices(indexName)
						.execute().actionGet();
				return res.isAcknowledged();
			} else {
				return true;
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
	}

	private boolean deleteIndex(String indexName) {
		System.out.println("DELETING: " + indexName);
		return NodeOperation.INSTANCE.getClient().admin().indices().prepareDelete(indexName).execute().actionGet()
				.isAcknowledged();

	}

	private boolean isTypeExist(String indexName, String typeName) {

		return NodeOperation.INSTANCE.getClient().admin().indices().prepareTypesExists(indexName).setTypes(typeName)
				.execute().actionGet().isExists();
	}

}
