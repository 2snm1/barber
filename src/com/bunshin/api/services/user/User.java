package com.bunshin.api.services.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.services.business.service.representation.ServiceRepersentation;
import com.bunshin.api.services.business.service.representation.ServicesRepresentation;
import com.bunshin.api.services.user.User;
import com.bunshin.api.services.user.representation.UserRepresentation;
import com.bunshin.api.services.user.representation.UsersRepresentation;
import com.bunshin.api.utils.Utils;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.MyDataSource;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.sematime.api.json.JSONObject;

@Path("resources/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class User {

	private Utils utils = new Utils();
	private Account acc = null;
	private final Logger log = LoggerFactory.getLogger(User.class);
	private String error = null;

	@SecurityCheck
	@Path("{userId}/activate")
	@RolesAllowed("Administrator")
	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response activateUser(@Context SecurityContext sc, @PathParam("userId") final String userId) {

		acc = (Account) sc.getUserPrincipal();
		return statusUser(userId, true);
	}

	@SecurityCheck
	@Path("{userId}/deactivate")
	@RolesAllowed("Administrator")
	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response deactivateUser(@Context SecurityContext sc, @PathParam("userId") final String userId) {
		acc = (Account) sc.getUserPrincipal();
		return statusUser(userId, false);

	}

	@SecurityCheck
	@Path("{userId}/{userTypeId}/")
	@RolesAllowed("Administrator")
	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response changeUserType(@Context SecurityContext sc, @PathParam("userId") final String userId,
			@PathParam("userTypeId") final String userTypeId) {
		acc = (Account) sc.getUserPrincipal();
		return typeUser(userId, userTypeId);

	}

	@SecurityCheck
	@PermitAll
	@GET
	@Path("count")
	public Response getUserCount(@Context SecurityContext sc) {
		acc = (Account) sc.getUserPrincipal();

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = null;
		String response = "{\"user\" : [";
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				sql = "SELECT COUNT(admin.user_id) as admin_count,COUNT(users.user_id) AS user_count "
						+ " FROM `business_site`.user "
						+ " LEFT JOIN  (SELECT * FROM `business_site`.user WHERE user_type=1) AS admin ON user.user_type=admin.user_type "
						+ " LEFT JOIN  (SELECT * FROM `business_site`.user WHERE user_type=2) AS users ON user.user_type=users.user_type ";

				stmt = conn.prepareStatement(sql);
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						response += "{\"admin count\" : \"" + rs.getString("admin_count") + "\","
								+ "\"user count\" : \"" + rs.getString("user_count") + "\"},";

					}
				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND, "There is no user with the given credentials");
					return Response.status(Status.NOT_FOUND).entity("{\"user\": [{ }],\"error\" : {" + error + " }}")
							.cacheControl(utils.cacheControl()).build();
				}
				System.out.println(response);
				int last = response.lastIndexOf(",");
				return Response.ok(new StringBuilder(response).replace(last, last + 1, "").toString() + " ] }")
						.cacheControl(utils.cacheControl()).build();

			} else {
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to get users try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity("{\"user\": [{ }],\"error\" : {" + error + " }}").cacheControl(utils.cacheControl())
						.build();
			}

		} catch (Exception e) {

			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get users please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"user\": [{ }],\"error\" : {" + error + " }}").cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to get users please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity("{\"user\": [{ }],\"error\" : {" + error + " }}").cacheControl(utils.cacheControl())
						.build();

			}
		}
	}

	private Response statusUser(String userId, boolean status) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		if (acc.getAccountId().equals(userId)) {
			error = utils
					.createErrorResponse(
							Status.FORBIDDEN,
							"Sorry for the inconvenience but you cannot change your own status, please get another administrator to do that or contact the sematime team");
			return Response
					.status(Status.FORBIDDEN)
					.entity(new UserRepresentation(!status, acc.getAccountId(), acc.getName(), acc.getAccountType(),
							acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc.getAbout(), error))
					.cacheControl(utils.cacheControl()).build();

		}
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();

			if (conn != null) {
				stmt = conn.prepareStatement("UPDATE `business_site`.`user` SET active=? WHERE user_id= ?");
				stmt.setBoolean(1, status);
				stmt.setString(2, userId);

				if (stmt.executeUpdate() == 1) {
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");

					HashMap<String, Object> user = new HashMap<String, Object>();
					HashMap<String, String> partial = new HashMap<String, String>();

					user.put("index_name", "business_site");
					user.put("type_name", "users");
					user.put("id", userId);
					partial.put("active", String.valueOf(status));
					user.put("partial", partial);

					utils.consumeMessage("PARTIAL", "direct", "search", props.build(), true);
					utils.queueMessage("PARTIAL", "direct", "search", props.build(), user);
					return Response
							.ok(new UserRepresentation(status, acc.getAccountId(), acc.getName(), acc.getAccountType(),
									acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc.getAbout(),
									error)).cacheControl(utils.cacheControl()).build();
				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND, "NO user with given credentials found");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new UserRepresentation(!status, acc.getAccountId(), acc.getName(), acc
									.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(),
									acc.getAbout(), error)).cacheControl(utils.cacheControl()).build();
				}

			} else {
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to change the status of a user please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserRepresentation(!status, acc.getAccountId(), acc.getName(),
								acc.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(),
								acc.getAbout(), error)).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to change the status of a user please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new UserRepresentation(!status, acc.getAccountId(), acc.getName(), acc.getAccountType(),
							acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc.getAbout(), error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {

				if (stmt != null)

					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to change the status of a user please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserRepresentation(!status, acc.getAccountId(), acc.getName(),
								acc.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(),
								acc.getAbout(), error)).cacheControl(utils.cacheControl()).build();
			}
		}
	}

	private Response typeUser(String userId, String type) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		if (acc.getAccountId().equals(userId)) {
			error = utils
					.createErrorResponse(
							Status.FORBIDDEN,
							"Sorry for the inconvenience but you cannot change your own status, please get another administrator to do that or contact the sematime team");
			return Response
					.status(Status.FORBIDDEN)
					.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
							.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc
							.getAbout(), error))
					.entity("{\"error\":{" + utils.createErrorResponse(Status.FORBIDDEN, error) + "}")
					.cacheControl(utils.cacheControl()).build();

		}
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();

			if (conn != null) {
				stmt = conn.prepareStatement("UPDATE `business_site`.`user` SET user_type=? WHERE user_id= ?");
				stmt.setString(1, type);
				stmt.setString(2, userId);

				if (stmt.executeUpdate() == 1) {
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");

					HashMap<String, Object> user = new HashMap<String, Object>();
					HashMap<String, String> partial = new HashMap<String, String>();

					user.put("index_name", "business_site");
					user.put("type_name", "users");
					user.put("id", userId);
					partial.put("user_type", type);
					user.put("partial", partial);

					utils.consumeMessage("PARTIAL", "direct", "search", props.build(), true);
					utils.queueMessage("PARTIAL", "direct", "search", props.build(), user);

					return Response.ok(utils.getUserRepresentation(userId, null, null))
							.cacheControl(utils.cacheControl()).build();
				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND, "NO user with given credentials found");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
									.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(),
									acc.getAbout(), error)).cacheControl(utils.cacheControl()).build();
				}

			} else {
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to change the type of a user please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
								.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc
								.getAbout(), error)).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to change the type of a user please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
							.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc
							.getAbout(), error)).cacheControl(utils.cacheControl()).build();

		} finally {
			try {

				if (stmt != null)

					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to change the type of a user please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
								.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc
								.getAbout(), error)).cacheControl(utils.cacheControl()).build();
			}
		}
	}

	@SecurityCheck
	@DELETE
	@Path("{userId}")
	@RolesAllowed("Administrator")
	public Response deleteUser(@Context SecurityContext sc, @PathParam("userId") final String userId) {
		acc = (Account) sc.getUserPrincipal();
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		if (userId.equals(acc.getAccountId())) {
			error = utils.createErrorResponse(Status.FORBIDDEN,
					"You cannot delete yourself, please get an administrator to delete you");
			return Response.status(Status.FORBIDDEN)
					.entity(new UserRepresentation(true, userId, null, null, null, null, null, null, null, error))
					.entity("{ \"error\" : {" + error + " }}").cacheControl(utils.cacheControl()).build();
		}
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("DELETE FROM `business_site`.`user` WHERE `user_id` = ?");
				stmt.setString(1, userId);

				if (stmt.executeUpdate() == 1) {

					return Response
							.ok(new UserRepresentation(true, userId, null, null, null, null, null, null, null, error))
							.cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND, "There's no user with that id");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new UserRepresentation(true, userId, null, null, null, null, null, null, null,
									error)).entity("{ \"error\" : {" + error + " }}")
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to delete user , please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new UserRepresentation(true, userId, null, null, null, null, null, null, null, error))
					.entity("{ \"error\" : {" + error + " }}").cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to delete user , please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserRepresentation(true, userId, null, null, null, null, null, null, null, error))
						.entity("{\"error\" : {" + error + "\" }").cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete user , please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new UserRepresentation(true, userId, null, null, null, null, null, null, null, error))

				.entity("{\"error\" : {" + error + " }").cacheControl(utils.cacheControl()).build();

	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getUsers(@Context SecurityContext sc, @QueryParam("json") final String json) {

		int offset = 0;
		int batch = 10;
		boolean ascending = true;
		boolean getAll = false;
		int modelLimit = 0;
		String userType = null;
		String userId = null;
		boolean active = true;

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("get_all")) {
				getAll = object.getBoolean("get_all");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}
			if (object.keySet().contains("user_id")) {
				userId = object.getString("user_id");
			}
			if (object.keySet().contains("user_type")) {
				userType = object.getString("user_type");
			}
			if (object.keySet().contains("active")) {
				active = object.getBoolean("active");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = " SELECT user.about_me,user.user_type,user_type.name AS type_name,user.email,user.phone,user.name,user.thumb_pic,user.pic,user.hash,user.last_login,user.user_id,user.active,COUNT(cnt.user_id) AS nos "
				+ " FROM `business_site`.user "
				+ " INNER JOIN `business_site`.user_type ON user_type.user_type_id=user.user_type"
				+ " LEFT JOIN (SELECT user_id FROM `business_site`.`user` ) cnt "
				+ " ON user.user_id IN (SELECT user_id FROM `business_site`.`user` )  ";
		List<UserRepresentation> list = new ArrayList<UserRepresentation>();
		UsersRepresentation response = new UsersRepresentation();
		String accountId = null;
		String email = "";
		String phone = "";
		String pic = null;
		String thumbPic = null;
		String about = null;
		String name = null;
		Map<String, Object> accountType = null;
		boolean user1 = false;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (userId == null) {
					if (userType == null) {
						if (getAll) {
							if (ascending) {
								sql += " WHERE user.active=? " + "  GROUP BY user.user_id ORDER BY user_id ASC";
							} else {
								sql += " WHERE user.active=? " + "  GROUP BY user.user_id ORDER BY user_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, active);
						} else {
							if (ascending) {
								sql += " WHERE user.active=? "
										+ " GROUP BY user.user_id ORDER BY user_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE user.active=? "
										+ "  GROUP BY user.user_id ORDER BY user_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, active);
							stmt.setInt(2, offset);
							stmt.setInt(3, batch);
						}
					} else {
						if (getAll) {
							if (ascending) {
								sql += " WHERE user.active=? AND user.user_type=? "
										+ "  GROUP BY user.user_id ORDER BY user_id ASC";
							} else {
								sql += " WHERE user.active=? AND user.user_type=? "
										+ "  GROUP BY user.user_id ORDER BY user_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, active);
							stmt.setString(2, userType);
						} else {
							if (ascending) {
								sql += " WHERE user.active=? AND user.user_type=? "
										+ "  GROUP BY user.user_id ORDER BY user_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE user.active=? AND user.user_type=? "
										+ "  GROUP BY user.user_id ORDER BY user_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, active);
							stmt.setString(2, userType);
							stmt.setInt(3, offset);
							stmt.setInt(4, batch);
						}
					}
				} else {
					user1 = true;
					if (userType == null) {
						if (getAll) {
							if (ascending) {
								sql += " WHERE user.active=? AND user.user_id=? "
										+ "  GROUP BY user.user_id ORDER BY user_id ASC";
							} else {
								sql += " WHERE user.active=? AND user.user_id=? "
										+ "  GROUP BY user.user_id ORDER BY user_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, active);
							stmt.setString(2, userId);
						} else {
							if (ascending) {
								sql += " WHERE user.active=? AND user.user_id=? "
										+ "  GROUP BY user.user_id ORDER BY user_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE user.active=? AND user.user_id=? "
										+ "  GROUP BY user.user_id ORDER BY user_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, active);
							stmt.setString(2, userId);
							stmt.setInt(3, offset);
							stmt.setInt(4, batch);
						}
					} else {
						if (getAll) {
							if (ascending) {
								sql += " WHERE user.active=? AND user.user_type=? AND user.user_id=? "
										+ "  GROUP BY user.user_id ORDER BY user_id ASC";
							} else {
								sql += " WHERE user.active=? AND user.user_type=? AND user.user_id=? "
										+ "  GROUP BY user.user_id ORDER BY user_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, active);
							stmt.setString(2, userType);
							stmt.setString(3, userId);
						} else {
							if (ascending) {
								sql += " WHERE user.active=? AND user.user_type=? AND user.user_id=? "
										+ "  GROUP BY user.user_id ORDER BY user_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE user.active=? AND user.user_type=? AND user.user_id=? "
										+ "  GROUP BY user.user_id ORDER BY user_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, active);
							stmt.setString(2, userType);
							stmt.setString(3, userId);
							stmt.setInt(4, offset);
							stmt.setInt(5, batch);
						}
					}
				}
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						modelLimit = rs.getInt("nos");
						accountId = rs.getString("user_id");
						active = rs.getBoolean("active");
						email = rs.getString("email");
						pic = rs.getString("pic");
						thumbPic = rs.getString("thumb_pic");
						phone = rs.getString("phone");
						name = rs.getString("name");
						accountType = new HashMap<>();
						accountType.put("id", rs.getInt("user_type"));
						accountType.put("name", rs.getString("type_name"));
						about = rs.getString("about_me");
						UserRepresentation userRep = new UserRepresentation(active, accountId, name, accountType,
								email, phone, thumbPic, thumbPic, about, error);
						if (user1) {
							return Response.ok(userRep).build();
						}
						list.add(userRep);
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service categories ");
					return Response.status(Status.NOT_FOUND).entity(response).entity("{\"error\" : {" + error + " }}")
							.cacheControl(utils.cacheControl()).build();
				}
				response = new UsersRepresentation(list, offset, batch, modelLimit);
				return Response.ok(response).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
					.entity("{\"error\" : {" + error + " }}").cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to get business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
						.entity("{\"error\" : {" + error + " }}").cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).entity("{\"error\" : {" + error + " }}")
				.cacheControl(utils.cacheControl()).build();
	}
}
