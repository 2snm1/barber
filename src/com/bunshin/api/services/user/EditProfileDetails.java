package com.bunshin.api.services.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.services.user.EditProfileDetails;
import com.bunshin.api.services.user.representation.UserRepresentation;
import com.bunshin.api.utils.Utils;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.google.gson.Gson;
import com.sematime.api.json.JSONObject;

@Path("resources/user/profile")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class EditProfileDetails {
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(EditProfileDetails.class);
	private Account acc = null;
	private String error = null;
	private Gson gson = new Gson();

	@SecurityCheck
	@PermitAll
	@GET
	@Path("{userId}")
	public Response getDetails(@Context SecurityContext sc, @PathParam("userId") final String userId) {

		acc = (Account) sc.getUserPrincipal();

		DataSource ds = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);

			conn = ds.getConnection();

			if (conn != null) {
				stmt = conn
						.prepareStatement(" SELECT user.about_me,user.user_type,user_type.name AS type_name,user.email,user.phone,user.name,user.thumb_pic,user.pic,user.hash,user.last_login,user.user_id,user.active "
								+ " FROM `business_site`.user "
								+ " INNER JOIN `business_site`.user_type ON user_type.user_type_id=user.user_type "
								+ " WHERE user.user_id=? ");
				stmt.setString(1, userId);
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						Map<String, Object> accountType = new HashMap<>();
						accountType.put("id", rs.getString("user_type"));
						accountType.put("name", rs.getString("type_name"));
						return Response
								.ok(new UserRepresentation(rs.getBoolean("active"), rs.getString("user_id"), rs
										.getString("name"), accountType, rs.getString("email"), rs.getString("phone"),
										rs.getString("thumb_pic"), rs.getString("pic"), rs.getString("about_me"), gson
												.toJson(error))).cacheControl(utils.cacheControl()).build();

					}
				} else {

					error = utils.createErrorResponse(Status.NOT_FOUND, "There is no user with the given credentials");
					return Response.status(Status.NOT_FOUND).entity(new UserRepresentation())
							.cacheControl(utils.cacheControl()).build();
				}

			}
		} catch (Exception e) {

			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Oops there is something wrong with the server, please try again later");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserRepresentation())
					.cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (stmt != null)

					stmt.close();
				stmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Oops there is something wrong with the server, please try again later");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserRepresentation())
						.cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Oops there is something wrong with the server, please try again later");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new UserRepresentation())
				.cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@PermitAll
	@PUT
	@Path("{userId}")
	public Response editDetails(@Context SecurityContext sc, @FormParam("json") final String json,
			@PathParam("userId") String userId) {

		acc = (Account) sc.getUserPrincipal();
		String name = null;
		String about = null;
		String photo = null;
		String thumb = null;
		String email = acc.getEmail();
		String newPhone = acc.getPhone();
		boolean active = false;
		/*if (!userId.equalsIgnoreCase(acc.getAccountId())) {
			return Response
					.status(Status.FORBIDDEN)
					.entity("{\"user\":\"" + userId + "\",\"error\":"
							+ utils.createErrorResponse(Status.FORBIDDEN, "You cannot edit another person's account")
							+ "}").cacheControl(utils.cacheControl()).build();
		}*/

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("name")) {
				name = object.getString("name");
			}
			if (object.keySet().contains("about")) {
				about = object.getString("about");
			}
			if (object.keySet().contains("photo_link")) {
				photo = object.getString("photo_link");
			}
			if (object.keySet().contains("thumb_link")) {
				thumb = object.getString("thumb_link");
			}
			if (object.keySet().contains("email")) {
				email = object.getString("email").trim();
				if (utils.isValidEmail(email) == false) {
					return Response
							.status(Status.BAD_REQUEST)
							.entity("{\"user\":\"" + userId + "\",\"error\":"
									+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid Email Address") + "}")
							.cacheControl(utils.cacheControl()).build();

				}
				if (utils.getAllEmailAdress().contains(email) == true) {
					return Response
							.status(Status.BAD_REQUEST)
							.entity("{\"user\":\""
									+ userId
									+ "\",\"error\": "
									+ utils.createErrorResponse(Status.BAD_REQUEST, "That Email Address already exists")
									+ "}").cacheControl(utils.cacheControl()).build();
				}
			}
			if (object.keySet().contains("new_phone")) {
				newPhone = utils.sanitizeNumber(object.getString("new_phone"));

				if (utils.isValidNumber(newPhone) == false) {
					String error = utils.createErrorResponse(Status.BAD_REQUEST, "Invalid phone number");
					return Response
							.status(Status.BAD_REQUEST)
							.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email, newPhone,
									thumb, photo, about, error))

							.cacheControl(utils.cacheControl()).build();

				}
				if (utils.getAllNumbers().contains(newPhone) == true) {
					String error = utils.createErrorResponse(Status.BAD_REQUEST, "That phone number already exist");
					return Response
							.status(Status.BAD_REQUEST)
							.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email, newPhone,
									thumb, photo, about, error)).cacheControl(utils.cacheControl()).build();
				}
			}

		} catch (Exception e) {

			log.error(e.getMessage());
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());
			error = utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage());
			return Response
					.status(Status.BAD_REQUEST)
					.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email, newPhone, thumb,
							photo, about, error))

					.cacheControl(utils.cacheControl()).build();
		}

		DataSource ds = null;
		PreparedStatement stmt = null;
		PreparedStatement emailStmt = null;
		PreparedStatement phoneStmt = null;
		Connection conn = null;
		boolean exceptionThrown = false;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				conn.setAutoCommit(false);
				stmt = conn
						.prepareStatement("UPDATE  `business_site`.`user` SET `pic`=?,`name`=?,`thumb_pic`=?,`about_me`=? WHERE user_id=?  ");
				stmt.setString(1, photo);
				stmt.setString(2, name);
				stmt.setString(3, thumb);
				stmt.setString(4, about);
				stmt.setString(5, userId);
				if (!email.equalsIgnoreCase(acc.getEmail())) {
					String sqlEmail = "UPDATE `business_site`.user SET email=? WHERE user_id =?";
					emailStmt = conn.prepareStatement(sqlEmail);
					emailStmt.setString(1, email);
					emailStmt.setString(2, userId);
					int rese = emailStmt.executeUpdate();
					if (rese != 1) {
						conn.rollback();
						error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops there is something wrong with the server, please try again later");
						return Response
								.status(Status.INTERNAL_SERVER_ERROR)
								.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email,
										newPhone, thumb, photo, about, error)).cacheControl(utils.cacheControl())
								.build();

					}

				}
				if (!newPhone.equalsIgnoreCase(acc.getPhone())) {
					String sqlPhone = "UPDATE `business_site`.user SET phone=? , active=? WHERE user_id =? ";
					phoneStmt = conn.prepareStatement(sqlPhone);
					phoneStmt.setString(1, newPhone);
					phoneStmt.setBoolean(2, active);
					phoneStmt.setString(3, userId);
					int resp = phoneStmt.executeUpdate();
					if (resp != 1) {
						conn.rollback();
						error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops there is something wrong with the server, please try again later");
						return Response
								.status(Status.INTERNAL_SERVER_ERROR)
								.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email,
										newPhone, thumb, photo, about, error)).cacheControl(utils.cacheControl())
								.build();

					}
				}

				if (stmt.executeUpdate() == 1) {
					conn.commit();
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");

					HashMap<String, Object> user = new HashMap<String, Object>();
					HashMap<String, String> partial = new HashMap<String, String>();

					user.put("index_name", "business_site");
					user.put("type_name", "users");
					user.put("id", userId);
					partial.put("name", name);
					partial.put("about_me", about);
					partial.put("email", email);
					partial.put("photo_link", photo);
					partial.put("thumb_link", thumb);
					partial.put("phone", newPhone);
					user.put("partial", partial);

					utils.consumeMessage("PARTIAL", "direct", "search", props.build(), true);
					utils.queueMessage("PARTIAL", "direct", "search", props.build(), user);
					return Response
							.ok(new UserRepresentation(active, userId, name, acc.getAccountType(), email, newPhone,
									thumb, photo, about, error)).cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Oops there is something wrong with the server, please try again later");
					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email, newPhone,
									thumb, photo, about, error)).cacheControl(utils.cacheControl()).build();
				}
			} else {

				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Oops there is something wrong with the server, please try again later");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email, newPhone,
								thumb, photo, about, error)).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			exceptionThrown = true;
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Oops there is something wrong with the server, please try again later");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email, newPhone, thumb,
							photo, about, error)).cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (exceptionThrown) {
					conn.rollback();
				}
				conn.setAutoCommit(true);
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Oops there is something wrong with the server, please try again later");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new UserRepresentation(active, userId, name, acc.getAccountType(), email, newPhone,
								thumb, photo, about, error)).cacheControl(utils.cacheControl()).build();

			}
		}
	}
}
