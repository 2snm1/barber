package com.bunshin.api.services.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;

import org.mindrot.BCrypt;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.bunshin.api.Account;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.sematime.api.json.JSONObject;
import com.bunshin.api.services.user.representation.UserRepresentation;
import com.bunshin.api.utils.Utils;
import com.google.gson.Gson;

@Path("resources/user/details/change")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class ChangeLogInDetails {

	private Account acc = null;
	private Utils utils = new Utils();
	private Gson gson = new Gson();
	private String error = null;

	@SecurityCheck
	@PermitAll
	@PUT
	@Path("{userId}/email")
	public Response newEmail(@Context SecurityContext sc, @FormParam("json") final String json,
			@PathParam("userId") String userId) {
		String newEmail = null;
		acc = (Account) sc.getUserPrincipal();
		if (!userId.equals(acc.getAccountId())) {
			return Response
					.status(Status.FORBIDDEN)
					.entity("{\"user\":\"" + userId + "\",\"error\":"
							+ utils.createErrorResponse(Status.FORBIDDEN, "Invalid Email Address") + "}")
					.cacheControl(utils.cacheControl()).build();
		}

		try {
			JSONObject object = new JSONObject(json);
			newEmail = object.getString("new_email").trim();
		} catch (Exception e) {
			e.printStackTrace();

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Server error: " + e.getLocalizedMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		if (utils.isValidEmail(newEmail) == false) {
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"user\":\"" + userId + "\",\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid Email Address") + "}")
					.cacheControl(utils.cacheControl()).build();

		}
		if (utils.getAllEmailAdress().contains(newEmail) == true) {
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"user\":\"" + userId + "\",\"error\": {"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "That Email Address already exists") + "}")
					.cacheControl(utils.cacheControl()).build();
		}

		DataSource ds = null;
		Connection conn = null;

		PreparedStatement updateStmt = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);

			conn = ds.getConnection();

			if (conn != null) {
				String sql = "UPDATE `business_site`.user SET email=? WHERE user_id =?";
				updateStmt = conn.prepareStatement(sql);
				updateStmt.setString(1, newEmail);
				updateStmt.setString(2, userId);

				int rowAffected = updateStmt.executeUpdate();

				if (rowAffected > 0) {
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("business_site").deliveryMode(2)
							.messageId(String.valueOf(System.currentTimeMillis())).type("custom");

					HashMap<String, Object> user = new HashMap<String, Object>();
					HashMap<String, String> partial = new HashMap<String, String>();

					user.put("index_name", "business_site");
					user.put("type_name", "users");
					user.put("id", userId);
					partial.put("email", newEmail);
					user.put("partial", partial);

					utils.consumeMessage("PARTIAL", "direct", "search", props.build(), true);
					utils.queueMessage("PARTIAL", "direct", "search", props.build(), user);
					return Response
							.ok(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
									.getAccountType(), newEmail, acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc
									.getAbout(), error)).cacheControl(utils.cacheControl()).build();
				} else {

					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
									.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(),
									acc.getAbout(), error))
							.entity("{\"error\" : {"
									+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
											"Oops we seem to have encountered a problem, please try again ") + " }}")
							.cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (updateStmt != null)

					updateStmt.close();
				updateStmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		error = "{"
				+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Oops we seem to have encountered a problem, please try again ") + " }";
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc.getAccountType(),
						acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc.getAbout(), gson
								.toJson(error)))

				.cacheControl(utils.cacheControl()).build();

	}

	/**
	 * used to change password of a user from an email link with the hash value,
	 * then the hash/{hashValue} returns the accountId
	 * 
	 * @param json
	 * @return Response object
	 */
	@Path("/password")
	@PermitAll
	@PUT
	public Response newPassword(@FormParam("json") final String json) {
		String password = null;
		String userId = null;
		try {
			JSONObject object = new JSONObject(json);
			userId = object.getString("user_id");
			password = object.getString("new_password");
		} catch (Exception e) {
			e.printStackTrace();
			error = utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
					+ "}";
			return Response
					.status(Status.BAD_REQUEST)
					.entity(new UserRepresentation(false, null, null, null, null, null, null, null, null, gson
							.toJson(error))).cacheControl(utils.cacheControl()).build();
		}

		if (utils.isValidPasword(password) == false) {
			error = "{"
					+ utils.createErrorResponse(
							Status.BAD_REQUEST,
							"Invalid password: <br> "
									+ "The password must contain: <ul><li> atleast one small letter</li><li>one caps letter</li><lii> one big caps letter</li><li> one special character</li><li> one number and no spaces</li></ul>")
					;
			return Response
					.status(Status.BAD_REQUEST)
					.entity(new UserRepresentation(false, null, null, null, null, null, null, null, null, gson
							.toJson(error))).cacheControl(utils.cacheControl()).build();
		}
		return setHash(password, userId);

	}

	@Path("/{userId}/password")
	@SecurityCheck
	@PUT
	public Response newPassword(@Context SecurityContext sc, @FormParam("json") final String json,
			@PathParam("userId") String userId) {
		String password = null;
		String currentPassword = null;
		acc = (Account) sc.getUserPrincipal();
		if (!userId.equals(acc.getAccountId())) {
			return Response
					.status(Status.FORBIDDEN)
					.entity("{\"user\":\"" + userId + "\",\"error\":"
							+ utils.createErrorResponse(Status.FORBIDDEN, "Invalid Email Address") + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		try {
			JSONObject object = new JSONObject(json);
			password = object.getString("new_password").trim();
			currentPassword = object.getString("current_password").trim();
		} catch (Exception e) {
			e.printStackTrace();
			error = utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()) ;
			return Response
					.status(Status.BAD_REQUEST)
					.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
							.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc
							.getAbout(), error)).cacheControl(utils.cacheControl()).build();
		}
		if (!checkCurrentPassword(currentPassword)) {
			error = utils.createErrorResponse(Status.BAD_REQUEST, "Invalid Current Password") ;
			return Response
					.status(Status.BAD_REQUEST)
					.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
							.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc
							.getAbout(), error)).cacheControl(utils.cacheControl()).build();

		}

		if (utils.isValidPasword(password) == false) {
			error = "{"
					+ utils.createErrorResponse(
							Status.BAD_REQUEST,
							"Invalid password: <br> "
									+ "The password must contain: <ul><li> atleast one small letter</li><li>one caps letter</li><lii> one big caps letter</li><li> one special character</li><li> one number and no spaces</li></ul>")
					;
			return Response
					.status(Status.BAD_REQUEST)
					.entity(new UserRepresentation(acc.isActive(), acc.getAccountId(), acc.getName(), acc
							.getAccountType(), acc.getEmail(), acc.getPhone(), acc.getThumbPic(), acc.getPic(), acc
							.getAbout(), error)).cacheControl(utils.cacheControl()).build();
		}

		return setHash(password, userId);

	}

	private Response setHash(String password, String accountId) {
		DataSource ds = null;
		Connection conn = null;

		PreparedStatement updateStmt = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);

			conn = ds.getConnection();

			if (conn != null) {
				String sql = "UPDATE `business_site`.user SET hash=? WHERE user_id =?";
				updateStmt = conn.prepareStatement(sql);
				updateStmt.setString(1, BCrypt.hashpw(password, BCrypt.gensalt(13)));
				updateStmt.setString(2, accountId);

				int rowAffected = updateStmt.executeUpdate();

				if (rowAffected > 0) {
					utils.deleteToken(null, accountId);
					String token = utils.setToken(accountId);

					return Response.ok(utils.getUserRepresentation(accountId, null, null)).header("TOKEN", token)
							.header("USER_ID", accountId).cacheControl(utils.cacheControl()).build();

				} else {
					return Response
							.status(Status.NOT_FOUND)
							.entity(utils.getUserRepresentation(accountId, null, null))
							.entity("{\"error\":"
									+ utils.createErrorResponse(Status.NOT_FOUND,
											"no user with given credentials exists") )
							.cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (updateStmt != null)

					updateStmt.close();
				updateStmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(utils.getUserRepresentation(accountId, null, null))
				.entity("{\"error\":"
						+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: please try to log in again") ).cacheControl(utils.cacheControl())
				.build();
	}

	private boolean checkCurrentPassword(String currentPassword) {
		Connection conn = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		DataSource ds = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {

				String hash = null;
				pstmt = conn.prepareStatement("SELECT user.hash " + " FROM `business_site`.user "
						+ " WHERE user_id =? ");

				pstmt.setString(1, acc.getAccountId());
				rs = pstmt.executeQuery();

				if (rs.isBeforeFirst()) {
					if (rs.next()) {
						hash = rs.getString("hash");

						return BCrypt.checkpw(currentPassword, hash);

					} else {
						return false;
					}
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	/**
	 * used to get the account Id from a token value that was sent to the user's
	 * email, THEN THE USER CAN CHANGE THE PASSWORD
	 * 
	 * @param hash
	 * @return
	 */
	@PermitAll
	@GET
	@Path("email")
	public Response getAccountIdFromToken(@QueryParam("token") String hash) {

		return getId(hash, true, null);

	}

	/**
	 * Used to get the user id from the phone number
	 * 
	 * @param phone
	 * @return
	 */
	@PermitAll
	@GET
	@Path("phone/{phone}")
	public Response getAccountIdFromPhoneKey(@PathParam("phone") String phone) {

		return getId(null, false, utils.sanitizeNumber(phone));

	}

	// could be used for forgot passwords
	private Response getId(String hash, boolean token, String phone) {
		String name = null;
		String email = null;
		String thumbPic = null;
		String pic = null;
		Connection conn = null;
		String accountId = null;
		PreparedStatement pstmt = null;
		String about = null;
		ResultSet rs = null;
		DataSource ds = null;
		String hashedValue = null;
		Map<String, Object> accountType = new HashMap<String, Object>();
		boolean active = false;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (token) {
					pstmt = conn
							.prepareStatement("SELECT user.about_me,`user`.`user_id`,`user`.`name`,`user`.user_type,"
									+ " `user`.phone,`user`.active,`user`.thumb_pic,`user`.pic,`user_type`.name AS type_name,reset_passwords.hash,user.email,reset_passwords.validity_period,reset_passwords.user_id FROM `business_site`.user "
									+ " LEFT JOIN `business_site`.reset_passwords ON user.user_id=reset_passwords.user_id "
									+ " LEFT JOIN `business_site`.user_type ON user_type.user_type_id=user.user_type "
									+ " WHERE reset_passwords.hash =? AND reset_passwords.done =?");
					pstmt.setString(1, hash);
					pstmt.setBoolean(2, false);
				} else {
					pstmt = conn
							.prepareStatement("SELECT user.about_me,`user`.`user_id`,`user`.`name`,`user`.user_type,"
									+ "`user`.phone,`user`.active,`user`.thumb_pic,`user`.pic,`user_type`.name AS type_name,reset_passwords.hash,user.email,reset_passwords.validity_period,reset_passwords.user_id "
									+ " FROM `business_site`.user "
									+ " LEFT JOIN `business_site`.reset_passwords  ON user.user_id=reset_passwords.user_id "
									+ " LEFT JOIN `business_site`.user_type ON user_type.user_type_id=user.user_type "
									+ " WHERE  user.phone =? ");

					pstmt.setString(1, phone);
				}
				rs = pstmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						accountId = rs.getString("user_id");

						hashedValue = rs.getString("hash");
						active = rs.getBoolean("active");
						email = rs.getString("email");
						thumbPic = rs.getString("thumb_pic");
						pic = rs.getString("pic");
						accountType.put("type", rs.getInt("user_type"));
						accountType.put("name", rs.getString("type_name"));
						about = rs.getString("about_me");
						if (token) {
							if (!hash.equalsIgnoreCase(hashedValue)) {
								return Response
										.status(Status.FORBIDDEN)
										.entity(new UserRepresentation(active, accountId, name, accountType, email,
												phone, thumbPic, pic, about, error))
										.entity("{\"error\":"
												+ utils.createErrorResponse(Status.FORBIDDEN,
														"You entered an incorrect or expired key") + "}")
										.cacheControl(utils.cacheControl()).build();
							}
							if (Long.parseLong(rs.getString("validity_period")) > System.currentTimeMillis()) {
								if (updateReset(hashedValue, accountId)) {
									return Response
											.ok(new UserRepresentation(active, accountId, name, accountType, email,
													phone, thumbPic, pic, about, error))
											.cacheControl(utils.cacheControl()).build();
								} else {
									return Response
											.status(Status.INTERNAL_SERVER_ERROR)
											.entity(new UserRepresentation(active, accountId, name, accountType, email,
													phone, thumbPic, pic, about, error))
											.entity("{\"error\":"
													+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
															"Server error: please try again") + "}")
											.cacheControl(utils.cacheControl()).build();

								}
							} else {
								return Response
										.status(Status.EXPECTATION_FAILED)
										.entity(new UserRepresentation(active, accountId, name, accountType, email,
												phone, thumbPic, pic, about, error))
										.entity("{\"error\":"
												+ utils.createErrorResponse(
														Status.EXPECTATION_FAILED,
														"The validity period of your password reset token has expired, please generate a new one by going to the forgot password")
												+ "}").cacheControl(utils.cacheControl()).build();

							}

						}
						return Response
								.ok(new UserRepresentation(active, accountId, name, accountType, email, phone,
										thumbPic, pic, about, error)).cacheControl(utils.cacheControl())
								.build();

					}
				} else {
					return Response
							.status(Status.NOT_FOUND)
							.entity(new UserRepresentation(active, accountId, name, accountType, email, phone,
									thumbPic, pic, about, error))
							.entity("{\"error\":"
									+ utils.createErrorResponse(Status.NOT_FOUND,
											"no user with given credentials exists") + "}")
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)

					rs.close();
				rs = null;

				if (pstmt != null)

					pstmt.close();
				pstmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		error = "{"
				+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR, "Server error: please try to log in again")
				;
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new UserRepresentation(active, accountId, name, accountType, email, phone, thumbPic, pic,
						about, error)).cacheControl(utils.cacheControl()).build();
	}

	private boolean updateReset(String hash, String userId) {
		DataSource ds = null;
		Connection conn = null;

		PreparedStatement updateStmt = null;
		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);

			conn = ds.getConnection();

			if (conn != null) {
				String sql = "UPDATE `business_site`.reset_passwords SET done=? WHERE hash =? AND user_id=?";
				updateStmt = conn.prepareStatement(sql);
				updateStmt.setBoolean(1, true);
				updateStmt.setString(2, hash);
				updateStmt.setString(3, userId);

				int rowAffected = updateStmt.executeUpdate();

				if (rowAffected > 0) {
					return true;
				} else {
					return false;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (updateStmt != null)

					updateStmt.close();
				updateStmt = null;

				if (conn != null)

					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return false;
	}
}
