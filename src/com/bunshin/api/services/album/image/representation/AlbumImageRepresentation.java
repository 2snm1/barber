package com.bunshin.api.services.album.image.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "business image")
@InjectLinks({
		@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/business/image/${instance.imageId}", rel = "get", title = "Get Specific Image"),
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/image/${instance.imageId}", rel = "create", title = "Add Image"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/image/${instance.imageId}", rel = "edit", title = "Edit Image"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/image/${instance.imageId}", rel = "del", title = "Delete Image") })
public class AlbumImageRepresentation {
	@XmlTransient
	// accessible from the JaXB representation
	private String imageUrl = null;
	@XmlElement
	// accessible from the JaXB representation
	private String imageId = null;
	@XmlElement
	// accessible from the JaXB representation
	private String error = null;
	@XmlElement
	// accessible from the JaXB representation
	private String albumId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String thumbLink = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String name = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String description = null;
	@InjectLink(style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/business/image/${instance.imageId}", rel = "get", title = "Get Specific Image"),
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/image/${instance.imageId}", rel = "create", title = "Add Image"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/image/${instance.imageId}", rel = "edit", title = "Edit Image"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/image/${instance.imageId}", rel = "del", title = "Delete Image") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public AlbumImageRepresentation() {

	}

	public AlbumImageRepresentation(String imageUrl,String thumbLink, String imageId, String albumId, String name,
			String description,String error) {
		this.imageUrl = imageUrl;
		this.imageId = imageId;
		this.albumId = albumId;
		this.thumbLink = thumbLink;
		this.name = name;
		this.description = description;
		this.error=error;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public String getServiceId() {
		return albumId;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getImageId() {
		return imageId;
	}

	public String getAlbumId() {
		return albumId;
	}

	public String getThumbLink() {
		return thumbLink;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if(error!=null){
		 errorMap = new HashMap<>();
		JSONObject errorObject = new JSONObject(error);
		errorMap.put("status", errorObject.get("status"));
		errorMap.put("error message", errorObject.get("error message"));
		errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}
	
	@Override
	public String toString() {
		return "AlbumImageRepresentation [imageUrl=" + imageUrl + ", imageId=" + imageId + ", error=" + error
				+ ", albumId=" + albumId + ", thumbLink=" + thumbLink + ", name=" + name + ", description="
				+ description + ", self=" + self + ", links=" + links + "]";
	}

}
