package com.bunshin.api.services.album.image.representation;

import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLink.Style;
import org.glassfish.jersey.linking.InjectLinks;

import com.bunshin.api.services.album.images.AlbumImage;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "services")
@InjectLinks({
		@InjectLink(value = "/resources/business/image/?json=%7B%22offset%22:%22${instance.offset + instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
		@InjectLink(value = "/resources/business/image/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
public class AlbumImagesRepresentation {
	@XmlElement
	private List<AlbumImageRepresentation> services;
	@XmlElement
	private int offset = 0;
	@XmlElement
	private int limit = 0;
	@XmlElement
	private int modelLimit = 0;

	@InjectLink(type = "query", style = Style.ABSOLUTE, resource = AlbumImage.class, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(value = "/resources/business/image/?json=%7B%22offset%22:%22${instance.offset + instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
			@InjectLink(value = "/resources/business/image/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public AlbumImagesRepresentation() {

	}

	public AlbumImagesRepresentation(List<AlbumImageRepresentation> services, int offset, int limit, int modelLimit) {
		this.services = services;
		this.offset = offset;
		this.limit = limit;
		this.modelLimit = modelLimit;
	}

	public List<AlbumImageRepresentation> getServices() {
		return services;
	}

	public int getOffset() {
		return offset;
	}

	public int getLimit() {
		return limit;
	}

	public int getModelLimit() {
		return modelLimit;
	}

	@Override
	public String toString() {
		return "BusinessImagesRepresentation [services=" + services + ", offset=" + offset + ", limit=" + limit
				+ ", modelLimit=" + modelLimit + ", self=" + self + ", links=" + links + "]";
	}

}
