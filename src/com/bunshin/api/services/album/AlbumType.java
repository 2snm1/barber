package com.bunshin.api.services.album;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.services.album.representation.AlbumTypeRepresentation;
import com.bunshin.api.services.album.representation.AlbumTypesRepresentation;
import com.bunshin.api.services.business.service.representation.ServiceRepersentation;
import com.bunshin.api.services.business.service.representation.ServicesRepresentation;
import com.bunshin.api.services.user.representation.UserTypeRepresentation;
import com.bunshin.api.utils.Utils;
import com.google.gson.Gson;
import com.sematime.api.json.JSONObject;

@Path("resources/album/type")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class AlbumType {
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(AlbumType.class);
	private String error = null;
	private Gson gson = new Gson();

	@PermitAll
	@SecurityCheck
	@POST
	public Response createAlbumType(@Context SecurityContext sc, @FormParam("json") final String json) {
		String name = null;

		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		if (name == null) {
			log.error(Status.BAD_REQUEST + "No name accompanying album type ");
			return Response
					.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST,
							"Album Type name missing .The albumtype should be accompanied with a name "))
					.cacheControl(utils.cacheControl()).build();
		}

		String albumTypeId = String.valueOf(System.currentTimeMillis());
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`album_type` VALUES (?,?)");
				stmt.setString(1, albumTypeId);
				stmt.setString(2, name);

				if (stmt.executeUpdate() == 1) {
					return Response.ok(new AlbumTypeRepresentation(albumTypeId, name, error))
							.cacheControl(utils.cacheControl()).build();
				} else {

					error = utils
							.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Server error: Oops something went wrong while trying to create album type, please try again.");
					return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new AlbumTypeRepresentation())
							.cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to create albumtype, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new AlbumTypeRepresentation())
					.cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to create albumtype, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new AlbumTypeRepresentation())
						.cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to create albumtype, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new AlbumTypeRepresentation())
				.cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getAlbumType(@Context SecurityContext sc, @QueryParam("json") final String json) {
		// TODO get error
		int offset = 0;
		int batch = 10;
		boolean ascending = true;
		boolean getAll = false;
		String albumTypeId = null;
		String name = null;
		int modelLimit = 0;

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("get_all")) {
				getAll = object.getBoolean("get_all");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = " SELECT album_type.album_type_id,album_type.name,COUNT(cnt.album_type_id) AS nos "
				+ " from `business_site`.`album_type`"
				+ " LEFT JOIN (SELECT album_type_id FROM `business_site`.`album_type` ) cnt "
				+ " ON album_type.album_type_id IN (SELECT album_type_id FROM `business_site`.`album_type` ) ";
		List<AlbumTypeRepresentation> list = new ArrayList<AlbumTypeRepresentation>();
		AlbumTypesRepresentation response = new AlbumTypesRepresentation();

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (getAll) {
					if (ascending) {
						sql += " GROUP BY album_type_id ORDER BY album_type_id ASC";
					} else {
						sql += " GROUP BY album_type_id ORDER BY album_type_id DESC";
					}
					stmt = conn.prepareStatement(sql);
				} else {
					if (ascending) {
						sql += " GROUP BY album_type_id ORDER BY album_type_id ASC LIMIT ?,?";
					} else {
						sql += " GROUP BY album_type_id ORDER BY album_type_id DESC LIMIT ?,?";
					}
					stmt = conn.prepareStatement(sql);
					stmt.setInt(1, offset);
					stmt.setInt(2, batch);
				}
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						modelLimit = rs.getInt("nos");
						albumTypeId = rs.getString("album_type_id");
						name = rs.getString("name");
AlbumTypeRepresentation albumType=new AlbumTypeRepresentation(albumTypeId, name, error);
						list.add(albumType);
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any albumtype categories ");
					return Response.status(Status.NOT_FOUND).entity(response).cacheControl(utils.cacheControl())
							.build();
				}
				response = new AlbumTypesRepresentation(list, offset, batch, modelLimit);

				return Response.ok(response).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get albumtype, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to get albumtype, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get albumtype, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
				.build();
	}

	@SecurityCheck
	@GET
	@Path("{albumTypeId}")
	public Response getService(@Context SecurityContext sc, @PathParam("albumTypeId") final String albumTypeId) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("SELECT * FROM `business_site`.`album_type` WHERE  `album_type_id`=?");
				stmt.setString(1, albumTypeId);
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						return Response
								.ok(new AlbumTypeRepresentation(rs.getString("album_type_id"), rs.getString("name"),
										error)).cacheControl(utils.cacheControl()).build();

					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service categories with such an id");
					return Response.status(Status.NOT_FOUND).entity(new AlbumTypeRepresentation())
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new AlbumTypeRepresentation())
					.cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to get business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new AlbumTypeRepresentation())
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new AlbumTypeRepresentation())
				.cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@DELETE
	@Path("{albumTypeId}")
	@RolesAllowed("Administrator")
	public Response deleteAlbumType(@Context SecurityContext sc, @PathParam("albumTypeId") final String albumTypeId) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("DELETE FROM `business_site`.`album_type` WHERE `album_type_id` = ?");
				stmt.setString(1, albumTypeId);

				if (stmt.executeUpdate() == 1) {

					return Response.ok(new AlbumTypeRepresentation()).cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service with that id");
					return Response.status(Status.NOT_FOUND)
							.entity(new AlbumTypeRepresentation(albumTypeId, null, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to delete business service , please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumTypeRepresentation(albumTypeId, null, error)).cacheControl(utils.cacheControl())
					.build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to delete business service , please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumTypeRepresentation(albumTypeId, null, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete business service , please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AlbumTypeRepresentation(albumTypeId, null, error))

				.cacheControl(utils.cacheControl()).build();

	}

	@PermitAll
	@SecurityCheck
	@RolesAllowed("Administrator")
	@PUT
	@Path("{albumTypeId}")
	public Response editAlbumType(@Context SecurityContext sc, @PathParam("albumTypeId") final String albumTypeId,
			@FormParam("json") String json) {
		String name = null;

		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("UPDATE `business_site`.`album_type` SET name=?   WHERE  album_type_id=?");
				stmt.setString(1, name);
				stmt.setString(2, albumTypeId);

				if (stmt.executeUpdate() == 1) {
					return Response.ok(new AlbumTypeRepresentation(albumTypeId, name, error))
							.cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service categories ");
					return Response.status(Status.NOT_FOUND)
							.entity(new AlbumTypeRepresentation(albumTypeId, name, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to delete business service group, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumTypeRepresentation(albumTypeId, name, error)).cacheControl(utils.cacheControl())
					.build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to delete business service group, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumTypeRepresentation(albumTypeId, name, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils
				.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to delete business service group, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AlbumTypeRepresentation(albumTypeId, name, error)).cacheControl(utils.cacheControl())
				.build();

	}

}
