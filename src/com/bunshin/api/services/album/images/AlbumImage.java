package com.bunshin.api.services.album.images;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.security.PermitAll;
import javax.imageio.ImageIO;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;

import org.apache.poi.util.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.cloudsearchdomain.model.Bucket;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.bunshin.api.services.album.image.representation.AlbumImageRepresentation;
import com.bunshin.api.services.album.image.representation.AlbumImagesRepresentation;
import com.bunshin.api.services.business.service.representation.ServiceRepersentation;
import com.bunshin.api.services.business.service.representation.ServicesRepresentation;
import com.bunshin.api.utils.Utils;
import com.bunshin.api.Account;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.MyDataSource;
import com.sematime.api.json.JSONObject;

@Path("resources/business/image")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class AlbumImage {

	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(AlbumImage.class);
	private Account acc = null;
	private String error = null;

	@SecurityCheck
	@POST
	@Path("upload/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response createPhoto(@Context SecurityContext sc, @FormDataParam("file") FormDataBodyPart body,
			@FormDataParam("file") InputStream fileInputStream, @FormDataParam("name") String name,
			@FormDataParam("description") String description, @FormDataParam("album_id") String albumId,
			@FormDataParam("file") FormDataContentDisposition contentDispositionHeader) {
		Optional<String> album = Optional.ofNullable(albumId);
		Optional<String> fileName = Optional.ofNullable(name);
		if (body.getMediaType().equals(com.google.common.net.MediaType.ANY_IMAGE_TYPE)) {
			// save the file to the server
			if (!album.isPresent()) {
				return Response.status(Status.BAD_REQUEST)
						.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Please provide an album_id "))
						.cacheControl(utils.cacheControl()).build();

			}
			// save the file to the server
			if (!fileName.isPresent()) {
				name = body.getName();
			}
			acc = (Account) sc.getUserPrincipal();
			return saveFile(fileInputStream, contentDispositionHeader.getFileName(), name, description, albumId,
					acc.getAccountId());

		} else {
			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid image format "))
					.cacheControl(utils.cacheControl()).build();
		}

	}

	// save uploaded file to a defined location on the server
	private Response saveFile(InputStream uploadedInputStream, String fileName, String name, String description,
			String albumId, String userId) {
		try {
			File tempFile = File.createTempFile("barber-" + userId + "-" + albumId, ".tmp");
			// tempFile.createNewFile();
			tempFile.deleteOnExit();
			File thumbFile = File.createTempFile("barber-" + userId + "-" + albumId + "thumb", ".tmp");
			thumbFile.deleteOnExit();
			try (OutputStream outpuStream = new FileOutputStream(tempFile)) {
				IOUtils.copy(uploadedInputStream, outpuStream);
			}

			BufferedImage thumbnail = Scalr.resize(ImageIO.read(tempFile), Scalr.Method.QUALITY, Scalr.Mode.AUTOMATIC,
					100, 100, Scalr.OP_ANTIALIAS);
			ImageIO.write(thumbnail, "jpeg", thumbFile);
			TransferManager tx = new TransferManager(new InstanceProfileCredentialsProvider());

			AmazonS3 s3Conn = new AmazonS3Client(new InstanceProfileCredentialsProvider());
			String key = albumId + "/" + fileName;
			String thumbKey = albumId + "/thumb-" + fileName;
			List<com.amazonaws.services.s3.model.Bucket> buckets = s3Conn.listBuckets();
			if (!buckets.contains("barber-photo")) {
				CreateBucketRequest createBucket = new CreateBucketRequest("barber-photo")
						.withCannedAcl(CannedAccessControlList.PublicRead);
				s3Conn.createBucket(createBucket);
			}
			// PutObjectResult resFull=s3Conn.putObject("barber-" +
			// /*userId123+ "-" + albumId, key, tempFile);
			Upload photoUpload = tx.upload(new PutObjectRequest("barber-photo", key, tempFile)
					.withCannedAcl(CannedAccessControlList.PublicRead));
			Upload thumbUpload = tx.upload(new PutObjectRequest("barber-photo", thumbKey, thumbFile)
					.withCannedAcl(CannedAccessControlList.PublicRead));

			// TODO:return asynchronous message of transferred bytes.
			while (photoUpload.isDone() == false) {
				// used transferred for showing % uploaded content
				Long transfered = photoUpload.getProgress().getBytesTransfered();
			}
			while (thumbUpload.isDone() == false) {
				Long transfered = thumbUpload.getProgress().getBytesTransfered();
			}

			tx.shutdownNow();
			if (!photoUpload.isDone() || !thumbUpload.isDone()) {
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity("{\"photo\" : \""
								+ name
								+ "\" , \"status\" : \"not uploaded\",\"error\" : "
								+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
										"Oops it seems an error occured while uploading your photo, please try again.")
								+ " }").cacheControl(utils.cacheControl()).build();
			}

			return uploadToDatabase(albumId, name, description, "http://s3.amazonaws.com/barber-photo/" + key,
					"http://s3.amazonaws.com/barber-photo/" + thumbKey, userId);

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumImageRepresentation(null, null, null, albumId, name, description, error))
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops it seems an error occured while uploading your photo, please try again.")
							+ " }").cacheControl(utils.cacheControl()).build();
		} finally {
			if (uploadedInputStream != null) {
				try {
					uploadedInputStream.close();
				} catch (IOException e) {
					log.error(e.getMessage());
					e.printStackTrace();
				}
			}
		}

	}

	private Response uploadToDatabase(String albumId, String photoName, String description, String link,
			String thumbLink, String userId) {
		String imageId = String.valueOf(System.nanoTime());
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`photo` VALUES (?,?,?,?,?,?,?,?)");
				stmt.setString(1, imageId);
				stmt.setString(2, albumId);
				stmt.setString(3, description);
				stmt.setString(4, photoName);
				stmt.setBoolean(5, true);
				stmt.setString(6, userId);
				stmt.setString(7, link);
				stmt.setString(8, thumbLink);

				if (stmt.executeUpdate() == 1) {
					return Response
							.ok(new AlbumImageRepresentation(link, thumbLink, imageId, albumId, photoName, description,
									error)).cacheControl(utils.cacheControl()).build();
				} else {

					error = utils
							.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Server error: Oops something went wrong while trying to upload photo, please try again.db");
					deleteAWS(link, thumbLink);
					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new AlbumImageRepresentation(link, thumbLink, null, albumId, photoName,
									description, error)).cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to upload photo, please try again.");
			try {
				deleteAWS(link, thumbLink);
			} catch (IOException e1) {
				e1.printStackTrace();
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumImageRepresentation(link, thumbLink, null, albumId, photoName, description,
								error)).cacheControl(utils.cacheControl()).build();
			}
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumImageRepresentation(link, thumbLink, null, albumId, photoName, description, error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to upload photo, please try again.");
				try {
					deleteAWS(link, thumbLink);
				} catch (IOException e1) {
					e1.printStackTrace();
					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new AlbumImageRepresentation(link, thumbLink, null, albumId, photoName,
									description, error)).cacheControl(utils.cacheControl()).build();
				}
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumImageRepresentation(link, thumbLink, null, albumId, photoName, description,
								error)).cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to upload photo, please try again.");
		try {
			deleteAWS(link, thumbLink);
		} catch (IOException e1) {
			e1.printStackTrace();
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumImageRepresentation(link, thumbLink, null, albumId, photoName, description, error))
					.cacheControl(utils.cacheControl()).build();
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AlbumImageRepresentation(link, thumbLink, null, albumId, photoName, description, error))
				.cacheControl(utils.cacheControl()).build();
	}

	private void deleteAWS(String photoLink, String thumbLink) throws IOException {
		AmazonS3 s3Conn = new AmazonS3Client(new PropertiesCredentials(
				AlbumImage.class.getResourceAsStream("/com/bunshin/api/AwsCredentials.properties")));

		DeleteObjectsRequest multiDelete = new DeleteObjectsRequest("schoolmart-photo");

		List<KeyVersion> keys = new ArrayList<DeleteObjectsRequest.KeyVersion>();

		keys.add(new KeyVersion(photoLink.replace("http://s3.amazonaws.com/barber-photo/", "")));
		keys.add(new KeyVersion(thumbLink.replace("http://s3.amazonaws.com/barber-photo/", "")));
		multiDelete.setKeys(keys);

		s3Conn.deleteObjects(multiDelete);
	}

	@PUT
	@SecurityCheck
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("{photoId}")
	public Response editPhoto(@Context SecurityContext sc, @PathParam("photoId") final String photoId,
			@FormParam("json") String json) {

		String photoName = null;
		String description = null;
		String albumId = null;
		boolean active = false;

		acc = (Account) sc.getUserPrincipal();
		try {
			JSONObject object = new JSONObject(json);
			photoName = object.getString("photo_name");
			description = object.getString("description");
			active = object.getBoolean("active");
			albumId = object.getString("album_id");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn
						.prepareStatement("UPDATE `business_site`.`image`  SET name=?,description=?,album_id=?,active=? WHERE image_id=? AND creator_id=?");

				stmt.setString(3, albumId);
				stmt.setString(2, description);
				stmt.setString(1, photoName);
				stmt.setBoolean(4, active);
				stmt.setString(5, photoId);
				stmt.setString(6, acc.getAccountId());

				if (stmt.executeUpdate() == 1) {
					return Response
							.ok(new AlbumImageRepresentation(null, null, photoId, albumId, photoName, description,
									error)).cacheControl(utils.cacheControl()).build();
				} else {

					error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to edit photo, please try again.db");

					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new AlbumImageRepresentation(null, null, photoId, albumId, photoName, description,
									error)).cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to edit photo, please try again.");

			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumImageRepresentation(null, null, photoId, albumId, photoName, description, error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to edit photo, please try again.");

				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumImageRepresentation(null, null, photoId, albumId, photoName, description,
								error)).cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to upload photo, please try again.");

		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AlbumImageRepresentation(null, null, photoId, albumId, photoName, description, error))
				.cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@DELETE
	@Path("{photoId}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response deletePhoto(@Context SecurityContext sc, @PathParam("photoId") final String photoId)
			throws IOException {
		acc = (Account) sc.getUserPrincipal();
		DataSource ds = null;
		PreparedStatement pstmt = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				pstmt = conn
						.prepareStatement("SELECT * FROM `business_site`.`image` WHERE `image_id` = ? AND creator_id=?");
				pstmt.setString(1, photoId);
				pstmt.setString(2, acc.getAccountId());
				rs = pstmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						stmt = conn
								.prepareStatement("DELETE FROM `business_site`.`image` WHERE `image_id` = ? AND `creator_id`= ?");
						stmt.setString(1, photoId);
						stmt.setString(3, acc.getAccountId());

						if (stmt.executeUpdate() == 1) {
							// delete from AWS
							deleteAWS(rs.getString("link"), rs.getString("thumb_link"));

							return Response
									.ok(new AlbumImageRepresentation(null, null, photoId, null, null, null, error))
									.cacheControl(utils.cacheControl()).build();

						} else {
							error = utils.createErrorResponse(Status.NOT_FOUND,
									"The user has no photo with the given credentials ");
							return Response.status(Status.NOT_FOUND)
									.entity(new AlbumImageRepresentation(null, null, photoId, null, null, null, error))
									.cacheControl(utils.cacheControl()).build();
						}
					}
				}

				else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has no photo with the given credentials ");
					return Response.status(Status.NOT_FOUND)
							.entity(new AlbumImageRepresentation(null, null, photoId, null, null, null, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to delete photo, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumImageRepresentation(null, null, photoId, null, null, null, error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to delete photo, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumImageRepresentation(null, null, photoId, null, null, null, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete photo, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AlbumImageRepresentation(null, null, photoId, null, null, null, error))
				.cacheControl(utils.cacheControl()).build();

	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getAlbums(@Context SecurityContext sc, @QueryParam("json") final String json) {

		int offset = 0;
		int batch = 10;
		boolean ascending = true;
		boolean getAll = false;
		int modelLimit = 0;
		String creatorId = null;
		String albumId = null;
		String link = null;
		String thumbLink = null;
		String name = null;
		String description = null;
		String imageId = null;
		boolean active = true;
		boolean im = false;
		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("get_all")) {
				getAll = object.getBoolean("get_all");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}
			if (object.keySet().contains("creator_id")) {
				creatorId = object.getString("crteator_id");
			}
			if (object.keySet().contains("album_id")) {
				albumId = object.getString("album_id");
			}
			if (object.keySet().contains("image_id")) {
				imageId = object.getString("image_id");
			}
			if (object.keySet().contains("active")) {
				active = object.getBoolean("active");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = " SELECT image.album_id,COUNT(cnt.image_id) AS nos,image.image_id,image.name,image.description,image.active,image.link,image.thumb_link "
				+ " from `business_site`.`image`"
				+ " LEFT JOIN (SELECT image_id FROM `business_site`.`image` ) cnt "
				+ " ON image.image_id =cnt.image_id ";
		List<AlbumImageRepresentation> list = new ArrayList<AlbumImageRepresentation>();
		AlbumImagesRepresentation response = new AlbumImagesRepresentation();

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (imageId == null) {
					if (albumId == null) {
						if (creatorId == null) {
							if (getAll) {
								if (ascending) {
									sql += " WHERE image.active=?  " + " GROUP BY image_id ORDER BY image_id ASC";
								} else {
									sql += " WHERE image.active=?  " + " GROUP BY image_id ORDER BY image_id DESC";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
							} else {
								if (ascending) {
									sql += " WHERE image.active=?  "
											+ " GROUP BY image_id ORDER BY image_id ASC LIMIT ?,?";
								} else {
									sql += " WHERE image.active=?  "
											+ " GROUP BY image_id ORDER BY image_id DESC LIMIT ?,?";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setInt(2, offset);
								stmt.setInt(3, batch);
							}
						} else {
							if (getAll) {
								if (ascending) {
									sql += " WHERE image.active=? AND image.creator_id=?  "
											+ " GROUP BY image_id ORDER BY image_id ASC";
								} else {
									sql += " WHERE image.active=? AND image.creator_id=?  "
											+ " GROUP BY image_id ORDER BY image_id DESC";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, creatorId);
							} else {
								if (ascending) {
									sql += " WHERE image.active=? AND image.creator_id=?  "
											+ " GROUP BY image_id ORDER BY image_id ASC LIMIT ?,?";
								} else {
									sql += " WHERE image.active=? AND image.creator_id=?  "
											+ " GROUP BY image_id ORDER BY image_id DESC LIMIT ?,?";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, creatorId);
								stmt.setInt(3, offset);
								stmt.setInt(4, batch);
							}
						}

					} else {
						im = true;
						if (creatorId == null) {
							if (getAll) {
								if (ascending) {
									sql += " WHERE image.active=? AND image.album_id=?  "
											+ " GROUP BY image_id ORDER BY image_id ASC";
								} else {
									sql += " WHERE image.active=? AND image.album_id=?  "
											+ " GROUP BY image_id ORDER BY image_id DESC";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, albumId);
							} else {
								if (ascending) {
									sql += " WHERE image.active=? AND image.album_id=?  "
											+ " GROUP BY image_id ORDER BY image_id ASC LIMIT ?,?";
								} else {
									sql += " WHERE image.active=? AND image.album_id=?  "
											+ " GROUP BY image_id ORDER BY image_id DESC LIMIT ?,?";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, albumId);
								stmt.setInt(3, offset);
								stmt.setInt(4, batch);
							}
						} else {
							if (getAll) {
								if (ascending) {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.album_id=?  "
											+ " GROUP BY image_id ORDER BY image_id ASC";
								} else {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.album_id=?  "
											+ " GROUP BY image_id ORDER BY image_id DESC";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, creatorId);
								stmt.setString(3, albumId);
							} else {
								if (ascending) {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.album_id=?  "
											+ " GROUP BY image_id ORDER BY image_id ASC LIMIT ?,?";
								} else {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.album_id=?  "
											+ " GROUP BY image_id ORDER BY image_id DESC LIMIT ?,?";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, creatorId);
								stmt.setString(3, albumId);
								stmt.setInt(4, offset);
								stmt.setInt(5, batch);
							}
						}
					}
				} else {
					if (albumId == null) {
						if (creatorId == null) {
							if (getAll) {
								if (ascending) {
									sql += " WHERE image.active=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id ASC";
								} else {
									sql += " WHERE image.active=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id DESC";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, imageId);
							} else {
								if (ascending) {
									sql += " WHERE image.active=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id ASC LIMIT ?,?";
								} else {
									sql += " WHERE image.active=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id DESC LIMIT ?,?";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, imageId);
								stmt.setInt(3, offset);
								stmt.setInt(4, batch);
							}
						} else {
							if (getAll) {
								if (ascending) {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id ASC";
								} else {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id DESC";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, creatorId);
								stmt.setString(3, imageId);
							} else {
								if (ascending) {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id ASC LIMIT ?,?";
								} else {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id DESC LIMIT ?,?";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, creatorId);
								stmt.setString(3, imageId);
								stmt.setInt(4, offset);
								stmt.setInt(5, batch);
							}
						}
					} else {
						im = true;
						if (creatorId == null) {
							if (getAll) {
								if (ascending) {
									sql += " WHERE image.active=? AND image.album_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id ASC";
								} else {
									sql += " WHERE image.active=? AND image.album_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id DESC";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, albumId);
								stmt.setString(3, imageId);
							} else {
								if (ascending) {
									sql += " WHERE image.active=? AND image.album_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id ASC LIMIT ?,?";
								} else {
									sql += " WHERE image.active=? AND image.album_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id DESC LIMIT ?,?";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, albumId);
								stmt.setString(3, imageId);
								stmt.setInt(4, offset);
								stmt.setInt(5, batch);
							}
						} else {
							if (getAll) {
								if (ascending) {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.album_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id ASC";
								} else {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.album_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id DESC";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, creatorId);
								stmt.setString(3, albumId);
								stmt.setString(4, imageId);
							} else {
								if (ascending) {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.album_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id ASC LIMIT ?,?";
								} else {
									sql += " WHERE image.active=? AND image.creator_id=? AND image.album_id=? AND image.image_id  "
											+ " GROUP BY image_id ORDER BY image_id DESC LIMIT ?,?";
								}
								stmt = conn.prepareStatement(sql);
								stmt.setBoolean(1, active);
								stmt.setString(2, creatorId);
								stmt.setString(3, albumId);
								stmt.setString(4, imageId);
								stmt.setInt(5, offset);
								stmt.setInt(6, batch);
							}
						}
					}
				}
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						modelLimit = rs.getInt("nos");
						link = rs.getString("link");
						thumbLink = rs.getString("thumb_link");
						imageId = rs.getString("image_id");
						albumId = rs.getString("album_id");
						name = rs.getString("name");
						description = rs.getString("description");
						AlbumImageRepresentation imRep = new AlbumImageRepresentation(link, thumbLink, imageId,
								albumId, name, description, error);
						list.add(imRep);
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service categories ");
					return Response.status(Status.NOT_FOUND).entity(response).cacheControl(utils.cacheControl())
							.build();
				}
				response = new AlbumImagesRepresentation(list, offset, batch, modelLimit);
				return Response.ok(response).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to get business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
				.build();
	}
}
