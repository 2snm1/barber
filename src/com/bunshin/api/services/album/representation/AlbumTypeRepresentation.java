package com.bunshin.api.services.album.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.album.AlbumType;
import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "album type")
@InjectLinks({
		@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/album/type/${instance.albumTypeId}", rel = "get", title = "Get Specific Album Type"),
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/album/type/${instance.albumTypeId}", rel = "create", title = "Add Album Type"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/album/type/${instance.albumTypeId}", rel = "edit", title = "Edit Album Type"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/album/type/${instance.albumTypeId}", rel = "del", title = "Delete Album Type") })
public class AlbumTypeRepresentation {
	@XmlElement
	// accessible from the JaXB representation
	private String albumTypeId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String name = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String error = null;

	@InjectLink(resource=AlbumType.class,style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/album/type/${instance.albumTypeId}", rel = "get", title = "Get Specific Album Type"),
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/album/type/${instance.albumTypeId}", rel = "create", title = "Add Album Type"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/album/type/${instance.albumTypeId}", rel = "edit", title = "Edit Album Type"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/album/type/${instance.albumTypeId}", rel = "del", title = "Delete Album Type") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public AlbumTypeRepresentation() {

	}

	public AlbumTypeRepresentation(String albumTypeId, String name,String error) {
		this.albumTypeId = albumTypeId;
		this.name = name;
		this.error=error;
	}

	public String getAlbumTypeId() {
		return albumTypeId;
	}

	public String getName() {
		return name;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if(error!=null){
		 errorMap = new HashMap<>();
		JSONObject errorObject = new JSONObject(error);
		errorMap.put("status", errorObject.get("status"));
		errorMap.put("error message", errorObject.get("error message"));
		errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}

	@Override
	public String toString() {
		return "AlbumTypeRepresentation [albumTypeId=" + albumTypeId + ", name=" + name + ", error=" + error
				+ ", self=" + self + ", links=" + links + "]";
	}

}
