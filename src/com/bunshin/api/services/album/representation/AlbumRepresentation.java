package com.bunshin.api.services.album.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.album.Album;
import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "service")
@InjectLinks({
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/album/${instance.albumId}", rel = "create", title = "Add Album"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/album/${instance.albumId}", rel = "edit", title = "Edit Album"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/album/${instance.albumId}", rel = "del", title = "Delete Album") })
public class AlbumRepresentation {

	@XmlElement
	// accessible from the JaXB representation
	private String albumId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String name = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String description = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String error = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String avi = null;

	public String getAvi() {
		return avi;
	}

	@XmlTransient
	// not accessible in the JaXB representation
	private boolean active = false;
	@XmlTransient
	// not accessible in the JaXB representation
	private String creatorId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String albumType = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String serviceId = null;

	@InjectLink(resource = Album.class, style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/album/${instance.albumId}", rel = "create", title = "Add Album"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/album/${instance.albumId}", rel = "edit", title = "Edit Album"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/album/${instance.albumId}", rel = "del", title = "Delete Album") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public AlbumRepresentation() {

	}

	public AlbumRepresentation(String albumId, String name, String description, boolean active, String creatorId,
			String albumType, String serviceId, String avi, String error) {
		this.albumId = albumId;
		this.name = name;
		this.description = description;
		this.active = active;
		this.creatorId = creatorId;
		this.albumType = albumType;
		this.serviceId = serviceId;
		this.avi = avi;
		this.error = error;
	}

	public String getAlbumId() {
		return albumId;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isActive() {
		return active;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public String getAlbumType() {
		return albumType;
	}

	public String getServiceId() {
		return serviceId;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if (error != null) {
			errorMap = new HashMap<>();
			JSONObject errorObject = new JSONObject(error);
			errorMap.put("status", errorObject.get("status"));
			errorMap.put("error message", errorObject.get("error message"));
			errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}

	@Override
	public String toString() {
		return "AlbumRepresentation [albumId=" + albumId + ", name=" + name + ", description=" + description
				+ ", error=" + error + ", avi=" + avi + ", active=" + active + ", creatorId=" + creatorId
				+ ", albumType=" + albumType + ", serviceId=" + serviceId + ", self=" + self + ", links=" + links + "]";
	}

}
