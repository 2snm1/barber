package com.bunshin.api.services.album;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.services.album.representation.AlbumRepresentation;
import com.bunshin.api.services.album.representation.AlbumsRepresentation;
import com.bunshin.api.utils.Utils;
import com.sematime.api.json.JSONObject;

@Path("resources/album")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class Album {
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(Album.class);
	private Account acc = null;
	private String error = null;

	@PermitAll
	@SecurityCheck
	@PUT
	@Path("{albumId}")
	public Response editAlbum(@Context SecurityContext sc, @PathParam("albumId") final String albumId,
			@FormParam("json") String json) {

		String name = null;
		String description = null;
		String albumType = null;
		String serviceId = null;
		String avi = null;
		boolean active = true;
		acc = (Account) sc.getUserPrincipal();

		try {
			JSONObject object = new JSONObject(json);
			albumType = object.getString("album_type");
			avi = object.getString("avi");
			active = object.getBoolean("active");
			name = object.getString("name");
			description = object.getString("description");
			serviceId = object.getString("service_id");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn
						.prepareStatement("UPDATE `business_site`.`album` SET name=? ,description=?,active=?,album_type=?,service_id=?,avi=?  WHERE  album.creator_id=? AND album.album_id=? ");
				stmt.setString(1, name);
				stmt.setString(2, description);
				stmt.setBoolean(3, active);
				stmt.setString(4, albumType);
				stmt.setString(5, serviceId);
				stmt.setString(6, avi);
				stmt.setString(7, acc.getAccountId());
				stmt.setString(8, albumId);

				if (stmt.executeUpdate() == 1) {
					return Response
							.ok(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(),
									albumType, serviceId, avi, error)).cacheControl(utils.cacheControl()).build();

				} else {
					error = utils
							.createErrorResponse(Status.NOT_FOUND, "The user has not created any business albums ");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(),
									albumType, serviceId, avi, error)).cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to edit album, please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(), albumType,
							serviceId, avi, error)).cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to edit album, please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(),
								albumType, serviceId, avi, error)).cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to edit album, please try again.");
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(), albumType,
						serviceId, avi, error)).cacheControl(utils.cacheControl()).build();

	}

	@SecurityCheck
	@DELETE
	@Path("{albumId}")
	public Response deleteAlbum(@Context SecurityContext sc, @PathParam("albumId") final String albumId) {
		acc = (Account) sc.getUserPrincipal();
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("DELETE FROM `business_site`.`album` WHERE `album_id` = ?");
				stmt.setString(1, albumId);

				if (stmt.executeUpdate() == 1) {

					return Response.ok(new AlbumRepresentation()).cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any album with that id");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new AlbumRepresentation(albumId, null, null, false, acc.getAccountId(), null, null,
									null, error)).cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to delete album , please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumRepresentation(albumId, null, null, false, acc.getAccountId(), null, null, null,
							error)).cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to delete album , please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumRepresentation(albumId, null, null, false, acc.getAccountId(), null, null,
								null, error)).cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete album , please try again.");
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AlbumRepresentation(albumId, null, null, false, acc.getAccountId(), null, null, null, error))
				.cacheControl(utils.cacheControl()).build();

	}

	@PermitAll
	@SecurityCheck
	@POST
	public Response createAlbum(@Context SecurityContext sc, @FormParam("json") final String json) {
		String name = null;
		String description = null;
		boolean active = true;
		String serviceId = null;
		String albumType = null;
		String avi = null;
		acc = (Account) sc.getUserPrincipal();
		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");
			description = object.getString("description");
			active = object.getBoolean("active");
			serviceId = object.getString("service_id");
			albumType = object.getString("album_type");
			avi = object.getString("avi");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		String albumId = String.valueOf(System.currentTimeMillis());
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`album` VALUES (?,?,?,?,?,?,?,?)");
				stmt.setString(1, albumId);
				stmt.setString(2, name);
				stmt.setString(3, description);
				stmt.setBoolean(4, active);
				stmt.setString(5, acc.getAccountId());
				stmt.setString(6, albumType);
				stmt.setString(7, serviceId);
				stmt.setString(8, avi);

				if (stmt.executeUpdate() == 1) {
					return Response
							.ok(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(),
									albumType, serviceId, avi, error)).cacheControl(utils.cacheControl()).build();
				} else {

					error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to create album, please try again.");
					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(),
									albumType, serviceId, avi, error)).cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to create album, please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(), albumType,
							serviceId, avi, error)).cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to create album, please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(),
								albumType, serviceId, avi, error)).cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to create album, please try again.");
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AlbumRepresentation(albumId, name, description, active, acc.getAccountId(), albumType,
						serviceId, avi, error)).cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getAlbums(@Context SecurityContext sc, @QueryParam("json") final String json) {

		int offset = 0;
		int batch = 10;
		boolean ascending = true;
		boolean getAll = false;
		int modelLimit = 0;
		String albumId = null;
		String creatorId = null;
		String serviceId = null;
		String albumType = null;
		String avi = null;
		String name = null;
		String description = null;
		boolean active = true;

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("get_all")) {
				getAll = object.getBoolean("get_all");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

			if (object.keySet().contains("album_type")) {
				albumType = object.getString("album_type");
			}
			if (object.keySet().contains("creator_id")) {
				creatorId = object.getString("creator_id");
			}
			if (object.keySet().contains("service_id")) {
				serviceId = object.getString("service_id");
			}
			if (object.keySet().contains("album_id")) {
				albumId = object.getString("album_id");
			}
			if (object.keySet().contains("active")) {
				active = object.getBoolean("active");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = " SELECT album.album_id,album.service_id,album.name,album.description,album.active,album.creator_id,album.album_type,album.avi,"
				+ " COUNT(cnt.album_id) AS nos "
				+ " from `business_site`.`album`"
				+ " LEFT JOIN (SELECT album_id FROM `business_site`.`album` ) cnt "
				+ " ON cnt.album_id=album.album_id ";
		List<AlbumRepresentation> list = new ArrayList<AlbumRepresentation>();
		AlbumsRepresentation response = new AlbumsRepresentation();

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (albumType == null) {
					if (serviceId == null) {
						if (albumId == null) {
							if (creatorId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? " + " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? " + " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? " + " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? " + " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setInt(2, offset);
									stmt.setInt(3, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND  album.creator_id=? "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND  album.creator_id=? "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.creator_id=? "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.creator_id=? "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							}
						} else {
							if (creatorId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? " + " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? " + " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.album_id=?  "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.album_id=?  "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, albumId);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_id=?  "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_id=?  "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumId);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_id=?  "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_id=? "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumId);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							}
						}
					} else {
						if (albumId == null) {
							if (creatorId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=?  AND album.service_id=?   "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND album.service_id=?  "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, serviceId);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.service_id=?  "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.service_id=?  "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, serviceId);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.service_id=?  "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND  album.creator_id=?  AND album.service_id=? "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, serviceId);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.service_id=?  "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.service_id=?  "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, serviceId);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							}
						} else {
							if (creatorId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=?  AND album.service_id=? "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND album.service_id=?  "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, serviceId);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.album_id=? AND album.service_id=?   "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.album_id=? AND album.service_id=?   "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, albumId);
									stmt.setString(3, serviceId);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_id=? AND album.service_id=?   "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_id=? AND album.service_id=?   "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumId);
									stmt.setString(4, serviceId);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_id=? AND album.service_id=?   "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_id=? AND album.service_id=? "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumId);
									stmt.setString(4, serviceId);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							}
						}
					}
				} else {
					if (serviceId == null) {
						if (albumId == null) {
							if (creatorId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND album.album_type=?  "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND album.album_type=?  "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, albumType);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.album_type=?  "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.album_type=?  "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, albumType);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_type=?  "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_type=?  "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumType);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_type=?  "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_type=?  "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumType);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							}
						} else {
							if (creatorId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND album.album_type=?  "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND album.album_type=?  "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, albumType);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.album_id=? AND album.album_type=?   "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.album_id=? AND album.album_type=?   "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, albumId);
									stmt.setString(3, albumType);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_id=? AND album.album_type=?   "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_id=? AND album.album_type=?   "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumId);
									stmt.setString(4, albumType);

								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_id=? AND album.album_type=?   "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_id=? AND album.album_type=?  "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumId);
									stmt.setString(4, albumType);
									stmt.setInt(5, offset);
									stmt.setInt(6, batch);
								}
							}
						}
					} else {
						if (albumId == null) {
							if (creatorId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=?  AND album.service_id=? AND album.album_type=?    "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND album.service_id=? AND album.album_type=?   "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, serviceId);
									stmt.setString(3, albumType);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.service_id=?  AND album.album_type=?  "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.service_id=? AND album.album_type=?   "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, serviceId);
									stmt.setString(3, albumType);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.service_id=? AND album.album_type=?   "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND  album.creator_id=?  AND album.service_id=? AND album.album_type=?  "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, serviceId);
									stmt.setString(4, albumType);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.service_id=? AND album.album_type=?   "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.service_id=? AND album.album_type=?   "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, serviceId);
									stmt.setString(4, albumType);
									stmt.setInt(5, offset);
									stmt.setInt(6, batch);
								}
							}
						} else {
							if (creatorId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=?  AND album.service_id=?  AND album.album_type=? "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND album.service_id=?  AND album.album_type=?  "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, serviceId);
									stmt.setString(3, albumType);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.album_id=? AND album.service_id=? AND album.album_type=?    "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.album_id=? AND album.service_id=? AND album.album_type=?  "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, albumId);
									stmt.setString(3, serviceId);
									stmt.setString(4, albumType);
									stmt.setInt(5, offset);
									stmt.setInt(6, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_id=? AND album.service_id=? AND album.album_type=?    "
												+ " ORDER BY album_id ASC";
									} else {
										sql += " WHERE album.active=? AND  album.creator_id=? AND album.album_id=? AND album.service_id=? AND album.album_type=? "
												+ " ORDER BY album_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumId);
									stmt.setString(4, serviceId);
									stmt.setString(5, albumType);
								} else {
									if (ascending) {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_id=? AND album.service_id=? AND album.album_type=?    "
												+ " ORDER BY album_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE album.active=? AND album.creator_id=? AND album.album_id=? AND album.service_id=? AND album.album_type=? "
												+ " ORDER BY album_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, active);
									stmt.setString(2, acc.getAccountId());
									stmt.setString(3, albumId);
									stmt.setString(4, serviceId);
									stmt.setString(5, albumType);
									stmt.setInt(6, offset);
									stmt.setInt(7, batch);
								}
							}
						}
					}
				}
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						modelLimit = rs.getInt("nos");
						description = rs.getString("description");
						albumId = rs.getString("album_id");
						name = rs.getString("name");
						active = rs.getBoolean("active");
						creatorId = rs.getString("creator_id");
						albumType = rs.getString("album_type");
						serviceId = rs.getString("service_id");
						avi = rs.getString("avi");
						AlbumRepresentation album=new AlbumRepresentation(albumId, name, description, active, creatorId, albumType,
								serviceId, avi, error);
						list.add(album);
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service categories ");
					return Response.status(Status.NOT_FOUND).entity(response).cacheControl(utils.cacheControl())
							.build();
				}
				response = new AlbumsRepresentation(list, offset, batch, modelLimit);
				return Response.ok(response).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to get business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
				.build();
	}

}
