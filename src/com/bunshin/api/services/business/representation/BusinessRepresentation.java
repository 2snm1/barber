package com.bunshin.api.services.business.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.business.Business;
import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "business")
@InjectLinks({
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/${instance.businessId}", rel = "create", title = "Add Business"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/${instance.businessId}", rel = "edit", title = "Edit Business"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/${instance.businessId}", rel = "del", title = "Delete Business") })
public class BusinessRepresentation {

	@XmlElement
	// accessible from the JaXB representation
	private String businessId = null;
	@XmlElement
	// not accessible in the JaXB representation
	private String location = null;
	@XmlElement
	// not accessible in the JaXB representation
	private String name = null;
	@XmlElement
	// not accessible in the JaXB representation
	private String error = null;
	@XmlElement
	// not accessible in the JaXB representation
	private String avi = null;
	@XmlElement
	// not accessible in the JaXB representation
	private String userId = null;
	@XmlElement
	// not accessible in the JaXB representation
	private boolean status = true;

	@InjectLink(resource = Business.class, style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/${instance.businessId}", rel = "create", title = "Add Business"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/${instance.businessId}", rel = "edit", title = "Edit Business"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/${instance.businessId}", rel = "del", title = "Delete Business") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public BusinessRepresentation() {

	}

	public BusinessRepresentation(String businessId, String location, String name, String avi, String userId,
			boolean status,String error) {
		this.businessId = businessId;
		this.location = location;
		this.name = name;
		this.avi = avi;
		this.userId = userId;
		this.status = status;
		this.error=error;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if(error!=null){
		 errorMap = new HashMap<>();
		JSONObject errorObject = new JSONObject(error);
		errorMap.put("status", errorObject.get("status"));
		errorMap.put("error message", errorObject.get("error message"));
		errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}


	public String getBusinessId() {
		return businessId;
	}

	public String getLocation() {
		return location;
	}

	public String getName() {
		return name;
	}

	public String getAvi() {
		return avi;
	}

	public String getUserId() {
		return userId;
	}

	public boolean isStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "BusinessRepresentation [businessId=" + businessId + ", location=" + location + ", name=" + name
				+ ", error=" + error + ", avi=" + avi + ", userId=" + userId + ", status=" + status + ", self=" + self
				+ ", links=" + links + "]";
	}

}
