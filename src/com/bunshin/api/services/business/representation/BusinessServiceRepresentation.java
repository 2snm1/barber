package com.bunshin.api.services.business.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.business.BusinessService;
import com.bunshin.api.utils.Utils;
import com.google.gson.Gson;
import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "business service")
@InjectLinks({
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/service/?json=%7B%22businesst%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%7D", rel = "create", title = "Add Service"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/service/?json=%7B%22businesst%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%7D", rel = "edit", title = "Edit Service"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/service/?json=%7B%22businesst%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%7D", rel = "del", title = "Delete Service") })
public class BusinessServiceRepresentation {
	@XmlElement
	// accessible from the JaXB representation
	private String serviceId = null;
	@XmlElement
	// accessible from the JaXB representation
	private String businessId = null;
	@XmlElement
	// accessible from the JaXB representation
	private String error = null;

	@InjectLink(resource = BusinessService.class, style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/service/?json=%7B%22businesst%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%7D", rel = "create", title = "Add Service"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/service/?json=%7B%22businesst%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%7D", rel = "edit", title = "Edit Service"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/service/?json=%7B%22businesst%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%7D", rel = "del", title = "Delete Service") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public BusinessServiceRepresentation() {

	}

	public BusinessServiceRepresentation(String serviceId, String businessId, String error) {
		this.serviceId = serviceId;
		this.businessId = businessId;
		this.error = error;
	}

	public String getServiceId() {
		return serviceId;
	}

	public String getBusinessId() {
		return businessId;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if(error!=null){
		 errorMap = new HashMap<>();
		JSONObject errorObject = new JSONObject(error);
		errorMap.put("status", errorObject.get("status"));
		errorMap.put("error message", errorObject.get("error message"));
		errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}

	@Override
	public String toString() {
		return "BusinessServiceRepresentation [serviceId=" + serviceId + ", businessId=" + businessId + ", error="
				+ error + ", self=" + self + ", links=" + links + "]";
	}

}
