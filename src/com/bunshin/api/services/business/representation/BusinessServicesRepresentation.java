package com.bunshin.api.services.business.representation;

import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLink.Style;
import org.glassfish.jersey.linking.InjectLinks;

import com.bunshin.api.services.business.BusinessService;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "services")
@InjectLinks({
		@InjectLink(value = "/resources/business/service/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%,%22business%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%%7", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
		@InjectLink(value = "/resources/business/service/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%,%22business%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%%7", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
public class BusinessServicesRepresentation {
	@XmlElement
	private String businessId = null;
	@XmlElement
	private String serviceId = null;
	@XmlElement
	private List<BusinessServiceRepresentation> services;
	@XmlElement
	private int offset = 0;
	@XmlElement
	private int limit = 10;
	@XmlElement
	private int modelLimit = 0;

	@InjectLink(type = "query", style = Style.ABSOLUTE, resource = BusinessService.class, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(value = "/resources/business/service/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%,%22business%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%%7", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
			@InjectLink(value = "/resources/business/service/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%,%22business%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22%%7", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public BusinessServicesRepresentation() {

	}

	public BusinessServicesRepresentation(String businessId, String serviceId,
			List<BusinessServiceRepresentation> services, int offset, int limit, int modelLimit) {
		this.businessId = businessId;
		this.serviceId = serviceId;
		this.services = services;
		this.offset = offset;
		this.limit = limit;
		this.modelLimit = modelLimit;
	}

	public String getBusinessId() {
		return businessId;
	}

	public String getserviceId() {
		return serviceId;
	}

	public List<BusinessServiceRepresentation> getServices() {
		return services;
	}

	public int getOffset() {
		return offset;
	}

	public int getLimit() {
		return limit;
	}

	public int getModelLimit() {
		return modelLimit;
	}

	@Override
	public String toString() {
		return "BusinessServicesRepresentation [businessId=" + businessId + ", serviceId=" + serviceId + ", services="
				+ services + ", offset=" + offset + ", limit=" + limit + ", modelLimit=" + modelLimit + ", self="
				+ self + ", links=" + links + "]";
	}

}
