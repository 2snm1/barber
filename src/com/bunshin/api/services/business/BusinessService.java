package com.bunshin.api.services.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.services.business.representation.BusinessServiceRepresentation;
import com.bunshin.api.services.business.representation.BusinessServicesRepresentation;
import com.bunshin.api.services.business.service.representation.ServiceRepersentation;
import com.bunshin.api.services.business.service.representation.ServicesRepresentation;
import com.bunshin.api.utils.Utils;
import com.google.gson.Gson;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.sematime.api.json.JSONObject;

@Path("resources/business/service/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class BusinessService {
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(BusinessService.class);
	private String error = null;

	@PermitAll
	@SecurityCheck
	@POST
	public Response createBusinessService(@Context SecurityContext sc, @FormParam("json") final String json) {
		String businessId = null;
		String serviceId = null;

		try {
			JSONObject object = new JSONObject(json);
			businessId = object.getString("business_id");
			serviceId = object.getString("service_id");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}
		if (serviceId.equals("1")) {
			return Response.status(Status.FORBIDDEN)
					.entity(utils.createErrorResponse(Status.FORBIDDEN, "You cannot create a Personal service"))
					.cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`business_services` VALUES (?,?)");
				stmt.setString(1, businessId);
				stmt.setString(2, serviceId);

				if (stmt.executeUpdate() == 1) {// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");
					HashMap<String, Object> service = new HashMap<String, Object>();
					HashMap<String, String> partial = new HashMap<String, String>();
					service.put("index_name", "business_site");
					service.put("type_name", "business");
					service.put("id", businessId);
					partial.put("services", serviceId);
					service.put("partial", partial);
					utils.consumeMessage("ADD PARTIAL", "direct", "search", props.build(), true);
					utils.queueMessage("ADD PARTIAL", "direct", "search", props.build(), service);
					return Response.ok(new BusinessServiceRepresentation(serviceId, businessId, error))
							.cacheControl(utils.cacheControl()).build();
				} else {

					error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to register, please try again.");
					return Response.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new BusinessServiceRepresentation(serviceId, businessId, error))
							.cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (MySQLIntegrityConstraintViolationException msq) {
			msq.printStackTrace();
			error = utils
					.createErrorResponse(Status.CONFLICT, "DUPLICATE ENTRY:Your business already has that service");
			return Response.status(Status.CONFLICT)
					.entity(new BusinessServiceRepresentation(serviceId, businessId, error))
					.cacheControl(utils.cacheControl()).build();
		} catch (Exception e) {
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to create business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new BusinessServiceRepresentation(serviceId, businessId, error))
					.cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to create business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new BusinessServiceRepresentation(serviceId, businessId, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to create business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new BusinessServiceRepresentation(serviceId, businessId, error))
				.cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@DELETE
	@Path("{businessId}/{serviceId}")
	public Response deleteService(@Context SecurityContext sc, @PathParam("businessId") final String businessId,
			@PathParam("serviceId") final String serviceId) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn
						.prepareStatement("DELETE FROM `business_site`.`business_services` WHERE `services_id` = ? AND business_id=?");
				stmt.setString(1, serviceId);
				stmt.setString(2, businessId);

				if (stmt.executeUpdate() == 1) {
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");
					HashMap<String, Object> service = new HashMap<String, Object>();
					HashMap<String, String> partial = new HashMap<String, String>();
					service.put("index_name", "business_site");
					service.put("type_name", "business");
					service.put("id", businessId);
					partial.put("services", serviceId);
					service.put("partial", partial);
					utils.consumeMessage("DELETE PARTIAL", "direct", "search", props.build(), true);
					utils.queueMessage("DELETE PARTIAL", "direct", "search", props.build(), service);
					return Response.ok(new BusinessServiceRepresentation(null, null, error))
							.cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service with that id");
					System.out.println(error);
					return Response.status(Status.NOT_FOUND)
							.entity(new BusinessServiceRepresentation(serviceId, businessId, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to delete business service , please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new BusinessServiceRepresentation(serviceId, businessId, error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to delete business service , please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new BusinessServiceRepresentation(serviceId, businessId, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete business service , please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new BusinessServiceRepresentation(serviceId, businessId, error))

				.cacheControl(utils.cacheControl()).build();

	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getBusinessServices(@Context SecurityContext sc, @QueryParam("json") final String json) {

		int offset = 0;
		int batch = 10;
		boolean ascending = true;
		boolean getAll = false;
		int modelLimit = 0;
		String businessId = null;
		String serviceId = null;

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("get_all")) {
				getAll = object.getBoolean("get_all");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

			if (object.keySet().contains("business_id")) {
				businessId = object.getString("business_id");
			}

			if (object.keySet().contains("service_id")) {
				serviceId = object.getString("service_id");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = " SELECT business_services.business_id,business_services.services_id,COUNT(cnt.services_id) AS nos  from `business_site`.`business_services`"
				+ " LEFT JOIN (SELECT services_id FROM `business_site`.`business_services` ) cnt "
				+ " ON business_services.services_id IN (SELECT services_id FROM `business_site`.`business_services` )  ";
		List<BusinessServiceRepresentation> list = new ArrayList<BusinessServiceRepresentation>();
		BusinessServicesRepresentation response = new BusinessServicesRepresentation();

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (serviceId == null) {
					if (businessId == null) {
						if (getAll) {
							if (ascending) {
								sql += " GROUP BY services_id  ORDER BY services_id ASC";

							} else {
								sql += " GROUP BY services_id  ORDER BY services_id DESC";
							}
							stmt = conn.prepareStatement(sql);
						} else {
							if (ascending) {
								sql += " GROUP BY services_id  ORDER BY services_id ASC LIMIT ?,?";
							} else {
								sql += " GROUP BY services_id  ORDER BY services_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setInt(1, offset);
							stmt.setInt(2, batch);
						}
					} else {
						if (getAll) {
							if (ascending) {
								sql += " WHERE business_services.business_id=? "
										+ " GROUP BY services_id  ORDER BY services_id ASC";
							} else {
								sql += " WHERE business_services.business_id=? "
										+ " GROUP BY services_id  ORDER BY services_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setString(1, businessId);
						} else {
							if (ascending) {
								sql += " WHERE business_services.business_id=? "
										+ " GROUP BY services_id  ORDER BY services_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE business_services.business_id=? "
										+ " GROUP BY services_id  ORDER BY services_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setString(1, businessId);
							stmt.setInt(2, offset);
							stmt.setInt(3, batch);
						}
					}
				} else {
					if (businessId == null) {
						if (getAll) {
							if (ascending) {
								sql += " WHERE business_services.services_id=? "
										+ " GROUP BY services_id  ORDER BY services_id ASC";
							} else {
								sql += " WHERE business_services.services_id=? "
										+ " GROUP BY services_id  ORDER BY services_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setString(1, serviceId);
						} else {
							if (ascending) {
								sql += " WHERE business_services.services_id=? "
										+ " GROUP BY services_id  ORDER BY services_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE business_services.services_id=? "
										+ " GROUP BY services_id  ORDER BY services_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setString(1, serviceId);
							stmt.setInt(2, offset);
							stmt.setInt(3, batch);
						}
					} else {
						if (getAll) {
							if (ascending) {
								sql += " WHERE business_services.business_id=? AND business_services.services_id=? "
										+ " GROUP BY services_id  ORDER BY services_id ASC";
							} else {
								sql += " WHERE business_services.business_id=? AND business_services.services_id=?  "
										+ " GROUP BY services_id  ORDER BY services_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setString(1, businessId);
							stmt.setString(2, serviceId);
						} else {
							if (ascending) {
								sql += " WHERE business_services.business_id=? AND business_services.services_id=?  "
										+ " GROUP BY services_id  ORDER BY services_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE business_services.business_id=?  AND business_services.services_id=? "
										+ " GROUP BY services_id  ORDER BY services_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setString(1, businessId);
							stmt.setString(2, serviceId);
							stmt.setInt(3, offset);
							stmt.setInt(4, batch);
						}
					}
				}
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						modelLimit = rs.getInt("nos");
						businessId = rs.getString("business_id");
						serviceId = rs.getString("service_id");
						BusinessServiceRepresentation biz = new BusinessServiceRepresentation(serviceId, businessId,
								error);
						list.add(biz);
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service categories ");
					return Response.status(Status.NOT_FOUND).entity(response).cacheControl(utils.cacheControl())
							.build();
				}
				response = new BusinessServicesRepresentation(businessId, serviceId, list, offset, batch, modelLimit);
				return Response.ok(response).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to get business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
				.build();
	}

}
