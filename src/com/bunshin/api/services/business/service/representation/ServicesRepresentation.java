package com.bunshin.api.services.business.service.representation;

import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.business.service.Service;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "services")
@InjectLinks({
		@InjectLink(value = "/resources/business/service/type/?json=%7B%22offset%22:%22${instance.offset + instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
		@InjectLink(value = "/resources/business/service/type/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
public class ServicesRepresentation {

	@XmlElement
	private List<ServiceRepersentation> services;
	@XmlElement
	private int offset = 0;
	@XmlElement
	private int limit = 0;
	@XmlElement
	private int modelLimit = 0;

	@InjectLink(type = "query", style = Style.ABSOLUTE,resource=Service.class, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;
	
	@InjectLinks({
			@InjectLink(value = "/resources/business/service/type/?json=%7B%22offset%22:%22${instance.offset + instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
			@InjectLink(value = "/resources/business/service/type/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public ServicesRepresentation() {

	}

	public ServicesRepresentation(List<ServiceRepersentation> services, int offset, int limit, int modelLimit) {
		this.services = services;
		this.offset = offset;
		this.limit = limit;
		this.modelLimit = modelLimit;
	}

	public List<ServiceRepersentation> getServices() {
		return services;
	}

	public int getOffset() {
		return offset;
	}

	public int getLimit() {
		return limit;
	}

	public int getModelLimit() {
		return modelLimit;
	}

	@Override
	public String toString() {
		return "ServicesRepresentation [services=" + services + ", offset=" + offset + ", limit=" + limit
				+ ", modelLimit=" + modelLimit + ", self=" + self + ", links=" + links + "]";
	}

}
