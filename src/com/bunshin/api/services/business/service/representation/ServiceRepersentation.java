package com.bunshin.api.services.business.service.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.business.BusinessService;
import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "service")
@InjectLinks({
		@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/business/service/type/${instance.serviceId}", rel = "get", title = "Get Specific Service"),
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/service/type/${instance.serviceId}", rel = "create", title = "Add Service"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/service/type/${instance.serviceId}", rel = "edit", title = "Edit Service"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/service/type/${instance.serviceId}", rel = "del", title = "Delete Service") })
public class ServiceRepersentation {
	@XmlElement
	// accessible from the JaXB representation
	private String serviceId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String name = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String description = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String error = null;

	@InjectLink(resource = BusinessService.class, style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/business/service/type/${instance.serviceId}", rel = "get", title = "Get Specific Service"),
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/service/type/${instance.serviceId}", rel = "create", title = "Add Service"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/service/type/${instance.serviceId}", rel = "edit", title = "Edit Service"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/service/type/${instance.serviceId}", rel = "del", title = "Delete Service") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public ServiceRepersentation() {

	}

	public ServiceRepersentation(String serviceId, String name, String description, String error) {
		this.serviceId = serviceId;
		this.name = name;
		this.description = description;
		this.error = error;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if(error!=null){
		 errorMap = new HashMap<>();
		JSONObject errorObject = new JSONObject(error);
		errorMap.put("status", errorObject.get("status"));
		errorMap.put("error message", errorObject.get("error message"));
		errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}

	public String getServiceId() {
		return serviceId;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "ServiceRepersentation [serviceId=" + serviceId + ", name=" + name + ", description=" + description
				+ ", error=" + error + ", self=" + self + ", links=" + links + "]";
	}

}
