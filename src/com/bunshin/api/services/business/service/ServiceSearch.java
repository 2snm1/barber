package com.bunshin.api.services.business.service;

import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.NodeOperation;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.services.business.service.representation.ServiceRepersentation;
import com.bunshin.api.services.business.service.representation.ServicesRepresentation;
import com.bunshin.api.services.user.representation.UserRepresentation;
import com.bunshin.api.services.user.representation.UsersRepresentation;
import com.bunshin.api.utils.Utils;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.gson.Gson;
import com.sematime.api.json.JSONObject;

@Path("resources/service/search")
@Consumes({ MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON })
@Produces(MediaType.APPLICATION_JSON)
public class ServiceSearch {
	Account acc = null;
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(ServiceSearch.class);

	@SecurityCheck
	@GET
	@Path("ayt")
	public Response check(@QueryParam("json") final String json, @Context SecurityContext sc) {
		acc = (Account) sc.getUserPrincipal();
		String text = null;
		List<ServiceRepersentation> list = new ArrayList<ServiceRepersentation>();
		try {
			JSONObject object = new JSONObject(json);

			text = object.getString("text").trim();

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}

		SearchResponse response = NodeOperation.INSTANCE.getClient().prepareSearch("business_site").setTypes("users")
				.setSearchType(SearchType.QUERY_THEN_FETCH).setQuery(QueryBuilders.multiMatchQuery(text, "name"))
				.setSize(10).execute().actionGet();
		System.out.println("response " + response + "hits ");

		for (SearchHit hit : response.getHits()) {
			Map<String, Object> hitSource = hit.getSource();
			ServiceRepersentation serviceRep = new ServiceRepersentation(hitSource.get("service_id").toString(),
					hitSource.get("name").toString(), hitSource.get("description").toString(), null);

			list.add(serviceRep);

		}
		ServicesRepresentation services = new ServicesRepresentation(list, 0, 10, 0);
		return Response.ok(services).cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@GET
	@Path("simple")
	public Response search(@QueryParam("offset") int offset,@QueryParam("batch")int batch,@QueryParam("json") final String json, @Context SecurityContext sc) {
		acc = (Account) sc.getUserPrincipal();
		JSONObject object = null;
		List<ServiceRepersentation> list = new ArrayList<ServiceRepersentation>();
		try {
			object = new JSONObject(json);

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		Map<String, String> scope = new HashMap<String, String>();
		Set<String> keys = object.keySet();
		for (String key : keys.toArray(new String[keys.size()])) {

			scope.put(key, object.getString(key));
		}
		MustacheFactory mf = new DefaultMustacheFactory();
		Mustache mustache = mf.compile(
				new InputStreamReader(getClass().getResourceAsStream(
						"/com/bunshin/api/search/template/simple_service.mustache")), "simple_service.mustache");
		StringWriter writer = new StringWriter();
		mustache.execute(writer, scope);
		writer.flush();
		System.out.println(writer.toString());
		SearchResponse response = NodeOperation.INSTANCE.getClient().prepareSearch("business_site")
				.setTypes("business").setSearchType(SearchType.QUERY_THEN_FETCH).setQuery(writer.toString())
				.setSize(batch).setFrom(offset).execute().actionGet();

		for (SearchHit hit : response.getHits()) {
			Map<String, Object> hitSource = hit.getSource();
			ServiceRepersentation serviceRep = new ServiceRepersentation(hitSource.get("service_id").toString(),
					hitSource.get("name").toString(), hitSource.get("description").toString(), null);

			list.add(serviceRep);

		}
		ServicesRepresentation services = new ServicesRepresentation(list, 0, 10, 0);
		return Response.ok(services).cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@GET
	@Path("complex")
	public Response multisearch(@QueryParam("offset") int offset,@QueryParam("batch")int batch,@QueryParam("json") final String json, @Context SecurityContext sc) {
		acc = (Account) sc.getUserPrincipal();
		JSONObject object = null;
		List<ServiceRepersentation> list = new ArrayList<ServiceRepersentation>();
		try {
			object = new JSONObject(json);

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":{"
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}}").cacheControl(utils.cacheControl()).build();
		}
		Gson gson = new Gson();

		Map<String, Object> scope = new HashMap<String, Object>();
		Set<String> keys = object.keySet();
		for (String key : keys.toArray(new String[keys.size()])) {

			scope.put(key, gson.fromJson(object.getJSONObject(key).toString(), HashMap.class));
		}

		MustacheFactory mf = new DefaultMustacheFactory();
		Mustache mustache = mf.compile(
				new InputStreamReader(getClass().getResourceAsStream(
						"/com/bunshin/api/search/template/complex_service.mustache")), "complex_service.mustache");
		StringWriter writer = new StringWriter();
		mustache.execute(writer, scope);
		writer.flush();

		NodeOperation.INSTANCE.getClient().admin().cluster().prepareHealth().setWaitForGreenStatus()
				.setTimeout(TimeValue.timeValueMillis(250)).execute().actionGet();
		SearchResponse response = NodeOperation.INSTANCE.getClient().prepareSearch("business_site")
				.setTypes("business").setSearchType(SearchType.QUERY_THEN_FETCH).setQuery(writer.toString())
				.setSize(batch).setFrom(offset).execute().actionGet();

		for (SearchHit hit : response.getHits()) {
			Map<String, Object> hitSource = hit.getSource();
			ServiceRepersentation serviceRep = new ServiceRepersentation(hitSource.get("service_id").toString(),
					hitSource.get("name").toString(), hitSource.get("description").toString(), null);

			list.add(serviceRep);

		}
		ServicesRepresentation services = new ServicesRepresentation(list, 0, 10, 0);
		return Response.ok(services).cacheControl(utils.cacheControl()).build();
	}

}
