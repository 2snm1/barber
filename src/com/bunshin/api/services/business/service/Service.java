package com.bunshin.api.services.business.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.services.business.service.representation.ServiceRepersentation;
import com.bunshin.api.services.business.service.representation.ServicesRepresentation;
import com.bunshin.api.utils.Utils;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.sematime.api.json.JSONObject;

@Path("resources/business/service/type")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class Service {
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(Service.class);
	private String error = null;

	@PermitAll
	@SecurityCheck
	@POST
	@RolesAllowed("Administrator")
	public Response createService(@Context SecurityContext sc, @FormParam("json") final String json) {
		String name = null;
		String description = null;

		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");
			description = object.getString("description");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		if (name == null) {
			log.error(Status.BAD_REQUEST + "No name accompanying service ");
			return Response
					.status(Status.BAD_REQUEST)
					.entity(utils
							.createErrorResponse(
									Status.BAD_REQUEST,
									"Service name missing .The business service should be accompanied with a name and a description is also advised but not necessary "))
					.cacheControl(utils.cacheControl()).build();
		}

		String serviceId = String.valueOf(System.currentTimeMillis());
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`services` VALUES (?,?,?)");
				stmt.setString(1, serviceId);
				stmt.setString(2, name);
				stmt.setString(3, description);

				if (stmt.executeUpdate() == 1) {
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.currentTimeMillis())).type("custom");
					HashMap<String, Object> service = new HashMap<String, Object>();
					service.put("index_name", "business_site");
					service.put("type_name", "services");
					service.put("source", "{" + "\"services_id\":\"" + serviceId + "\"," + "\"name\":\"" + name + "\","
							+ "\"description\":\"" + description + "\"}");

					utils.consumeMessage("INPUT", "direct", "search", props.build(), true);
					utils.queueMessage("INPUT", "direct", "search", props.build(), service);

					return Response.ok(new ServiceRepersentation(serviceId, name, description, error))
							.cacheControl(utils.cacheControl()).build();
				} else {

					error = utils
							.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Server error: Oops something went wrong while trying to create business service, please try again.");
					return Response.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new ServiceRepersentation(serviceId, null, null, error))
							.cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to create business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new ServiceRepersentation(serviceId, null, null, error)).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to create business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new ServiceRepersentation(serviceId, null, null, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to create business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new ServiceRepersentation(serviceId, null, null, error)).cacheControl(utils.cacheControl())
				.build();
	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getServices(@Context SecurityContext sc, @QueryParam("json") final String json) {
		// TODO find error
		int offset = 0;
		int batch = 10;
		boolean ascending = true;
		boolean getAll = false;
		int modelLimit = 0;
		String serviceId = null;
		String name = null;
		String description = null;

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("get_all")) {
				getAll = object.getBoolean("get_all");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = " SELECT services.services_id,services.name,services.description,COUNT(cnt.services_id) AS nos "
				+ " from `business_site`.`services`"
				+ " LEFT JOIN (SELECT services_id FROM `business_site`.`services` ) cnt "
				+ " ON services.services_id IN (SELECT services_id FROM `business_site`.`services` ) ";
		List<ServiceRepersentation> list = new ArrayList<ServiceRepersentation>();
		ServicesRepresentation response = new ServicesRepresentation();

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (getAll) {
					if (ascending) {
						sql += " GROUP BY services_id ORDER BY services_id ASC";
					} else {
						sql += " GROUP BY services_id ORDER BY services_id DESC";
					}
					stmt = conn.prepareStatement(sql);
				} else {
					if (ascending) {
						sql += " GROUP BY services_id ORDER BY services_id ASC LIMIT ?,?";
					} else {
						sql += " GROUP BY services_id ORDER BY services_id DESC LIMIT ?,?";
					}
					stmt = conn.prepareStatement(sql);
					stmt.setInt(1, offset);
					stmt.setInt(2, batch);
				}
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						modelLimit = rs.getInt("nos");
						serviceId = rs.getString("services_id");
						name = rs.getString("name");
						description = rs.getString("description");
						ServiceRepersentation service = new ServiceRepersentation(serviceId, name, description, error);
						list.add(service);
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The service has not created any business service categories ");
					return Response.status(Status.NOT_FOUND).entity(response).cacheControl(utils.cacheControl())
							.build();
				}
				response = new ServicesRepresentation(list, offset, batch, modelLimit);
				return Response.ok(response).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to get business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
				.build();
	}

	@SecurityCheck
	@GET
	@Path("{serviceId}")
	public Response getService(@Context SecurityContext sc, @PathParam("serviceId") final String serviceId) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("SELECT * FROM `business_site`.`services` WHERE  `services_id`=?");
				stmt.setString(1, serviceId);
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						return Response
								.ok(new ServiceRepersentation(rs.getString("services_id"), rs.getString("name"), rs
										.getString("description"), error)).cacheControl(utils.cacheControl()).build();

					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The service has not created any business service categories with such an id");
					return Response.status(Status.NOT_FOUND)
							.entity(new ServiceRepersentation(serviceId, null, null, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new ServiceRepersentation(serviceId, null, null, error)).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to get business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new ServiceRepersentation(serviceId, null, null, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new ServiceRepersentation(serviceId, null, null, error)).cacheControl(utils.cacheControl())
				.build();
	}

	@SecurityCheck
	@DELETE
	@Path("{serviceId}")
	@RolesAllowed("Administrator")
	public Response deleteService(@Context SecurityContext sc, @PathParam("serviceId") final String serviceId) {

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("DELETE FROM `business_site`.`services` WHERE `services_id` = ?");
				stmt.setString(1, serviceId);

				if (stmt.executeUpdate() == 1) {
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");
					HashMap<String, Object> service = new HashMap<String, Object>();
					service.put("index_name", "business_site");
					service.put("type_name", "services");
					service.put("id", serviceId);
					utils.consumeMessage("DELETE", "direct", "search", props.build(), true);
					utils.queueMessage("DELETE", "direct", "search", props.build(), service);
					return Response.ok(new ServiceRepersentation(serviceId, null, null, error))
							.cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The service has not created any business service with that id");
					return Response.status(Status.NOT_FOUND)
							.entity(new ServiceRepersentation(serviceId, null, null, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils
					.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
							"Server error: Oops something went wrong while trying to delete business service , please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new ServiceRepersentation(serviceId, null, null, error)).cacheControl(utils.cacheControl())
					.build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to delete business service , please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new ServiceRepersentation(serviceId, null, null, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete business service , please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new ServiceRepersentation(serviceId, null, null, error))

				.cacheControl(utils.cacheControl()).build();

	}

	@PermitAll
	@SecurityCheck
	@RolesAllowed("Administrator")
	@PUT
	@Path("{serviceId}")
	public Response editService(@Context SecurityContext sc, @PathParam("serviceId") final String serviceId,
			@FormParam("json") String json) {
		String name = null;
		String description = null;

		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");
			description = object.getString("description");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn
						.prepareStatement("UPDATE `business_site`.`services` SET name=? ,description=?  WHERE  services_id=?");
				stmt.setString(1, name);
				stmt.setString(2, description);
				stmt.setString(3, serviceId);

				if (stmt.executeUpdate() == 1) {// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");

					HashMap<String, Object> service = new HashMap<String, Object>();
					HashMap<String, String> partial = new HashMap<String, String>();

					service.put("index_name", "business_site");
					service.put("type_name", "services");
					service.put("id", serviceId);
					partial.put("name", name);
					partial.put("description", description);
					service.put("partial", partial);

					utils.consumeMessage("PARTIAL", "direct", "search", props.build(), true);
					utils.queueMessage("PARTIAL", "direct", "search", props.build(), service);

					return Response.ok(new ServiceRepersentation(serviceId, name, description, error))
							.cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The service has not created any business service categories ");
					return Response.status(Status.NOT_FOUND)
							.entity(new ServiceRepersentation(serviceId, name, description, error))
							.cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to edit business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new ServiceRepersentation(serviceId, name, description, error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to edit business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new ServiceRepersentation(serviceId, name, description, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to edit business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new ServiceRepersentation(serviceId, name, description, error))
				.cacheControl(utils.cacheControl()).build();

	}
}
