package com.bunshin.api.services.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.services.business.representation.BusinessRepresentation;
import com.bunshin.api.services.business.representation.BusinessesRepresentation;
import com.bunshin.api.utils.Utils;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.sematime.api.json.JSONObject;

@Path("resources/business")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class Business {
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(Business.class);
	private Account acc = null;
	private String error = null;

	@PermitAll
	@SecurityCheck
	@POST
	public Response createBusiness(@Context SecurityContext sc, @FormParam("json") final String json) {

		String name = null;
		String location = null;
		boolean status = true;
		String avi = null;
		acc = (Account) sc.getUserPrincipal();
		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");
			location = object.getString("location");
			status = object.getBoolean("status");
			avi = object.getString("avi");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		if (name == null) {
			log.error(Status.BAD_REQUEST + "No name accompanying business ");
			return Response
					.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST,
							"Business name missing .The business should be accompanied with a name "))
					.cacheControl(utils.cacheControl()).build();
		}

		String businessId = String.valueOf(System.currentTimeMillis());
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("INSERT INTO `business_site`.`business` VALUES (?,?,?,?,?,?)");
				stmt.setString(1, businessId);
				stmt.setString(3, name);
				stmt.setString(2, location);
				stmt.setString(4, avi);
				stmt.setString(5, acc.getAccountId());
				stmt.setBoolean(6, status);

				if (stmt.executeUpdate() == 1) {// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("shule-mart").deliveryMode(2)
							.messageId(String.valueOf(System.currentTimeMillis())).type("custom");

					HashMap<String, Object> business = new HashMap<String, Object>();
					business.put("index_name", "business");
					business.put("type_name", "business");
					business.put("source", "{" + "\"user_id\":\"" + acc.getAccountId() + "\"," + "\"business_id\":\""
							+ businessId + "\"," + "\"status\":\"" + status + "\"," + "\"services\":[\"\"],"
							+  "\"name\":\"" + name + "\"," + "\"location\":\""
							+ location + "\"}");

					utils.consumeMessage("INPUT", "direct", "search", props.build(), true);
					utils.queueMessage("INPUT", "direct", "search", props.build(), business);
					return Response
							.ok(new BusinessRepresentation(businessId, location, name, avi, acc.getAccountId(), status,
									error)).cacheControl(utils.cacheControl()).build();
				} else {

					error = utils
							.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Server error: Oops something went wrong while trying to create business, please try again.");
					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new BusinessRepresentation(null, location, name, avi, acc.getAccountId(), status,
									error)).cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to create business, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new BusinessRepresentation(null, location, name, avi, acc.getAccountId(), status, error))
					.cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to create business, please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new BusinessRepresentation(null, location, name, avi, acc.getAccountId(), status, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to create business, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new BusinessRepresentation(null, location, name, avi, acc.getAccountId(), status, error))
				.cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getBusinesses(@Context SecurityContext sc, @QueryParam("json") final String json) {
		// TODO get error
		int offset = 0;
		int batch = 10;
		boolean ascending = true;
		boolean getAll = false;
		int modelLimit = 0;
		String creatorId = null;
		String businessId = null;
		boolean status = true;
		boolean biz = false;
		String location = null;
		String name = null;
		String avi = null;

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("get_all")) {
				getAll = object.getBoolean("get_all");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}
			if (object.keySet().contains("creator_id")) {
				creatorId = object.getString("creator_id");
			}
			if (object.keySet().contains("business_id")) {
				businessId = object.getString("business_id");
			}
			if (object.keySet().contains("status")) {
				status = object.getBoolean("status");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = " SELECT business.business_id,business.name,business.location,business.avi,business.creator_id,business.status,COUNT(cnt.business_id) AS nos "
				+ " from `business_site`.`business`"
				+ " LEFT JOIN (SELECT business_id FROM `business_site`.`business` ) cnt "
				+ " ON business.business_id IN (SELECT business_id FROM `business_site`.`business` ) ";
		List<BusinessRepresentation> list = new ArrayList<BusinessRepresentation>();
		BusinessesRepresentation response = new BusinessesRepresentation();

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (businessId == null) {
					if (creatorId == null) {
						if (getAll) {
							if (ascending) {
								sql += " WHERE business.status=? " + " GROUP BY business_id ORDER BY business_id ASC";
							} else {
								sql += " WHERE business.status=? " + " GROUP BY business_id ORDER BY business_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, status);
						} else {
							if (ascending) {
								sql += " WHERE business.status=? "
										+ " GROUP BY business_id ORDER BY business_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE business.status=? "
										+ " GROUP BY business_id ORDER BY business_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, status);
							stmt.setInt(2, offset);
							stmt.setInt(3, batch);
						}
					} else {
						if (getAll) {
							if (ascending) {
								sql += " WHERE business.status=? AND business.creator_id=? "
										+ " GROUP BY business_id ORDER BY business_id ASC";
							} else {
								sql += " WHERE business.status=? AND business.creator_id=? "
										+ " GROUP BY business_id ORDER BY business_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, status);
							stmt.setString(2, creatorId);
						} else {
							if (ascending) {
								sql += " WHERE business.status=? AND business.creator_id=? "
										+ " GROUP BY business_id ORDER BY business_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE business.status=? AND business.creator_id=? "
										+ " GROUP BY business_id ORDER BY business_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, status);
							stmt.setString(2, creatorId);
							stmt.setInt(3, offset);
							stmt.setInt(4, batch);
						}
					}
				} else {
					biz = true;
					if (creatorId == null) {
						if (getAll) {
							if (ascending) {
								sql += " WHERE business.status=? AND business.business_id=? "
										+ " GROUP BY business_id ORDER BY business_id ASC";
							} else {
								sql += " WHERE business.status=? AND business.business_id=? "
										+ " GROUP BY business_id ORDER BY business_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, status);
							stmt.setString(2, businessId);
						} else {
							if (ascending) {
								sql += " WHERE business.status=? AND business.business_id=? "
										+ " GROUP BY business_id ORDER BY business_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE business.status=? AND business.business_id=? "
										+ " GROUP BY business_id ORDER BY business_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, status);
							stmt.setString(2, businessId);
							stmt.setInt(3, offset);
							stmt.setInt(4, batch);
						}
					} else {
						if (getAll) {
							if (ascending) {
								sql += " WHERE business.status=? AND business.creator_id=? AND business.business_id=? "
										+ " GROUP BY business_id ORDER BY business_id ASC";
							} else {
								sql += " WHERE business.status=? AND business.creator_id=? AND business.business_id=? "
										+ " GROUP BY business_id ORDER BY business_id DESC";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, status);
							stmt.setString(2, creatorId);
							stmt.setString(3, businessId);
						} else {
							if (ascending) {
								sql += " WHERE business.status=? AND business.creator_id=? AND business.business_id=? "
										+ " GROUP BY business_id ORDER BY business_id ASC LIMIT ?,?";
							} else {
								sql += " WHERE business.status=? AND business.creator_id=? AND business.business_id=? "
										+ " GROUP BY business_id ORDER BY business_id DESC LIMIT ?,?";
							}
							stmt = conn.prepareStatement(sql);
							stmt.setBoolean(1, status);
							stmt.setString(2, creatorId);
							stmt.setString(3, businessId);
							stmt.setInt(4, offset);
							stmt.setInt(5, batch);
						}
					}
				}
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						modelLimit = rs.getInt("nos");
						location = rs.getString("location");
						name = rs.getString("name");
						businessId = rs.getString("business_id");
						creatorId = rs.getString("creator_id");
						avi = rs.getString("avi");
						BusinessRepresentation bizRep = new BusinessRepresentation(businessId, location, name, avi,
								creatorId, status, error);
						if (biz) {
							return Response.ok(bizRep).cacheControl(utils.cacheControl()).build();
						}
						list.add(bizRep);
						System.out.println(list);
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business categories ");
					return Response.status(Status.NOT_FOUND).entity(response).cacheControl(utils.cacheControl())
							.build();
				}
				response = new BusinessesRepresentation(list, offset, batch, modelLimit);
				System.out.println(response);
				Gson gson = new Gson();
				return Response.ok(response).cacheControl(utils.cacheControl()).build();

			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to get business, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
				.build();
	}

	@SecurityCheck
	@DELETE
	@Path("{businessId}")
	public Response deleteBusiness(@Context SecurityContext sc, @PathParam("businessId") final String businessId) {
		Account acc = (Account) sc.getUserPrincipal();

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn.prepareStatement("DELETE FROM `business_site`.`business` WHERE `business_id` = ?");
				stmt.setString(1, businessId);

				if (stmt.executeUpdate() == 1) {
					// produce and consume
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");
					HashMap<String, Object> business = new HashMap<String, Object>();
					business.put("index_name", "business_site");
					business.put("type_name", "business");
					business.put("id", businessId);
					utils.consumeMessage("DELETE", "direct", "search", props.build(), true);
					utils.queueMessage("DELETE", "direct", "search", props.build(), business);
					return Response
							.ok(new BusinessRepresentation(businessId, null, null, null, acc.getAccountId(), false,
									error)).cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business with that id");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new BusinessRepresentation(businessId, null, null, null, acc.getAccountId(), false,
									error)).cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to delete business , please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new BusinessRepresentation(businessId, null, null, null, acc.getAccountId(), false, error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to delete business , please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new BusinessRepresentation(businessId, null, null, null, acc.getAccountId(), false,
								error)).entity("{\"error\" : {" + error + "\" }").cacheControl(utils.cacheControl())
						.build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete business , please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new BusinessRepresentation(businessId, null, null, null, acc.getAccountId(), false, error))

				.entity("{\"error\" : {" + error + " }").cacheControl(utils.cacheControl()).build();

	}

	@PermitAll
	@SecurityCheck
	@PUT
	@Path("{businessId}")
	public Response editBusiness(@Context SecurityContext sc, @PathParam("businessId") final String businessId,
			@FormParam("json") String json) {
		String name = null;
		String location = null;
		String avi = null;
		boolean status = true;
		acc = (Account) sc.getUserPrincipal();

		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");
			status = object.getBoolean("status");
			avi = object.getString("avi");
			location = object.getString("location");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn
						.prepareStatement("UPDATE `business_site`.`business` SET name=? ,location=?,avi=?,status=?  WHERE business_id=? AND  creator_id=?");
				stmt.setString(1, name);
				stmt.setString(2, location);
				stmt.setString(3, avi);
				stmt.setBoolean(4, status);
				stmt.setString(6, acc.getAccountId());
				stmt.setString(5, businessId);

				if (stmt.executeUpdate() == 1) {
					BasicProperties.Builder props = new BasicProperties.Builder();
					props.contentType("Text/Plain").priority(0).appId("barber").deliveryMode(2)
							.messageId(String.valueOf(System.nanoTime())).type("custom");

					HashMap<String, Object> business = new HashMap<String, Object>();
					HashMap<String, String> partial = new HashMap<String, String>();

					business.put("index_name", "business_site");
					business.put("type_name", "business");
					business.put("id", businessId);
					partial.put("name", name);
					partial.put("location", location);
					partial.put("avi", avi);
					partial.put("status", String.valueOf(status));
					business.put("partial", partial);

					utils.consumeMessage("PARTIAL", "direct", "search", props.build(), true);
					utils.queueMessage("PARTIAL", "direct", "search", props.build(), business);
					return Response
							.ok(new BusinessRepresentation(businessId, location, name, avi, acc.getAccountId(), status,
									error)).cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business categories ");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new BusinessRepresentation(businessId, location, name, avi, acc.getAccountId(),
									status, error)).cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to edit business, please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new BusinessRepresentation(businessId, location, name, avi, acc.getAccountId(), status,
							error)).cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to edit business, please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new BusinessRepresentation(businessId, location, name, avi, acc.getAccountId(), status,
								error)).cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to edit business, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new BusinessRepresentation(businessId, location, name, avi, acc.getAccountId(), status, error))
				.cacheControl(utils.cacheControl()).build();

	}
}
