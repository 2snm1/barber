package com.bunshin.api.services.websockets;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.atmosphere.config.service.AtmosphereService;
import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceEventListenerAdapter;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.DefaultBroadcaster;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.sematime.api.json.JSONObject;
import com.bunshin.api.Account;
import com.bunshin.api.utils.Utils;

@Path("async/ws")
@Consumes({ MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON })
@Produces(MediaType.APPLICATION_JSON)
@AtmosphereService(path = "/barber/async/ws/", listeners = { com.bunshin.api.services.websockets.WS.OnDisconnect.class }, dispatch = false, interceptors = { AtmosphereResourceLifecycleInterceptor.class }, servlet = "org.glassfish.jersey.servlet.ServletContainer")
public class WS {
	public final static String HEARTBEAT_FUTURE = "heartbeat.future";
	private static Account acc = null;
	private static ConcurrentHashMap<String, Object> users = null;
	private static Utils utils = new Utils();
	// private List<String> groupMemberList = null;
	// private final static ConcurrentHashMap<String, List<String>> group = new
	// ConcurrentHashMap<String, List<String>>();

	// private final static ConcurrentHashMap<String,
	// List<ConcurrentHashMap<String, Object>>> schoolUsers = new
	// ConcurrentHashMap<String, List<ConcurrentHashMap<String, Object>>>();
	private final static List<ConcurrentHashMap<String, Object>> user = Collections

	.synchronizedList(new LinkedList<>());
	static ConcurrentHashMap<String, Object> uuid = new ConcurrentHashMap<String, Object>();
	private final static ConcurrentHashMap<String, ConcurrentHashMap<String, Object>> userUuid = new ConcurrentHashMap<String, ConcurrentHashMap<String, Object>>();

	private AtmosphereResource userResource = null;
	private BroadcasterFactory bf = null;

	private HashMap<String, Object> auth = null;

	@POST
	public Response send(String json, @QueryParam("AUTHORIZATION") String authorization, @Context HttpServletRequest req) {
		userResource = (AtmosphereResource) req.getAttribute(ApplicationConfig.ATMOSPHERE_RESOURCE);
		bf = userResource.getAtmosphereConfig().getBroadcasterFactory();

		String type = null;
		String action = null;
		String value = null;
		try {

			JSONObject object = new JSONObject(json);
			value = object.getString("value");
			type = object.getString("type");
			action = object.getString("action");

		} catch (Exception e) {
			e.printStackTrace();

			Response res = Response
					.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							).cacheControl(utils.cacheControl()).build();
			return res;
		}
		auth = utils.suspendedAuthentication(authorization);

		if (auth.containsKey("false")) {
			return Response.status(Status.FORBIDDEN).entity(auth.get("false")).build();
		}
		acc = (Account) auth.get("true");
		if (type.contains("notification")) {
			if (action.equalsIgnoreCase("PUT")) {
				return Response.status(Status.FORBIDDEN)
						.entity(utils.createErrorResponse(Status.FORBIDDEN, "invalid choice")).build();
			} else if (action.equalsIgnoreCase("DELETE")) {

				// register notification
				DefaultBroadcaster jerseyCast = bf.lookup("notification/" + acc.getAccountId());
				jerseyCast.removeAtmosphereResource(userResource);

				return Response.ok("{\"type\":\"ack\",\"value\":\"notification\",\"action\":\"remove\"}").build();

			} else if (action.equalsIgnoreCase("POST")) {
				return Response.status(Status.FORBIDDEN)
						.entity(utils.createErrorResponse(Status.FORBIDDEN, "invalid choice")).build();
			} else if (action.equalsIgnoreCase("GET")) {

				// register notification
				DefaultBroadcaster jerseyCast = bf.lookup(DefaultBroadcaster.class,
						"notification/" + acc.getAccountId(), true);
				if (jerseyCast == null) {
					jerseyCast = bf.get(DefaultBroadcaster.class, "notification/" + acc.getAccountId());
				}
				jerseyCast.addAtmosphereResource(userResource);
				return Response.ok("{\"type\":\"ack\",\"value\":\"notification\",\"action\":\"add\"}").build();

			} else {

				return Response.status(Status.FORBIDDEN)
						.entity(utils.createErrorResponse(Status.FORBIDDEN, "invalid choice")).build();
			}
		}

		return Response.status(Status.FORBIDDEN).entity(utils.createErrorResponse(Status.FORBIDDEN, "invalid choice"))
				.build();
	}

	public static final class OnDisconnect extends AtmosphereResourceEventListenerAdapter {
		private final Logger logger = LoggerFactory.getLogger(WS.class);

		/**
		 * have a get method to create the broadcasters ad link them to related
		 * resources a POST method to post messages it gets type from message
		 * and deals with them chat if user not online sed notifiction
		 */

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onDisconnect(AtmosphereResourceEvent event) {

			System.out.println("on disconnect");
			try {
				// AtmosphereResource userResource = (AtmosphereResource) event
				// .getResource().getRequest()
				// .getAttribute(ApplicationConfig.ATMOSPHERE_RESOURCE);
				AtmosphereResource userResource = event.getResource();
				BroadcasterFactory bf = event.getResource().getAtmosphereConfig().getBroadcasterFactory();
				System.out.println("bf: " + bf);
				System.out.println("bc: " + event.broadcaster());
				DefaultBroadcaster jerseyCast = bf.lookup("/barber/async/ws/", false);
				System.out.println("jc " + jerseyCast);
				String accountId = (String) userUuid.get(event.getResource().uuid()).get("id");
				if (event.isCancelled()) {
					logger.info("Browser {} unexpectedly disconnected (cancelled)", event.getResource().uuid());
					synchronized (user) {

						user.remove(userUuid.get(event.getResource().uuid()));

						userUuid.remove(Utils.userAcc.get(accountId));

						Utils.userAcc.remove(accountId);

					}
					Gson gson = new Gson();

					System.out.println("jcb0 " + jerseyCast);
					Future<?> f = (Future<?>) event.getResource().getRequest().getAttribute(HEARTBEAT_FUTURE);
					if (f != null)
						f.cancel(false);
					event.getResource().getRequest().removeAttribute(HEARTBEAT_FUTURE);
					userResource.close();

					System.out.println("ur " + userResource);

					System.out.println("jcb " + jerseyCast);

					jerseyCast.broadcast(gson.toJson(user));

				} else if (event.isClosedByClient()) {
					System.out.println("dfg 2");
					logger.info("Browser {} closed the connection", event.getResource().uuid());

					synchronized (user) {

						user.remove(userUuid.get(event.getResource().uuid()));

						userUuid.remove(Utils.userAcc.get(accountId));

						Utils.userAcc.remove(accountId);
					}
					Gson gson = new Gson();

					Future<?> f = (Future<?>) event.getResource().getRequest().getAttribute(HEARTBEAT_FUTURE);
					if (f != null)
						f.cancel(false);
					event.getResource().getRequest().removeAttribute(HEARTBEAT_FUTURE);
					userResource.close();

					System.out.println("ur " + userResource);

					System.out.println("jcb " + jerseyCast);

					jerseyCast.broadcast(gson.toJson(user));
				} else if (event.isClosedByApplication()) {
					System.out.println("dfg 3");
					logger.info("Browser {} closed the connection", event.getResource().uuid());

					synchronized (user) {

						user.remove(userUuid.get(event.getResource().uuid()));

						userUuid.remove(Utils.userAcc.get(accountId));

						Utils.userAcc.remove(accountId);
					}
					Gson gson = new Gson();

					Future<?> f = (Future<?>) event.getResource().getRequest().getAttribute(HEARTBEAT_FUTURE);
					if (f != null)
						f.cancel(false);
					event.getResource().getRequest().removeAttribute(HEARTBEAT_FUTURE);
					userResource.close();

					System.out.println("ur " + userResource);

					System.out.println("jcb " + jerseyCast);

					jerseyCast.broadcast(gson.toJson(user));

				} else {

					System.out.println("dfg 3");
					logger.info("Browser {} closed the connection", event.getResource().uuid());

					synchronized (user) {

						user.remove(userUuid.get(event.getResource().uuid()));

						userUuid.remove(Utils.userAcc.get(accountId));

						Utils.userAcc.remove(accountId);
					}
					Gson gson = new Gson();
					Future<?> f = (Future<?>) event.getResource().getRequest().getAttribute(HEARTBEAT_FUTURE);
					if (f != null)
						f.cancel(false);
					event.getResource().getRequest().removeAttribute(HEARTBEAT_FUTURE);
					jerseyCast.removeAtmosphereResource(userResource);
					userResource.close();

					System.out.println("ur " + userResource);

					System.out.println("jcb " + jerseyCast);

					jerseyCast.broadcast(gson.toJson(user));

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onSuspend(AtmosphereResourceEvent event) {
			// TODO Auto-generated method stub
			System.out.println("suspend: ");
			// TODO check validity of use of UserState
			HashMap<String, Object> auth = null;

			AtmosphereResource userResource = (AtmosphereResource) event.getResource().getRequest()
					.getAttribute(ApplicationConfig.ATMOSPHERE_RESOURCE);
			BroadcasterFactory bf = userResource.getAtmosphereConfig().getBroadcasterFactory();
			DefaultBroadcaster jerseyCast = bf.lookup(DefaultBroadcaster.class, "/barber/async/ws/", true);
			if (jerseyCast == null) {
				jerseyCast = bf.get(DefaultBroadcaster.class, "/barber/async/ws/");
			}
			try {
				auth = utils.suspendedAuthentication(event.getResource().getRequest().queryStringsMap()
						.get("AUTHORIZATION")[0]);
				Gson gson = new Gson();
				if (auth.containsKey("true")) {
					System.out.println("is true");
					acc = (Account) auth.get("true");

					users = new ConcurrentHashMap<String, Object>();
					users.put("id", acc.getAccountId());
					users.put("name", acc.getName());
					synchronized (user) {

						if (Utils.userAcc.contains(acc.getAccountId())) {
							AtmosphereResource res = (AtmosphereResource) uuid
									.get(Utils.userAcc.get(acc.getAccountId()));
							res.getBroadcaster().broadcast("{\"type\":\"unsubscribe\"}");
							Future<?> f = (Future<?>) res.getRequest().getAttribute(HEARTBEAT_FUTURE);
							if (f != null)
								f.cancel(false);
							res.getRequest().removeAttribute(HEARTBEAT_FUTURE);
							userUuid.remove(Utils.userAcc.get(acc.getAccountId()));
							uuid.remove(Utils.userAcc.get(acc.getAccountId()));
							Utils.userAcc.remove(acc.getAccountId());
						}

						Utils.userAcc.put(acc.getAccountId(), event.getResource().uuid());

						userUuid.putIfAbsent(event.getResource().uuid(), users);
						uuid.put(event.getResource().uuid(), userResource);
						if (!user.contains(users)) {
							user.add(users);
						}
					}
				} else {
					userResource.getBroadcaster().broadcast(auth.get("false"));
					jerseyCast.removeAtmosphereResource(userResource);
				}
				jerseyCast.broadcast("{\"type\":\"ack\",\"value\":\"suspended\"}");
				// to see all the people who are online
				jerseyCast.broadcast(gson.toJson(user));

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				acc = null;
			}
		}
	}

}
