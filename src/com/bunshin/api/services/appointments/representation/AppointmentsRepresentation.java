package com.bunshin.api.services.appointments.representation;

import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.album.Album;
import com.bunshin.api.services.business.service.representation.ServiceRepersentation;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "appointments")
@InjectLinks({
		@InjectLink(value = "/resources/business/appointment/?json=%7B%22offset%22:%22${instance.offset + instance.limit}%22,%22limit%22:%22${instance.limit}%22,%22business%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22,%22user%22:%22${instance.userId}%22,%22time%22:%22${instance.time}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
		@InjectLink(value = "/resources/business/appointment/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22,%22business%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22,%22user%22:%22${instance.userId}%22,%22time%22:%22${instance.time}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
public class AppointmentsRepresentation {
	@XmlElement
	private String businessId = null;
	@XmlElement
	private String serviceId = null;
	@XmlElement
	private String time = null;
	@XmlElement
	private String userId = null;
	@XmlElement
	private List<AppointmentRepresentation> services;
	@XmlTransient
	private int offset = 0;
	@XmlTransient
	private int limit = 0;
	private int modelLimit = 0;

	@InjectLink(type = "query", style = Style.ABSOLUTE, resource=Album.class, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(value = "/resources/business/appointment/?json=%7B%22offset%22:%22${instance.offset + instance.limit}%22,%22limit%22:%22${instance.limit}%22,%22business%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22,%22user%22:%22${instance.userId}%22,%22time%22:%22${instance.time}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset + instance.limit < instance.modelLimit}", rel = "next"),
			@InjectLink(value = "/resources/business/appointment/?json=%7B%22offset%22:%22${instance.offset - instance.limit}%22,%22limit%22:%22${instance.limit}%22,%22business%22:%22${instance.businessId}%22,%22service%22:%22${instance.serviceId}%22,%22user%22:%22${instance.userId}%22,%22time%22:%22${instance.time}%22%7D", style = Style.ABSOLUTE, method = "query", condition = "${instance.offset - instance.limit >= 0}", rel = "prev") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public AppointmentsRepresentation() {

	}

	public AppointmentsRepresentation(String businessId, String serviceId, String time, String userId,
			List<AppointmentRepresentation> services, int offset, int limit, int modelLimit) {
		this.businessId = businessId;
		this.serviceId = serviceId;
		this.time = time;
		this.userId = userId;
		this.services = services;
		this.offset = offset;
		this.limit = limit;
		this.modelLimit = modelLimit;
	}

	public String getBusinessId() {
		return businessId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public String getTime() {
		return time;
	}

	public String getUserId() {
		return userId;
	}

	public List<AppointmentRepresentation> getServices() {
		return services;
	}

	public int getOffset() {
		return offset;
	}

	public int getLimit() {
		return limit;
	}

	public int getModelLimit() {
		return modelLimit;
	}

	@Override
	public String toString() {
		return "AppointmentsRepresentation [businessId=" + businessId + ", serviceId=" + serviceId + ", time=" + time
				+ ", userId=" + userId + ", services=" + services + ", offset=" + offset + ", limit=" + limit
				+ ", modelLimit=" + modelLimit + ", self=" + self + ", links=" + links + "]";
	}

}
