package com.bunshin.api.services.appointments.representation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.glassfish.jersey.linking.InjectLink.Style;

import com.bunshin.api.services.appointments.Appointment;
import com.sematime.api.json.JSONObject;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "appointment")
@InjectLinks({
		@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/business/appointment/${instance.appointmentId}", rel = "get", title = "Get Specific Appointment"),
		@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/appointment/${instance.appointmentId}", rel = "create", title = "Add Appointment"),
		@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/appointment/${instance.appointmentId}", rel = "edit", title = "Edit Appointment"),
		@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/appointment/${instance.appointmentId}", rel = "del", title = "Delete appointment") })
public class AppointmentRepresentation {
	@XmlElement
	// accessible from the JaXB representation
	private String appointmentId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private int repeatFrequency = 0;
	@XmlTransient
	// not accessible in the JaXB representation
	private String time = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String businessId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String serviceId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String serviceName = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String serviceDescription = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String userId = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String userName = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String userThumbPic = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String businessName = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String error = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private String businessAvi = null;
	@XmlTransient
	// not accessible in the JaXB representation
	private boolean done=false;
	@XmlTransient
	// not accessible in the JaXB representation
	private String businessCreator = null;

	@InjectLink(resource = Appointment.class, style = Style.ABSOLUTE, rel = "self")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link")
	Link self;

	@InjectLinks({
			@InjectLink(type = "GET", style = Style.ABSOLUTE, value = "/resources/business/appointment/${instance.appointmentId}", rel = "get", title = "Get Specific Appointment"),
			@InjectLink(type = "POST", style = Style.ABSOLUTE, value = "/resources/business/appointment/${instance.appointmentId}", rel = "create", title = "Add Appointment"),
			@InjectLink(type = "PUT", style = Style.ABSOLUTE, value = "/resources/business/appointment/${instance.appointmentId}", rel = "edit", title = "Edit Appointment"),
			@InjectLink(type = "DELETE", style = Style.ABSOLUTE, value = "/resources/business/appointment/${instance.appointmentId}", rel = "del", title = "Delete appointment") })
	@XmlElement(name = "link")
	@XmlElementWrapper(name = "links")
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	List<Link> links;

	public AppointmentRepresentation() {

	}

	public AppointmentRepresentation(String appointmentId, String time, String businessId, String serviceId,
			String serviceName, String serviceDescription, String userId, String userName, String userThumbPic,
			String businessName, String businessAvi, String businessCreator, int repeatFrequency,boolean done, String error) {
		this.appointmentId = appointmentId;
		this.time = time;
		this.businessId = businessId;
		this.serviceId = serviceId;
		this.serviceName = serviceName;
		this.serviceDescription = serviceDescription;
		this.userId = userId;
		this.userName = userName;
		this.userThumbPic = userThumbPic;
		this.businessName = businessName;
		this.businessAvi = businessAvi;
		this.businessCreator = businessCreator;
		this.repeatFrequency = repeatFrequency;
		this.done=done;
		this.error = error;
	}

	public int getRepeatFrequency() {
		return repeatFrequency;
	}

	public String getAppointmentId() {
		return appointmentId;
	}

	public String getTime() {
		return time;
	}

	public String getBusinessId() {
		return businessId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public String getUserThumbPic() {
		return userThumbPic;
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getBusinessAvi() {
		return businessAvi;
	}

	public String getBusinessCreator() {
		return businessCreator;
	}

	public Map<String, Object> getError() {
		Map<String, Object> errorMap = null;
		if(error!=null){
		 errorMap = new HashMap<>();
		JSONObject errorObject = new JSONObject(error);
		errorMap.put("status", errorObject.get("status"));
		errorMap.put("error message", errorObject.get("error message"));
		errorMap.put("description", errorObject.get("description"));
		}

		return errorMap;
	}

	public boolean isDone() {
		return done;
	}

	@Override
	public String toString() {
		return "AppointmentRepresentation [appointmentId=" + appointmentId + ", repeatFrequency=" + repeatFrequency
				+ ", time=" + time + ", businessId=" + businessId + ", serviceId=" + serviceId + ", serviceName="
				+ serviceName + ", serviceDescription=" + serviceDescription + ", userId=" + userId + ", userName="
				+ userName + ", userThumbPic=" + userThumbPic + ", businessName=" + businessName + ", error=" + error
				+ ", businessAvi=" + businessAvi + ", done=" + done + ", businessCreator=" + businessCreator
				+ ", self=" + self + ", links=" + links + "]";
	}

}
