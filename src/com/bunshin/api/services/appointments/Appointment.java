package com.bunshin.api.services.appointments;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bunshin.api.Account;
import com.bunshin.api.MyDataSource;
import com.bunshin.api.SecurityCheck;
import com.bunshin.api.services.appointments.representation.AppointmentRepresentation;
import com.bunshin.api.services.appointments.representation.AppointmentsRepresentation;
import com.bunshin.api.utils.Utils;
import com.sematime.api.json.JSONObject;

@Path("resources/business/appointment")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class Appointment {
	private Utils utils = new Utils();
	private final Logger log = LoggerFactory.getLogger(Appointment.class);
	private Account acc = null;
	private String error = null;

	@PermitAll
	@SecurityCheck
	@POST
	public Response createAppointment(@Context SecurityContext sc, @FormParam("json") final String json) {
		String businessId = null;
		String serviceId = null;
		String time = null;
		acc = (Account) sc.getUserPrincipal();
		String serviceName = null;
		String serviceDescription = null;
		String businessName = null;
		String businessAvi = null;
		String businessCreator = null;
		int repeatFrequency = 0;
		String description = null;
		String appointmentTime = null;

		try {
			JSONObject object = new JSONObject(json);
			businessId = object.getString("business_id");
			serviceId = object.getString("service_id");
			time = object.getString("time");
			serviceName = object.getString("service_name");
			serviceDescription = object.getString("service_description");
			businessName = object.getString("business_name");
			businessAvi = object.getString("business_avi");
			businessCreator = object.getString("business_creator");
			repeatFrequency = object.getInt("repeat_frequency");
			description = object.getString("description");
			appointmentTime = object.getString("appointment_time");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}
		Date today = new Date();
		if (new Date(Long.parseLong(time)).before(today)) {
			return Response
					.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "The start date must be after "
							+ new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(today)))
					.cacheControl(utils.cacheControl()).build();
		}
		if (new Date(Long.parseLong(appointmentTime)).before(today)) {
			return Response
					.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "The start date must be after "
							+ new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(today)))
					.cacheControl(utils.cacheControl()).build();
		}

		String appointmentId = String.valueOf(System.currentTimeMillis());
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.parseLong(time));

		int minute = cal.get(Calendar.MINUTE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		int month = (cal.get(Calendar.MONTH));
		String expression = null;
		String jobName = "appointment-" + appointmentId;

		if (repeatFrequency == 1) {
			expression = "0 " + minute + " " + hour + " * * ?";
		}
		if (repeatFrequency == 2) {
			expression = "0 " + minute + " " + hour + " ? * " + dayOfWeek + " *";
		}
		if (repeatFrequency == 3) {
			expression = "0 " + minute + " " + hour + " " + dayOfMonth + " 1/1 ? *";
		}
		if (repeatFrequency == 4) {
			expression = "0 " + minute + " " + hour + " " + dayOfMonth + " " + (month + 1) + " ? *";
		}
		if (repeatFrequency == 0) {
			expression = "0 " + minute + " " + hour + " " + dayOfMonth + " " + (month + 1) + " ?";
		}

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn
						.prepareStatement("INSERT INTO `business_site`.`appointments` VALUES (?,?,?,?,?,?,?,?,false)");
				stmt.setString(1, appointmentId);
				stmt.setString(2, businessId);
				stmt.setString(3, serviceId);
				stmt.setString(4, acc.getAccountId());
				stmt.setString(5, time);
				stmt.setInt(6, repeatFrequency);
				stmt.setString(7, description);
				stmt.setString(8, appointmentTime);

				if (stmt.executeUpdate() == 1) {
					utils.scheduleJob(expression, jobName, new Date(Long.parseLong(time)), acc.getAccountId());
					return Response
							.ok(new AppointmentRepresentation(appointmentId, time, businessId, serviceId, serviceName,
									serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(),
									businessName, businessAvi, businessCreator, repeatFrequency, false, error))
							.cacheControl(utils.cacheControl()).build();
				} else {

					error = utils
							.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Server error: Oops something went wrong while trying to create appointment, please try again.");
					return Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.entity(new AppointmentRepresentation(null, time, businessId, serviceId, serviceName,
									serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(),
									businessName, businessAvi, businessCreator, repeatFrequency, false, error))
							.cacheControl(utils.cacheControl()).build();

				}

			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to create appointment, please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AppointmentRepresentation(null, time, businessId, serviceId, serviceName,
							serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(), businessName,
							businessAvi, businessCreator, repeatFrequency, false, error))
					.cacheControl(utils.cacheControl()).build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to create appointment, please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AppointmentRepresentation(null, time, businessId, serviceId, serviceName,
								serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(), businessName,
								businessAvi, businessCreator, repeatFrequency, false, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}

		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to create appointment, please try again.");
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AppointmentRepresentation(null, time, businessId, serviceId, serviceName,
						serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(), businessName,
						businessAvi, businessCreator, repeatFrequency, false, error))
				.cacheControl(utils.cacheControl()).build();
	}

	@SecurityCheck
	@DELETE
	@Path("{appointmentId}")
	public Response deleteAppointment(@Context SecurityContext sc,
			@PathParam("appointmentId") final String appointmentId) {
		acc = (Account) sc.getUserPrincipal();
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				conn.setAutoCommit(false);
				stmt = conn.prepareStatement("DELETE FROM `business_site`.`appointments` WHERE `appointments_id` = ?");
				stmt.setString(1, appointmentId);

				if (stmt.executeUpdate() == 1) {
					if (utils.unScheduleJob("appointment-" + appointmentId, acc.getAccountId())) {
						conn.commit();
						return Response.ok(new AppointmentRepresentation()).cacheControl(utils.cacheControl()).build();
					} else {
						conn.rollback();
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any appointment with that id");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new AppointmentRepresentation(appointmentId, null, null, null, null, null, acc
									.getAccountId(), acc.getName(), acc.getThumbPic(), null, null, null, 0, false,
									error)).cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to delete appointment , please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AppointmentRepresentation(appointmentId, null, null, null, null, null, acc
							.getAccountId(), acc.getName(), acc.getThumbPic(), null, null, null, 0, false, error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to delete appointment , please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AppointmentRepresentation(appointmentId, null, null, null, null, null, acc
								.getAccountId(), acc.getName(), acc.getThumbPic(), null, null, null, 0, false, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to delete appointment , please try again.");
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AppointmentRepresentation(appointmentId, null, null, null, null, null, acc.getAccountId(),
						acc.getName(), acc.getThumbPic(), null, null, null, 0, false, error))

				.cacheControl(utils.cacheControl()).build();

	}

	@PermitAll
	@SecurityCheck
	@PUT
	@Path("{appointmentId}")
	public Response editAppointment(@Context SecurityContext sc,
			@PathParam("appointmentId") final String appointmentId, @FormParam("json") String json) {
		String businessId = null;
		String serviceId = null;
		String time = null;
		acc = (Account) sc.getUserPrincipal();
		String serviceName = null;
		String serviceDescription = null;
		String businessName = null;
		String businessAvi = null;
		String businessCreator = null;
		int repeatFrequency = 0;
		String description = null;
		String appointmentTime = null;

		try {
			JSONObject object = new JSONObject(json);
			businessId = object.getString("business_id");
			serviceId = object.getString("service_id");
			time = object.getString("time");
			serviceName = object.getString("service_name");
			serviceDescription = object.getString("service_description");
			businessName = object.getString("business_name");
			businessAvi = object.getString("business_avi");
			businessCreator = object.getString("business_creator");
			repeatFrequency = object.getInt("repeat_frequency");
			description = object.getString("description");
			appointmentTime = object.getString("appointment_time");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage()))
					.cacheControl(utils.cacheControl()).build();
		}
		Date today = new Date();
		if (new Date(Long.parseLong(time)).before(today)) {
			return Response
					.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "The start date must be after "
							+ new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(today)))
					.cacheControl(utils.cacheControl()).build();
		}
		if (new Date(Long.parseLong(appointmentTime)).before(today)) {
			return Response
					.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "The start date must be after "
							+ new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(today)))
					.cacheControl(utils.cacheControl()).build();
		}
		if (Long.parseLong(appointmentId) < System.currentTimeMillis()) {
			return Response.status(Status.BAD_REQUEST)
					.entity(utils.createErrorResponse(Status.BAD_REQUEST, "You can only edit a future appointment"))
					.cacheControl(utils.cacheControl()).build();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.parseLong(time));

		int minute = cal.get(Calendar.MINUTE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		int month = (cal.get(Calendar.MONTH));
		String expression = null;
		String jobName = "appointment-" + appointmentId;

		if (repeatFrequency == 1) {
			expression = "0 " + minute + " " + hour + " * * ?";
		}
		if (repeatFrequency == 2) {
			expression = "0 " + minute + " " + hour + " ? * " + dayOfWeek + " *";
		}
		if (repeatFrequency == 3) {
			expression = "0 " + minute + " " + hour + " " + dayOfMonth + " 1/1 ? *";
		}
		if (repeatFrequency == 4) {
			expression = "0 " + minute + " " + hour + " " + dayOfMonth + " " + (month + 1) + " ? *";
		}
		if (repeatFrequency == 0) {
			expression = "0 " + minute + " " + hour + " " + dayOfMonth + " " + (month + 1) + " ?";

		}

		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				stmt = conn
						.prepareStatement(" UPDATE `business_site`.`appointments` SET business_id=? ,service_id=?,time=?,repeat_frequency=?,description=?,appointment_time=?  WHERE  appointments_id=? AND user_id=? AND appointments_id>? ");
				stmt.setString(1, businessId);
				stmt.setString(2, serviceId);
				stmt.setString(3, time);
				stmt.setString(7, appointmentId);
				stmt.setInt(4, repeatFrequency);
				stmt.setString(6, appointmentTime);
				stmt.setString(5, description);
				stmt.setString(8, acc.getAccountId());
				stmt.setString(9, String.valueOf(System.currentTimeMillis()));

				if (stmt.executeUpdate() == 1) {
					utils.reScheduleJob(expression, jobName, new Date(Long.parseLong(time)), acc.getAccountId());
					return Response
							.ok(new AppointmentRepresentation(appointmentId, time, businessId, serviceId, serviceName,
									serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(),
									businessName, businessAvi, businessCreator, repeatFrequency, false, error))
							.cacheControl(utils.cacheControl()).build();

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any appointment categories ");
					return Response
							.status(Status.NOT_FOUND)
							.entity(new AppointmentRepresentation(appointmentId, time, businessId, serviceId,
									serviceName, serviceDescription, acc.getAccountId(), acc.getName(), acc
											.getThumbPic(), businessName, businessAvi, businessCreator,
									repeatFrequency, false, error)).cacheControl(utils.cacheControl()).build();
				}
			}
		} catch (Exception e) {
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to edit appointment, please try again.");
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new AppointmentRepresentation(appointmentId, time, businessId, serviceId, serviceName,
							serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(), businessName,
							businessAvi, businessCreator, repeatFrequency, false, error))
					.cacheControl(utils.cacheControl()).build();

		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
						"Server error: Oops something went wrong while trying to edit appointment, please try again.");
				return Response
						.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new AppointmentRepresentation(appointmentId, time, businessId, serviceId, serviceName,
								serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(), businessName,
								businessAvi, businessCreator, repeatFrequency, false, error))
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to edit appointment, please try again.");
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity(new AppointmentRepresentation(appointmentId, time, businessId, serviceId, serviceName,
						serviceDescription, acc.getAccountId(), acc.getName(), acc.getThumbPic(), businessName,
						businessAvi, businessCreator, repeatFrequency, false, error))
				.cacheControl(utils.cacheControl()).build();

	}

	private boolean isWithinRange(Date monthStart, Date monthEnd, Date eventStart, int repeats) {
		if (repeats == 4) {
			// some more work needed here to ensure that event happens on the
			// filter month
			Calendar cal = Calendar.getInstance();
			cal.setTime(eventStart);

			int eventMonth = cal.get(Calendar.MONTH);

			cal.setTime(monthStart);
			int month = cal.get(Calendar.MONTH);
			return ((eventStart.equals(monthStart) || eventStart.after(monthStart)) && eventMonth == month);

		} else
			return ((eventStart.equals(monthEnd) || eventStart.before(monthEnd)));
	}

	@SecurityCheck
	@PermitAll
	@GET
	public Response getAppointments(@Context SecurityContext sc, @QueryParam("json") final String json) {
		acc = (Account) sc.getUserPrincipal();
		int offset = 0;
		int batch = 10;
		boolean ascending = true;
		boolean getAll = false;
		int modelLimit = 0;
		String businessId = null;
		String serviceId = null;
		String businessId1 = null;
		String serviceId1 = null;
		String userId = null;
		boolean user1 = false;
		Boolean done = false;
		Calendar now = Calendar.getInstance();
		int month = now.get(Calendar.MONTH);
		int year = now.get(Calendar.YEAR);
		String appointmentId = null;
		String time = null;
		String description = null;
		String appointmentTime = null;
		String bizName = null;
		String email = null;
		String bizPic = null;
		String bizMan = null;
		String serviceName = null;
		String serviceDescription = null;
		int repeatFrequency = 0;
		String creatorName = null;
		String aptName = null;
		String creatorPic = null;
		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("get_all")) {
				getAll = object.getBoolean("get_all");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}
			if (object.keySet().contains("business_id")) {
				businessId = object.getString("business_id");
			}
			if (object.keySet().contains("service_id")) {
				serviceId = object.getString("service_id");
			}
			if (object.keySet().contains("user_id")) {
				userId = object.getString("user-id");
			}
			if (object.keySet().contains("done")) {
				done = object.getBoolean("done");
			}
			if (object.keySet().contains("month")) {
				month = object.getInt("month");
			}
			if (object.keySet().contains("year")) {
				year = object.getInt("year");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Invalid JSON STRING" + e.getMessage());

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\" : "
							+ utils.createErrorResponse(Status.BAD_REQUEST, "Invalid JSON string. " + e.getMessage())
							+ "}").cacheControl(utils.cacheControl()).build();
		}
		DataSource ds = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = " SELECT appointments.appointments_id,appointments.business_id,appointments.service_id,appointments.user_id, "
				+ " appointments.time,appointments.repeat_frequency,appointments.description,appointments.appointment_time,"
				+ " business.name AS biz_name,business.avi AS biz_pic,business.creator_id AS biz_man,services.name AS service_name ,"
				+ " services.description AS service_description,apt_creator.name AS apt_name,biz_creator.name AS creator_name,"
				+ " apt_creator.email,apt_creator.thumb_pic AS creator_pic,COUNT(cnt.appointments_id) AS nos,appointments.done  "
				+ " FROM `business_site`.appointments "
				+ " LEFT JOIN `business_site`.business ON appointments.business_id=business.business_id "
				+ " LEFT JOIN `business_site`.services ON services.services_id=appointments.service_id "
				+ " LEFT JOIN (SELECT * FROM `business_site`.user ) AS biz_creator ON biz_creator.user_id=appointments.user_id "
				+ " LEFT JOIN (SELECT * FROM `business_site`.user ) AS apt_creator ON apt_creator.user_id=appointments.user_id  "
				+ " LEFT JOIN (SELECT appointments_id FROM `business_site`.appointments ) AS cnt ON cnt.appointments_id=appointments.appointments_id ";
		List<AppointmentRepresentation> list = new ArrayList<AppointmentRepresentation>();
		AppointmentsRepresentation response = new AppointmentsRepresentation();

		try {
			// now we're all good
			Calendar filterCalendar = Calendar.getInstance();
			filterCalendar.set(Calendar.DAY_OF_MONTH, 1);
			filterCalendar.set(Calendar.MONTH, month);
			filterCalendar.set(Calendar.YEAR, year);
			filterCalendar.set(Calendar.HOUR, 0);
			filterCalendar.set(Calendar.MINUTE, 0);
			filterCalendar.set(Calendar.SECOND, 0);

			Date monthStart = filterCalendar.getTime();

			filterCalendar.set(Calendar.DAY_OF_MONTH, getLastDayOfMonth(month, year));
			filterCalendar.set(Calendar.HOUR, 11);
			filterCalendar.set(Calendar.MINUTE, 0);
			filterCalendar.set(Calendar.SECOND, 0);

			Date monthEnd = filterCalendar.getTime();
			System.out.println("Month start: " + monthStart + "\tmonth end: " + monthEnd);
			ds = MyDataSource.INSTANCE.getDataSource(utils.dbName);
			conn = ds.getConnection();
			if (conn != null) {
				if (done == null) {
					if (userId == null) {
						if (businessId == null) {
							if (serviceId == null) {
								if (getAll) {
									if (ascending) {
										sql += " ORDER BY appointments_id ASC";
									} else {
										sql += " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
								} else {
									if (ascending) {
										sql += " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setInt(1, offset);
									stmt.setInt(2, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.service_id=? " + " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.service_id=? " + " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
								} else {
									if (ascending) {
										sql += " WHERE appointments.service_id=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.service_id=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setInt(2, offset);
									stmt.setInt(3, batch);
								}
							}
						} else {
							if (serviceId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.business_id=? " + " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.business_id=? " + " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, businessId);
								} else {
									if (ascending) {
										sql += " WHERE appointments.business_id=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.business_id=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, businessId);
									stmt.setInt(2, offset);
									stmt.setInt(3, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, businessId);
								} else {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, businessId);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							}
						}
					} else {
						if (businessId == null) {
							if (serviceId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.user_id=? " + " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.user_id=? " + " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, userId);
								} else {
									if (ascending) {
										sql += " WHERE appointments.user_id=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.user_id=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, userId);
									stmt.setInt(2, offset);
									stmt.setInt(3, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, userId);
								} else {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, userId);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							}
						} else {
							if (serviceId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.business_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.business_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, businessId);
									stmt.setString(2, userId);
								} else {
									if (ascending) {
										sql += " WHERE appointments.business_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.business_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, businessId);
									stmt.setString(2, userId);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, businessId);
									stmt.setString(3, userId);
								} else {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND appointments.user_id=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, businessId);
									stmt.setString(3, userId);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							}
						}
					}
				} else {
					if (userId == null) {
						if (businessId == null) {
							if (serviceId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.done=? ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.done=? ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, done);
								} else {
									if (ascending) {
										sql += " WHERE appointments.done=? ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.done=? ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setBoolean(1, done);
									stmt.setInt(2, offset);
									stmt.setInt(3, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.service_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setBoolean(2, done);
								} else {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.service_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setBoolean(2, done);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							}
						} else {
							if (serviceId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.business_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.business_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, businessId);
									stmt.setBoolean(2, done);
								} else {
									if (ascending) {
										sql += " WHERE appointments.business_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.business_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, businessId);
									stmt.setBoolean(2, done);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, businessId);
									stmt.setBoolean(3, done);
								} else {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, businessId);
									stmt.setBoolean(3, done);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							}
						}
					} else {
						if (businessId == null) {
							if (serviceId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, userId);
									stmt.setBoolean(2, done);
								} else {
									if (ascending) {
										sql += " WHERE appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, userId);
									stmt.setBoolean(2, done);
									stmt.setInt(3, offset);
									stmt.setInt(4, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, userId);
									stmt.setBoolean(3, done);
								} else {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, userId);
									stmt.setBoolean(3, done);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							}
						} else {
							if (serviceId == null) {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.business_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.business_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, businessId);
									stmt.setString(2, userId);
									stmt.setBoolean(3, done);
								} else {
									if (ascending) {
										sql += " WHERE appointments.business_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.business_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, businessId);
									stmt.setString(2, userId);
									stmt.setBoolean(3, done);
									stmt.setInt(4, offset);
									stmt.setInt(5, batch);
								}
							} else {
								if (getAll) {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, businessId);
									stmt.setString(3, userId);
									stmt.setBoolean(4, done);
								} else {
									if (ascending) {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id ASC LIMIT ?,?";
									} else {
										sql += " WHERE appointments.service_id=? AND appointments.business_id=? AND appointments.user_id=? AND  appointments.done=? "
												+ " ORDER BY appointments_id DESC LIMIT ?,?";
									}
									stmt = conn.prepareStatement(sql);
									stmt.setString(1, serviceId);
									stmt.setString(2, businessId);
									stmt.setString(3, userId);
									stmt.setBoolean(4, done);
									stmt.setInt(5, offset);
									stmt.setInt(6, batch);
								}
							}
						}
					}
				}
				rs = stmt.executeQuery();

				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						businessId1 = rs.getString("business_id");
						serviceId1 = rs.getString("service_id");
						appointmentId = rs.getString("appointments_id");
						time = rs.getString("time");
						description = rs.getString("description");
						appointmentTime = rs.getString("appointment_time");
						bizName = rs.getString("biz_name");
						bizPic = rs.getString("biz_pic");
						bizMan = rs.getString("biz_man");
						serviceName = rs.getString("service_name");
						serviceDescription = rs.getString("service_description");
						repeatFrequency = rs.getInt("repeat_frequency");
						aptName = rs.getString("apt_name");
						creatorName = rs.getString("creator_name");
						email = rs.getString("email");
						modelLimit = rs.getInt("nos");
						creatorPic = rs.getString("creator_pic");
						done = rs.getBoolean("done");

						if (isWithinRange(monthStart, monthEnd, new Date(Long.parseLong(appointmentTime)),
								repeatFrequency)) {

							AppointmentRepresentation app = new AppointmentRepresentation(appointmentId, time,
									businessId1, serviceId1, serviceName, serviceDescription, userId, aptName,
									creatorPic, bizName, bizPic, creatorName, repeatFrequency, done, error);
							list.add(app);

						}
						appointmentTime = null;
					}

				} else {
					error = utils.createErrorResponse(Status.NOT_FOUND,
							"The user has not created any business service categories ");
					return Response.status(Status.NOT_FOUND).entity(response).cacheControl(utils.cacheControl())
							.build();
				}
				response = new AppointmentsRepresentation(businessId, serviceId, appointmentTime, userId, list, offset,
						modelLimit, modelLimit);
				return Response.ok(response).cacheControl(utils.cacheControl()).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Server error: Oops something went wrong while trying to get business service, please try again.");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
					.build();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				stmt = null;

				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error(e.getMessage());
				error = utils
						.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Server error: Oops something went wrong while trying to get business service, please try again.");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response)
						.cacheControl(utils.cacheControl()).build();
			}
		}
		error = utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
				"Server error: Oops something went wrong while trying to get business service, please try again.");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(response).cacheControl(utils.cacheControl())
				.build();
	}

	private int getLastDayOfMonth(int month, int year) {
		if (month == 0)
			return 31;
		else if (month == 1) {
			if ((year % 4) == 0)
				return 29;
			else
				return 28;
		} else if (month == 2) {
			return 31;
		} else if (month == 3) {
			return 30;
		} else if (month == 4) {
			return 31;
		} else if (month == 5) {
			return 30;
		} else if (month == 6) {
			return 31;
		} else if (month == 7) {
			return 31;
		} else if (month == 8) {
			return 30;
		} else if (month == 9) {
			return 31;
		} else if (month == 10) {
			return 30;
		} else
			return 31;
	}
}
