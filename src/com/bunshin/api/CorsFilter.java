package com.bunshin.api;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CorsFilter implements Filter {
	Logger log = LoggerFactory.getLogger(CorsFilter.class);

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		System.out.println("in filter " + chain.toString() + "\n"
				+ request.toString());
		log.trace("doFilter");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		if (req.getHeader("Origin") != null || req.getHeader("origin") != null) {
			res.addHeader("Access-Control-Allow-Origin", "*");
			res.addHeader(
					"Access-Control-Expose-Headers",
					"X-Cache-Date, X-Atmosphere-tracking-id,Authorization, USER_SCHOOL ,  X-Heartbeat-Server , CONTENT_AUTHOR , REMOTE_TOKEN ,REMOTE_USER , REMOTE_SCHOOL_ID ");
		}

		if ("OPTIONS".equalsIgnoreCase(req.getMethod())) {
			res.addHeader("Access-Control-Allow-Origin", "*");
			res.addHeader("Access-Control-Allow-Methods",
					"GET, POST, DELETE, PUT, OPTIONS, HEAD");
			res.addHeader("Access-Control-Allow-Credentials", "true");
			res.addHeader(
					"Access-Control-Allow-Headers",
					"Origin, Content-Type, X-Atmosphere-Framework,X-Cache-Date, X-Atmosphere-tracking-id, X-Atmosphere-Transport, Authorization , USER_SCHOOL , X-Heartbeat-Server , REMOTE_TOKEN , REMOTE_USER , REMOTE_SCHOOL_ID , CONTENT_AUTHOR ");
			res.addHeader("Access-Control-Max-Age", "-1");
		}

	}

	@Override
	public void init(FilterConfig conf) throws ServletException {
		System.out.println(" " + conf.getServletContext().getContextPath());

	}

}
