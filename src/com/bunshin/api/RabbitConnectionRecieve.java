package com.bunshin.api;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public enum RabbitConnectionRecieve {
	INSTANCE;

	private RabbitConnectionRecieve() {

	}

	private static ConnectionFactory factory = null;
	private static Connection connection = null;

	public Connection getConnection() {

		if (connection == null) {

			synchronized (RabbitConnectionRecieve.INSTANCE) {
				try {
					factory = new ConnectionFactory();
					factory.setHost("localhost");
					factory.setTopologyRecoveryEnabled(true);
					factory.setAutomaticRecoveryEnabled(true);

					connection = factory.newConnection();
					System.out.println("rabbit: "+connection.isOpen());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
		return connection;

	}

	public void shutdown() {
		if (connection != null) {
			synchronized (NodeOperation.INSTANCE) {
				try {
					connection.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					factory=null;
					connection = null;
				}
			}
		}
	}

}
