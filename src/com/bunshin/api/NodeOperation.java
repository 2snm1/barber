package com.bunshin.api;

import javax.inject.Singleton;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

@Singleton
public enum NodeOperation {
	INSTANCE;

	private NodeOperation() {

	}

	// private static Node node = null;
	private ClusterHealthResponse hr = null;
	private Client client = null;

	public Client getClient() {

		if (client == null) {

			synchronized (NodeOperation.INSTANCE) {

				ImmutableSettings.Builder settings = ImmutableSettings.settingsBuilder();

				// the node to store items
				// settings.put("node.client", false);
				// settings.put("node.data", true);
				// the node is only a client and does not store anything
				/*
				 * settings.put("node.client", true); settings.put("node.data",
				 * false); settings.put("node.master", false);
				 * settings.put("node.name", "barber");
				 */
				settings.put("cluster.name", "API");

				settings.put("bootstrap.mlockall", "true");
				settings.put("client.transport.sniff", true);
				settings.build();
				// node = NodeBuilder.nodeBuilder().settings(settings).node();
				// hr=node.client().admin().cluster().
				// prepareHealth().
				// setWaitForGreenStatus().
				// setTimeout(TimeValue.timeValueMillis(250)).
				// execute().
				// actionGet();
				// if(hr==null){
				// node=null;
				// }
				// cannot use try-with cause the Transport client will be closed
				// after exiting the block thus no nodes can be found
				client = new TransportClient(settings).addTransportAddress(new InetSocketTransportAddress("ec2-52-25-229-135.us-west-2.compute.amazonaws.com",
						9300));

			}
		}
		return client;

	}

	public void shutdown() {
		if (client != null) {
			synchronized (NodeOperation.INSTANCE) {
				client.close();
				client = null;
			}
		}
	}

}
