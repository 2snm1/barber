package com.bunshin.api;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.inject.Singleton;
import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.bunshin.api.utils.Utils;

@Singleton
public enum MyDataSource {

	INSTANCE;

	private MyDataSource() {
	}

	Utils utils = new Utils();
	private ConcurrentMap<String, ComboPooledDataSource> dataSources = new ConcurrentHashMap<String, ComboPooledDataSource>();

	private ComboPooledDataSource cpds = null;

	private ComboPooledDataSource getDataSource() {
		if (cpds == null) {
			cpds = new ComboPooledDataSource();
//			cpds.setJdbcUrl("jdbc:mysql://localhost:3306/");
//			cpds.setPassword("");
//			cpds.setUser("root");
			 cpds.setUser("barber");
			 cpds.setPassword("r00tAW$1");
			 cpds.setJdbcUrl("jdbc:mysql://aadotdbuugpmuw.cjoft2rqycdt.us-west-2.rds.amazonaws.com:3306/");
		}
		return cpds;
	}

	public DataSource getDataSource(String tenantId) {
		if (tenantId.equalsIgnoreCase("main")) {
			dataSources.putIfAbsent("main", getDataSource());
			cpds = null;
		} else {
			dataSources.putIfAbsent(tenantId, getComboPooledDataSource(tenantId));
			cpds = null;
		}
		return dataSources.get(tenantId);
	}

	private ComboPooledDataSource getComboPooledDataSource(String tenantId) {
		if (cpds == null) {
			cpds = new ComboPooledDataSource();
		}

		 cpds.setJdbcUrl("jdbc:mysql://aadotdbuugpmuw.cjoft2rqycdt.us-west-2.rds.amazonaws.com:3306/"
		 + tenantId);
		 cpds.setUser("barber");
		 cpds.setPassword("r00tAW$1");
//		cpds.setJdbcUrl("jdbc:mysql://localhost:3306/" + tenantId);
//		 cpds.setPassword("");
		 cpds.setUser("customer");
		 cpds.setPassword("dbCu$t0m3r");

		return cpds;
	}

	public void close() {
		if (dataSources != null && !dataSources.isEmpty()) {
			for (ComboPooledDataSource ds : dataSources.values()) {
				System.out.println("\n  close: " + ds);
				ds.close();
			}
			dataSources = null;
		}
	}

}
