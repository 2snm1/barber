package com.bunshin.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.ServletContext;

import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceFactory;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.util.ServletContextFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.bunshin.api.utils.Utils;
import com.bunshin.api.MyDataSource;

public class ScheduledEventsHandler implements Job {

	private Utils utils = new Utils();
	private AtmosphereResource resource = null;
	private ServletContext servletContext = ServletContextFactory.getDefault().getServletContext();
	private AtmosphereFramework framework = (AtmosphereFramework) servletContext.getAttribute("AtmosphereServlet");
	private AtmosphereResourceFactory atmosphereResourceFactory = framework.atmosphereFactory();

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getJobDetail().getKey().getName();
		String group = context.getJobDetail().getKey().getGroup();
		sendReminders(jobName, group);
	}

	private void sendReminders(String jobName, String userId) {
		String appointmentId = jobName.replace("appointment-", "").trim();
		System.out.println("REMINDERS SENDING");

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String email = null;
		String businessId = null;
		String serviceId = null;
		String time = null;
		String description = null;
		String appointmentTime = null;
		String bizName = null;
		String bizPic = null;
		String bizMan = null;
		String serviceName = null;
		String serviceDescription = null;
		int repeatFrequency = 0;
		String creatorName = null;
		String aptName = null;
		try {
			conn = MyDataSource.INSTANCE.getDataSource(utils.dbName).getConnection();
			if (conn != null) {
				stmt = conn
						.prepareStatement(" SELECT appointments.appointments_id,appointments.business_id,appointments.service_id,appointments.user_id, "
								+ " appointments.time,appointments.repeat_frequency,appointments.description,appointments.appointment_time,"
								+ " business.name AS biz_name,business.avi AS biz_pic,business.creator_id AS biz_man,services.name AS service_name, "
								+ " services.description AS service_description,apt_creator.name AS apt_name,biz_creator.name AS creator_name,apt_creator.email "
								+ " FROM `business_site`.appointments "
								+ " LEFT JOIN `business_site`.business ON appointments.business_id=business.business_id "
								+ " LEFT JOIN `business_site`.services ON services.services_id=appointments.service_id "
								+ " LEFT JOIN (SELECT * FROM `business_site`.user ) AS biz_creator ON biz_creator.user_id=appointments.user_id "
								+ " LEFT JOIN (SELECT * FROM `business_site`.user ) AS apt_creator ON apt_creator.user_id=appointments.user_id "
								+ " WHERE appointments.appointments_id=? AND appointments.user_id=? ");
				stmt.setString(1, String.valueOf(appointmentId));
				stmt.setString(2, userId);
				rs = stmt.executeQuery();
				if (rs.isBeforeFirst()) {
					while (rs.next()) {
						businessId = rs.getString("bussiness_id");
						serviceId = rs.getString("service_id");
						time = rs.getString("time");
						description = rs.getString("description");
						appointmentTime = rs.getString("appointment_time");
						bizName = rs.getString("biz_name");
						bizPic = rs.getString("biz_pic");
						bizMan = rs.getString("biz_man");
						serviceName = rs.getString("service_name");
						serviceDescription = rs.getString("service_description");
						repeatFrequency = rs.getInt("repeat_frequency");
						aptName = rs.getString("apt_name");
						creatorName = rs.getString("creator_name");
						email = rs.getString("email");
						if (description == null) {
							description = "<br>Hi "
									+ creatorName
									+ ",<br> This is your "
									+ new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(new Date(Long.parseLong(time)))
									+ " reminder of your appointment "
									+ new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(new Date(Long
											.parseLong(appointmentTime))) + "at  " + bizName + " for " + serviceName
									+ ": " + serviceDescription;
						}

						if (utils.sendEmail("APPOINTMENT REMINDER", description, email, null, null,
								"stephenmaina685@gmail.com", false, null, "stephenmaina685@gmail.com")) {
							utils.setAppointmentsDone(appointmentId);
							resource = atmosphereResourceFactory.find(Utils.userAcc.get(userId));
							BroadcasterFactory bf = resource.getAtmosphereConfig().getBroadcasterFactory();
							utils.setNotification(String.valueOf(System.currentTimeMillis()), userId,
									"reminder of scheduled event", "resources/business/appointment/" + appointmentId,
									"{\"type\":\"notification\",\"value\":\"resources/business/appointment/"
											+ appointmentId + "\",\"action\":\"add\"}", 1, bf.lookup("notification/"));
						}
					}
				} else {
					// not found
				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				rs = null;
				if (stmt != null)
					stmt.close();
				stmt = null;
				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
