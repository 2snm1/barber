package com.bunshin.api;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

//register as a provider
@Provider
@Priority(Priorities.HEADER_DECORATOR)
public class PoweredBy implements ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext requestContext,
			ContainerResponseContext responseContext) throws IOException {
		responseContext.getHeaders().add("API-Powered-By", "BUNSHIN API");

		responseContext.getHeaders().putSingle("Access-Control-Allow-Origin",
				"*");
		responseContext.getHeaders().putSingle(
				"Access-Control-Allow-Credentials", "true");
		responseContext
				.getHeaders()
				.putSingle("Access-Control-Allow-Headers",
						"Authorization, USER_SCHOOL , X-Atmosphere-tracking-id , X-Heartbeat-Server , Origin , Content-Type , REMOTE_USER , REMOTE_TOKEN , CONTENT_AUTHOR , REMOTE_SCHOOL_ID ,Cache-Control,X-Requested-With,SITE");
		responseContext.getHeaders().putSingle("Access-Control-Allow-Methods",
				"GET, POST, DELETE, PUT, OPTIONS, HEAD");
		String expose = "";
		for (String ex : responseContext.getHeaders().keySet()) {
			expose += ex + ",";
		}
		System.out.println(responseContext.getHeaders().keySet());
		responseContext.getHeaders().putSingle("Access-Control-Expose-Headers",
				expose + "X-Atmosphere-tracking-id,X-Heartbeat-Server ");
	}

}
