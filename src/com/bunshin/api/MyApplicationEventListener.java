package com.bunshin.api;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;


@Provider
@Priority(Priorities.ENTITY_CODER)
public class MyApplicationEventListener implements ApplicationEventListener {
	   private volatile int requestCnt = 0;

       static long methodExecution =0;
	   
	    @SuppressWarnings("incomplete-switch")
		@Override
	    public void onEvent(ApplicationEvent event) {
	        switch (event.getType()) {
	            case INITIALIZATION_FINISHED:
	                System.out.println("Application "
	                        + event.getResourceConfig().getApplicationName()
	                        + " was initialized.");
	                break;
	            case DESTROY_FINISHED:
	                System.out.println("Application "
	                    + event.getResourceConfig().getApplicationName()+" destroyed.");
	                break;
	        }
	    }
	 
	    @Override
	    public RequestEventListener onRequest(RequestEvent requestEvent) {
	        requestCnt++;
	        System.out.println("[TIMING] Request " + requestCnt + " started.");
	        // return the listener instance that will handle this request.
	        return new MyRequestEventListener(requestCnt);
	    }


    public static class MyRequestEventListener implements RequestEventListener {
        private volatile long methodStartTime;
        private final int requestNumber;
        private final long startTime;
        public MyRequestEventListener(int requestNumber) {
            this.requestNumber = requestNumber;
            startTime = System.currentTimeMillis();
        }
        @SuppressWarnings({ "unused", "incomplete-switch" })
		@Override
        public void onEvent(RequestEvent requestEvent) {
        	Set<OpenOption> options = new HashSet<OpenOption>();
            options.add(StandardOpenOption.APPEND);
            options.add(StandardOpenOption.CREATE);
         // Create the custom permissions attribute.
//            Set<PosixFilePermission> perms =
//              PosixFilePermissions.fromString("rwxrwxrwx");
//            FileAttribute<Set<PosixFilePermission>> attr =
//              PosixFilePermissions.asFileAttribute(perms);
//            
//            
//            Path file = Paths.get("./opt/timing.log");
          
            String output=null; 
            switch (requestEvent.getType()) {
                case RESOURCE_METHOD_START:
                    methodStartTime = System.currentTimeMillis();
                     output="[TIMING] Resource method "
                            + requestEvent.getUriInfo().getMatchedResourceMethod()
                            .getHttpMethod()
                        + " started for request " + requestNumber+"\n";
                    System.out.println(output);
                    byte data[] = output.getBytes();
                    ByteBuffer bb = ByteBuffer.wrap(data);
//                    try (SeekableByteChannel sbc =
//                    	      Files.newByteChannel(file, options, attr)) {
//                    	      sbc.write(bb);
//                    	    } catch (IOException x) {
//                    	      System.out.println("Exception thrown: " + x);
//                    	    }
                    break;
                case RESOURCE_METHOD_FINISHED:
                     methodExecution = System.currentTimeMillis() - methodStartTime;
                    final String methodName = requestEvent.getUriInfo().getMatchedResourceMethod().getInvocable().getHandlingMethod().getName();
                   output="[TIMING] Method '" + methodName + "' executed. Processing time: " + methodExecution + " ms"+"\n";
                    System.out.println(output);
                    System.out.println(output);
                    byte finishedData[] = output.getBytes();
                    ByteBuffer finishedBufer = ByteBuffer.wrap(finishedData);
//                    try (SeekableByteChannel sbc =
//                    	      Files.newByteChannel(file, options, attr)) {
//                    	      sbc.write(finishedBufer);
//                    	    } catch (IOException x) {
//                    	      System.out.println("Exception thrown: " + x);
//                    	    }
                    break;
           
            
            
            
            }
        }
    }
    
    @Priority(Priorities.HEADER_DECORATOR)
    public class Timing implements ContainerResponseFilter {

    	@Override
    	public void filter(ContainerRequestContext requestContext,
    			ContainerResponseContext responseContext) throws IOException {
    		responseContext.getHeaders().add("Timing", methodExecution);
    	}
    }
}

